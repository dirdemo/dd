#!/bin/bash

cp start-dd.sh ~/start-dd.sh

cd ..
#sudo su
apt-get update -y
apt-get install nodejs -y
apt-get install npm -y
apt-get install webpack -y
apt-get install redis -y
apt-get install redis-tools -y

cd server
npm install
npm upgrade
npm audit fix
npm install --global pm2
npm install --global webpack
npm install --global nodemon
npm audit fix