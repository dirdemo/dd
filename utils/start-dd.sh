#!/bin/bash

echo "Navigate dd/server/"
cd dd/server/
echo "Git check..."
git reset --hard
git pull origin master
echo "Starting..."
echo 'password' | sudo -S  pm2 flush
echo 'password' | sudo -S  pm2 start server.config.json -f
echo " "
echo "Ended"
echo " "
echo " "
echo "sudo pm2 logs dd-server"
sudo pm2 logs dd-server
