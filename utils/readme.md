##### _*Javna platforma, za direktnu demokratiju, 2019.*_

# Utils

### Install on Linux
```
sudo sh install-ubuntu.sh
copy start-dd.sh ~/start-dd.sh
```

## Commands

### Create default server SSL
##### create-ssl.sh
```
#!/bin/sh

echo "First read readme.md file"
echo ""
cd ..
echo "Start creating ssl..."

openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
rm csr.pem

echo "End ssl"
```

### Start Local Tunnel
##### start-lt.sh
```
#!/bin/bash

echo "Localtunnel: https://localtunnel.me/"
echo "To install: npm install -g localtunnel"

lt -p1024 -s dd-test
```


### Start DirDemo Server
##### start-dd.sh
```
#!/bin/bash

echo "Navigate src/dd/server/"
cd src/dd/server/
echo "Git check..."
git reset --hard
git pull origin master
if [ $# -eq 1 ]
  then
    echo "Upgrade mode"
    echo "Installing..."
    npm install
    echo "Upgrading..."
    npm upgrade
    npm audit fix
    echo "Compiling..."
    webpack
fi
echo "Starting..."
echo 'password' | sudo -S  pm2 flush
echo 'password' | sudo -S  pm2 start server.config.json -f
echo " "
echo "Ended"
echo " "
echo " "
echo "sudo pm2 logs dd-server"
sudo pm2 logs dd-server
```

### Start Backup

###### backup.sh
```
#!/bin/bash

zip -r dd-backup.zip src/dd/

zip    dd-backup-server.zip src/dd/server/
zip -r dd-backup-server-js.zip src/dd/server/js/
zip -r dd-backup-server-config.zip src/dd/server/config/
zip -r dd-backup-server-html.zip src/dd/server/html/
zip -r dd-backup-server-webpack.zip src/dd/server/webpack/
```



