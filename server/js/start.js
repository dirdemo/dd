console.log('DirDEMO', 1, 0, 1);
console.log('Starting: ', __dirname, process.argv);
 
var fConfigFile = "config/config.json";
if(process&&process.argv&&2<process.argv.length){
	fConfigFile = process.argv[2].toString();
}
const Lang = require('./language.01.js');
const Engine = require('./engine.01.js');
if(Engine.setConfig(fConfigFile)){
	/**/ Engine.echoTest('Configuration applied', undefined, 5); /**/
}
if(Engine.setLicence()){
	/**/ Engine.echoTest('Licence applied', Engine.Stats.FileVersion, 5); /**/
}
const bindHttp = function(request, response){
	try{
		return Engine.bindHttp(request, response);
	}catch(err){
		console.log('Error http', err);
	}
}
const bindSocket = function(socket) {
	Engine.echoTest('Bind socket', socket.id, 1);

	var fSocket = Engine.initSocket(socket.id, socket);
	
	socket.emit('auth', fSocket, Engine.Sockets);
	Engine.Event('Event.Auth', fSocket);
	socket.on('disconnect', function(reason) {
		/**/	Engine.echoTest('Disconnect socket', reason, 1);/**/
		Engine.delSocket(socket.id);
		Engine.Event('Event.Disconnect', fSocket);
	});
  socket.on('error', function (err) {
		console.log('Error socket:', err);
  });
  socket.on('Event', function (iData, iCallback) {
		//Engine.Event(iData, iCallback);
  });
  socket.on('Sync', function (iData, iCallback) {
		try{
			iData.Trace = fSocket;
			/*Engine.echoTest('Sync start', iData, -1, fSocket);*/
			var fNameStart = Engine.getDataNameStart(iData);

			if(fNameStart=="Buildin"){
				Engine.Sync(iData, iCallback, socket);
			}else{
				if(Engine.Services[fNameStart]){
					Engine.echoTest('Sync found', fNameStart+"/"+iData.Name, -1);
					Engine.Services[fNameStart].Sync(iData, iCallback);
				}else{
					Engine.echoTest('Sync not found', fNameStart, -1);
					console.log('Sync not found', fNameStart, Engine.Services, iData);
				}
			}
		}catch(err){
      console.log('Error sync', err, iData);
    }
  });
}
const bindPeer = function(peer){
	//console.log('Bind peer');
}
if(Engine.setHttp(bindHttp)){
	/**/ Engine.echoTest('Loaded bindHttp', undefined, 5); /**/
}
if(Engine.setIO(bindSocket)){
	/**/ Engine.echoTest('Loaded bindSocket', undefined, 5); /**/
}
if(Engine.setPeer(bindPeer)){
	/**/ Engine.echoTest('Loaded bindPeer', undefined, 5); /**/
}
/**/ Engine.echoTest('Starting as ', Engine.Config.Server.Http, 5); /**/
Engine.http.listen(Engine.Config.Server.Http.Port, Engine.Config.Server.Http.IP, 5, function(){
	/**/ Engine.echoTest('Listening on', Engine.http.address(), 5); /**/
});
