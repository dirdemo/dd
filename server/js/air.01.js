module.exports = {
	Utils: null,
	UseCache: false,
	Cache: {
		Index: [],
		Table: {}
	},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Air.Cache', this.UseCache);
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fCallbackExec = true;
		/**/ this.Utils.echoTest('Sync Air', iData, 1); /**/
		switch (iData.Name) {
			case "Air-Init":
			break;
			case "Air-List":
				switch((iData.Action||"")){
					case "detail":
						fResult = this.Utils.getData('air', this.Cache.Table, iData.Row);
					break;
					case "view":
					case "save":
						this.Cache.Table = this.Utils.setData('air', this.Cache.Table, iData.Row, iData.Action);
						this.Utils.setEvent({ Name:  iData.Name});
						fResult = this.Utils.getClone(this.Cache.Table);
					break;
					default:
						fResult = { 
							Online: this.Utils.Users,
							All: this.Utils.Users,
							Info: {
								Sockets: this.Utils.Sockets,
								Cookies: this.Utils.Cookies,
								Users: this.Utils.Users
							}
						 };
					break;
				}
			break;
		}
		if (iCallback&&fCallbackExec) {
			iCallback(undefined, iData, fResult);
		}
	}
}