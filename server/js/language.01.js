Lng = {
  Site: {
    Title: "DirDEMO",
    Logo: "/res/img/android-icon-36x36.png",
    Website: "dd.in.rs",
    Webhref: "//dd.in.rs",
    Description: "politi&#269;ke informacije iz Srbije.",
    Year: "2023",
    MenuMain: [
      { Caption: "Po&#269;etna", Link: "/"},
      { Caption: "Krugovi", Link: "/krugovi"},
      { Caption: "Diskusija", Link: "/diskusija"},
      { Caption: "Glasanja", Link: "/glasanja"},
      { Caption: "Vesti", Link: "/vesti"}
    ],
    MenuFooter: [
      { Caption: "Po&#269;etna", Link: "/"},
      { Caption: "Krugovi", Link: "/krugovi"},
      { Caption: "&#268;lanovi", Link: "/clanovi"},
      { Caption: "Diskusija", Link: "/diskusija"},
      { Caption: "Glasanja", Link: "/glasanja"},
      { Caption: "Izbori", Link: "/izbori"},
      { Caption: "Vesti", Link: "/vesti"},
      { Caption: "Live", Link: "/live"},
      { Caption: "Tenderi", Link: "/tenderi"},
      { Caption: "Statistika", Link: "/stat"},
      { Caption: "Istorija", Link: "/istorija"},
      { Caption: "Izvori", Link: "/izvori"},
      { Caption: "Konfiguracija", Link: "/config"}
    ],
    MenuResource: [
      { Caption: "<i class='fa fa-twitter'></i>&nbsp;Twitter", Link: "https://www.twitter.com/"},
      { Caption: "<i class='fa fa-facebook'></i>&nbsp;Facebook", Link: "https://www.facebook.com/groups/direktna.demokratija"},
      { Caption: "<i class='fa fa-comments-o'></i>&nbsp;Discord", Link: "https://discord.gg/uqjDjSz"},
      { Caption: "<i class='fa fa-meetup'></i>&nbsp;Meetup", Link: "https://www.meetup.com/direktna-demokratija/"},
      { Caption: "<i class='fa fa-bitbucket'></i>&nbsp;Bitbucket", Link: "https://bitbucket.org/dirdemo/dd/"},
      { Caption: "<i class='fa fa-wikipedia-w'></i>&nbsp;Wiki", Link: "https://bitbucket.org/dirdemo/dd/wiki/Home"}
    ],
    MenuSocial: [
      { Caption: "<i class='fa fa-2x fa-twitter'></i>", Link: "https://twitter.com/intent/tweet?text=https://dd.in.rs/"},
      { Caption: "<i class='fa fa-2x fa-facebook'></i>", Link: "https://www.facebook.com/sharer/sharer.php?u=https://dd.in.rs/"}
    ]
  }
};