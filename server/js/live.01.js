module.exports = {
	Utils: null,
	UseCache: false,
	TableName: "live",
	Pages: {
		"live": { Caption: "Live", href: "/live"}
	},
	Widgets: {
		"live": { Caption: "Live", Widget: "" }
	},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Service.Live.Cache', this.UseCache);
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fPath = this.Utils.getStorage(this.TableName, iData);
		var fCallbackExec = true;
		/**/ this.Utils.echoTest('Live.Sync', iData, 1); /**/
		switch (iData.Name) {
			case "Live-Init":
				if(this.UseCache){
					fResult = this.Utils.getData(this.TableName, {});
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
			break;
			case "Live-List":
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);

				switch((iData.Action||"")){
					case "detail":
						fResult = this.Utils.getData(this.TableName, fTable, iData.Row, fPath);
					break;
					case "delete":
					case "save":
						fTable= this.Utils.setData(this.TableName, fTable, iData.Row, iData.Action, fPath);
						this.Utils.setEvent({ Name:  iData.Name });
						fResult = this.Utils.getClone(fTable);
					break;
					default:
						fResult = this.Utils.getClone(fTable);
					break;
				}

				if(this.UseCache){
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
			break;
			case "Live-Index":
				var fTable = {};
				if(this.UseCache){
					fTable = this.Utils.getCache(fPath, this.TableName);
				}else{
					fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);
				}
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);

				fResult = this.Utils.getClone(fTable);
			break;
		}
		if (iCallback&&fCallbackExec) {
			iCallback(undefined, iData, fResult);
		}
	}
}