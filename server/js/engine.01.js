var fs = require('fs');
var mail = require('nodemailer');
var atob = require('atob');
var btoa = require('btoa');
var crypto = require('crypto');
const redis = require("redis");

module.exports = {
  isTest: true,
  UseCache: false,
  DebugLevel: 9,
  Config: {},
  ConfigFile: "config.json",
  Services: {},
  Sources: [],
  SourcesFile: "data/sources.json",
  TableExt: ".db",
  TableCompression: false,
  Redis: undefined,
	UseCompress: false,
	RoutingFile: "routings.json",
	Routing: {

	},
  Cache: {
    Pages: [],
    Resource: [],
    FolderExists: {},
    Pins: {},
    Data: {}
  },
  Stats: {
    UpTime: null,
    StartTime: null,
    OS: "",
    MemorySize: 0,
    FileVersion: ""
  },
  Trace: { Stack: [], Text: '', LastTime: null },
  Sessions: {},
  http: undefined,
  io: undefined,
  peer: undefined,
  Sockets: {},
  Cookies: {},
  Users: {},
  setConfig: function (iConfigFile) {
    if (iConfigFile) {
      this.ConfigFile = iConfigFile;
    }

    if (fs.existsSync(this.ConfigFile)) {
      /**/ this.echoTest('Reading ', this.ConfigFile, 5); /**/

      this.Config = this.getFileJson(this.ConfigFile, this.Config);
      this.UseCache = this.getConfig('Server.Cache', this.UseCache);
      this.isTest = this.getConfig('Server.Test', this.isTest);
      this.DebugLevel = this.getConfig('Server.DebugLevel', this.DebugLevel);
    } else {
      console.log('Error: no configuration file ', this.ConfigFile);
    }
    this.setStorageInit();
    this.SourcesFile = this.getConfig('Sources.File', this.SourcesFile);
    this.getSources(true);
    //console.log('SourcesFile', this.SourcesFile,  this.Sources);
    //this.Config.Service["buildin"] = this;
    for (var fService in this.Config.Service) {
      if (fJsFile = this.Config.Service[fService].Js) {
        this.Stats.FileVersion += this.getFileHash(fJsFile);

        this.Services[fService] = require(fJsFile);
        this.Services[fService].setUtils(this);
        var self = this;
        this.Services[fService].Sync({ Name: fService + "-Init" }, function (oData, oResult) {
          /**/ self.echoTest('Service loaded:', fService, 5); /**/
        });
      }
    }

		this.UseCache = this.getConfig('Server.Http.Cache', this.UseCache);
		this.UseCompress = this.getConfig('Server.Http.Compress', this.UseCompress);
		if (this.getConfig('RoutingFile', "")) {
			this.RoutingFile = this.getConfig('RoutingFile', this.RoutingFile);
			this.Routing = this.getFileJson(this.RoutingFile, this.Routing);
		} else {
			this.Routing = this.getConfig('Routing', this.Routing);
		}

    try {
      this.Stats.StartTime = new Date();
      this.Stats.UpTime = this.Stats.StartTime.getTime();
      this.Stats.OS = process.platform;
    } catch (err) {
      console.log('Error:', err);
    }
    return true;
  },
	setLng: function (iLng) {
		if (!this.Lng) {
			this.Lng = iLng;
		}
	},
  validateConfig: function (iConfigFile) {
    return true;
  },
  /**
   * get Cache
   * @param {string} iKey 
   * @param {object} iDefault 
   * @param {string} iTableName 
   */
  getCache: function (iKey, iDefault, iTableName) {
    var fKey = iKey;

    if (this.Redis) {
      if (fResult = this.Redis.get(fKey)) {
        return JSON.parse(fResult);
      }
    } else {
      if (this.Cache.Data[fKey]) {
        return this.Cache.Data[fKey];
      }
    }

    return iDefault;
  },
  /**
 * set Cache
 * @param {*} iKey 
 * @param {*} iData 
 * @param {*} iTableName 
 */
  setCache: function (iKey, iData, iTableName) {
    var fKey = iKey;

    if (this.UseCache) {
      if (this.Redis) {
        this.Redis.set(fKey, JSON.stringify(iData));
      } else {
        this.Cache.Data[fKey] = JSON.stringify(iData);
      }
    }

    return iData;
  },
  setCacheReset: function () {
    this.Cache.Resource = [];
    this.Cache.Pages = [];

    return true;
  },
  echoTrace: function () {
    this.Trace.LastTime = new Date();
  },
  echoTest: function (iMessage, iMessageObject, iDebuglevel, iIsError) {

    if (!this.isTest) {
      return;
    }
    if (iDebuglevel && iDebuglevel == -1) {
      this.Trace.LastTime = new Date();
    }
    if (!this.Trace.LastTime) this.Trace.LastTime = new Date();
    var fNow = new Date();
    var fSeconds = (fNow.getTime() - this.Trace.LastTime.getTime()) / 1000;
    var fMessage = "[" + fSeconds.toFixed(5).toString() + "] - " + iMessage;
    //DebugLevel Test
    this.Trace.LastTime = fNow;
    this.Trace.Text = fMessage;
    this.Trace.Stack.push(fMessage);

    if ((!iDebuglevel) || (iDebuglevel <= this.DebugLevel)) {
      if (iMessageObject) {
        console.log(fMessage, iMessageObject);
      } else {
        console.log(fMessage);
      }
    }
  },
  echoError: function (iMessage, iMessageObject) {
    console.log("ERROR!!!");

    if (iMessageObject)
      console.log(iMessage, iMessageObject);
    else
      console.log(iMessage);

    return true;
  },
  setLicence: function (iFiles) {
    var fFileHash = "";
    var self = this;
    if (!iFiles) {
      iFiles = [__filename];
    }
    if (iFiles) {
      for (var i = 0; i < iFiles.length; i++) {
        //__filename
        fs.readFileSync(iFiles[i], function (err, data) {
          if (err) {
            self.echoError('Licence error', err);
          } else {
            fFileHash += checksum(data);
          }
        });
      }
    }
    this.Licence = {
      hash: fFileHash
    }

    return true;
  },
  setHttp: function (iBindHttp) {
    if ((this.Config) && (this.Config.Server.Http) && (this.Config.Server.Http.Secure) && (this.Config.Server.Http.Https)) {
      this.http = require('https').createServer(this.getSecureOptions(), iBindHttp)
    } else {
      this.http = require("http").createServer(iBindHttp);
    }

    return true;
  },
  setIO: function (iBindSocket) {
    this.io = require('socket.io')(this.http);
    this.io.on('connection', iBindSocket);

    return true;
  },
  setPeer: function (iBindPeer, iPeerServer) {
    var fConfig = {
      port: this.getConfig("Server.Peer.Port", 9000),
      path: '/peerjs'/*,
      ssl: {
          key: fs.readFileSync('./../certificates/key.pem', 'utf8'),
          cert: fs.readFileSync('./../certificates/cert.pem', 'utf8')
      }*/
    };
    if (iPeerServer) {
      this.peer = iPeerServer ? iPeerServer(fConfig) : require('peer').PeerServer(fConfig);
    } else {
      this.peer = require('peer').PeerServer(fConfig);
    }
    if (iBindPeer) {
      iBindPeer(this.peer);
    }
    return true;
  },
  getConfig: function (iName, iDefault) {
    if ((this.Config) && (iName) && (0 < iName.indexOf('.'))) {
      var fItems = iName.split('.');
      var fConfig = this.Config;

      for (i = 0; i < fItems.length; i++) {
        var fKey = fItems[i];
        if (typeof (fConfig[fKey]) != 'undefined') {
          fConfig = fConfig[fKey];
          if (i == fItems.length - 1) {
            return fConfig;
          }
        } else {
          return iDefault;
        }
      }
    }
    if ((this.Config) && (iName) && (typeof (this.Config[iName]) != 'undefined')) {
      return this.Config[iName];
    }

    return iDefault;
  },
  setEvent: function (iEvent, iClients) {
    //this.io.emit('event', iEvent, { UserId: "Event" });
    return "";
  },
  isSaveAction: function (iData) {
    return ((iData) && (iData.Action) && (iData.Action.toLowerCase() == "save"));
  },
  getDataNameStart: function (iData) {
    fResult = "";
    if (iData.Name) {
      fResult = iData.Name.split('-')[0];
    }

    return fResult;
  },
  getFileHash: function(iFileName){
    try {
      var md5sum = crypto.createHash('md5');
      md5sum.update(fs.readFileSync(iFileName.replace("./", "js/")));
      var d = md5sum.digest('hex');

      return d.toString(); 
    } catch (err) {
      console.log('Error getFileHash:', err);
    }

    return "";
  },
  setStorageInit: function(){
    var fDirInt = this.getConfig('Storage.internal.Path', "/data/test/");
    if (!fs.existsSync(fDirInt)){
      fs.mkdirSync(fDirInt);
    }

    var fDirAuth = this.getConfig('Auth.Path', "/data/auth/");
    if (!fs.existsSync(fDirAuth)){
      fs.mkdirSync(fDirAuth);
    }
    /*try {
      this.Redis = redis.createClient();
    } catch (err) {
      console.log('Error redis:', err);
    }*/

    return true;
  },
  /**
   * Data fetch core function
   * @param {string} iTableName 
   * @param {object} iDefault Return object
   * @param {object} iRow Item to sync
   * @param {string} iPath 
   */
   getData: function (iTableName, iDefault, iRow, iPath) {
    var fPath;
    if (iPath) { 
      fPath = iPath; 
    }else{
      fPath = (this.Config.Storage.default.Path) ? this.Config.Storage.default.Path : 'data/';
    }

    if(this.TableCompression){
      var fZipFile = this.getDataZipName(iTableName, iRow, fPath);
      if(iRow&&iRow.guid){
        return this.getFileZip(fZipFile, iRow.guid, iDefault);
      }else{
        return this.getFileZip(fZipFile, 'list', iDefault);
      }
    }else{
      var fRowFile = this.getDataFileName(iTableName, iRow, fPath);
      return this.getFileJson(fRowFile, iDefault);
    }

    
    var fTableFile = fPath + iTableName + this.TableExt;

    if (fs.existsSync(fTableFile)) {
      return this.getFileJson(fTableFile, iDefault);
    } else {
      console.log('Error file not found', fTableFile);
    }

    return iDefault;
  },  
  /**
   * Data fetch core function
   * @param {string} iTableName 
   * @param {object} iDefault Return object
   * @param {object} iRow Item to sync
   * @param {string} iPath 
   */
  getDataOld: function (iTableName, iDefault, iRow, iPath) {
    var fPath = (this.Config.Storage.default.Path) ? this.Config.Storage.default.Path : 'data/';
    if (iPath) { fPath = iPath; }

    // if (iRow && typeof (iRow.Page) !== "undefined" && Number.isInteger(iRow.Page)) {
    //   var fTableFile = fPath + iTableName + "/" + iTableName + '-p-' + iRow.Page.toString() + this.TableExt;
    //   if (fs.existsSync(fTableFile)) {
    //     return this.getFileJson(fTableFile, {});
    //   } else {
    //     return iDefault;
    //   }
    // }

    /*if (iRow && iRow.guid) {
      var fRowFile = fPath + iTableName + "/" + iRow.guid + this.TableExt;
      if (fs.existsSync(fRowFile)) {
        return this.getFileJson(fRowFile, {});
      }

      return {};
    }*/

    if(this.TableCompression){
      var fZipFile = this.getDataZipName(iTableName, iRow, iPath);
      if(iRow&&iRow.guid){
        return this.getFileZip(fZipFile, iRow.guid, iDefault);
      }else{
        return this.getFileZip(fZipFile, 'list', iDefault);
      }
    }else{
      var fRowFile = this.getDataFileName(iTableName, iRow, iPath);
      return this.getFileJson(fRowFile, iDefault);
    }

    
    var fTableFile = fPath + iTableName + this.TableExt;

    if (fs.existsSync(fTableFile)) {
      return this.getFileJson(fTableFile, iDefault);
    } else {
      console.log('Error file not found', fTableFile);
    }

    return iDefault;
  },
  setMongo: function (iStorageName, iTableName, iRow) {
    if (this.Config.Storage[iStorageName].Driver) {
      var fLogName = this.Config.Storage[iStorageName].Query;
      var fRow = this.getClone(iRow);
      fRow._id = iRow.guid;
      var self = this;
      require(this.Config.Storage[iStorageName].Driver).connect(this.Config.Storage[iStorageName].url, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        authSource: 'dd'
      }).then(function (client) {
        console.log('Mongo db connected');
        var db = client.db();
        try {
          db.createCollection(iTableName);
        } catch (errCol) {
          console.log('Error setMongo.createCollection:', errCol, iTableName);
        }
        db.collection(iTableName).findOneAndReplace({ _id: iRow.guid }, fRow, { upsert: true, returnOriginal: false }).then(function (errUpd, resUpd) {
          if (errUpd && !errUpd.ok) {
            console.log('Error setMongo.findOneAndReplace:', errUpd, resUpd);
          }
          if (errUpd.ok) {
              /**/ self.echoTest('Mongo saved', errUpd, 5); /**/
          }
        });
        fRow.tablename = iTableName;
        db.collection(fLogName).findOneAndReplace({ _id: iRow.guid }, fRow, { upsert: true, returnOriginal: false }).then(function (errUpd, resUpd) {
          if (errUpd && !errUpd.ok) {
            console.log('Error setMongo.findOneAndReplace:', errUpd, resUpd);
          }
        });
      }).catch(function (err) {
        //console.log(Error, err.message);
        console.log('Error setMongo.connect:', err);
      });
    }
  },
  setMysql: function (iStorageName, iTableName, iRow) {
    try {
      var sql = (this.Config.Storage[iStorageName].Query) ? this.Config.Storage[iStorageName].Query : "REPLACE INTO dd_main(`type`, `guid`, `rev`, `data`, `date`)VALUES('@type', '@guid', '@rev', '@data', '@date')";

      if (this.Config.Storage[iStorageName].Driver) {
        //var mysql = require('mysql');
        if (!this.Config.Storage[iStorageName].Pool) {
          this.Config.Storage[iStorageName].Pool = require(this.Config.Storage[iStorageName].Driver).createPool({
            host: this.Config.Storage[iStorageName].host,
            user: this.Config.Storage[iStorageName].user,
            password: this.Config.Storage[iStorageName].password,
            database: this.Config.Storage[iStorageName].database
          });
        }

        this.Config.Storage[iStorageName].Pool.getConnection(function (err, connection) {
          if (err) {
            console.log('Error MySQL', err);
          }

          if (connection) {
            sql = sql.replace('@type', connection.escape(iTableName));
            sql = sql.replace('@guid', connection.escape(iRow.guid));
            sql = sql.replace('@rev', connection.escape(iRow.rev)).replace('@rev', connection.escape(iRow.rev));
            sql = sql.replace('@data', connection.escape(JSON.stringify(iRow))).replace('@data', connection.escape(JSON.stringify(iRow)));
            sql = sql.replace('@date', connection.escape(iRow.date)).replace('@date', connection.escape(iRow.date));
            if(iRow.deleted){
              sql = sql.replace('@state', connection.escape('D')).replace('@state', connection.escape('D'));
            }else{
              sql = sql.replace('@state', 'NULL');
            }
            if(iRow.puid){
              sql = sql.replace('@puid', connection.escape(iRow.puid)).replace('@puid', connection.escape(iRow.puid));
            }else{
              sql = sql.replace('@puid', 'NULL');
            }
            if(iRow.session&&iRow.session.SessionUID){
              sql = sql.replace('@suid', connection.escape(iRow.session.SessionUID.toString().replace('-', ''))).replace('@suid', connection.escape(iRow.session.SessionUID.toString().replace('-', '')));
            }else{
              sql = sql.replace('@suid', 'NULL');
            }
            //console.log('Sql:', sql);

            connection.query(sql, function (errQ, resultsQ, fieldsQ) {
              connection.release();

              if (errQ) {
                console.log('Error:', sql, errQ);
              }
            });
          }
        });
      }
    } catch (err) {
      console.log('Error', err);
    }
  },
  setDataMap: function (iTableName, iIPP, iCallback) {
    var fPath = (this.Config.Storage.default.Path) ? this.Config.Storage.default.Path : 'data/';
    var fTableName = iTableName;
    var fTableExt = this.TableExt;
    var fIPP = (iIPP) ? iIPP : 50;
    dir = fPath + fTableName + "/";
    fs.readdir(dir, function (err, files) {
      if (err) {
        console.log("Error", err);
      }
      if (files && Array.isArray(files)) {
        files = files.map(function (fileName) {
          return {
            name: fileName,
            time: fs.statSync(dir + '/' + fileName).mtime.getTime()
          };
        }).sort(function (a, b) {
          return b.time - a.time;
        }).map(function (v) {
          return v.name;
        });

        var fCP = 0;
        var fLP = 0;
        var fI = 0;
        var fContent = "";

        for (i = 0; i < files.length; i++) {
          if (files[i].indexOf('-p-') == -1) {
            var fFileName = files[i];
            fCP = Math.trunc(fI / fIPP);
            fI++;
            if (fLP != fCP) {
              fs.writeFileSync(dir + iTableName + '-p-' + fLP + fTableExt, '[' + fContent + ']');
              if (fLP == 0) {
                fs.writeFileSync(fPath + iTableName + fTableExt, '[' + fContent + ']');
              }
              fContent = "";
              fLP = fCP;
            }
            fContent += ((fContent) ? "," : "") + fs.readFileSync(dir + fFileName, 'utf8');
            //console.log(fFileName);
          }
        }
        fs.writeFileSync(dir + iTableName + '-p-' + fLP + fTableExt, '[' + fContent + ']');
      }

      if (iCallback) iCallback();
    });
  },
  getDataFileName: function(iTableName, iRow, iPath){
    var fPath = (iPath) ? iPath : this.getConfig('Storage.default.Path', 'data/');
    var fTableFile = fPath + iTableName;
    
    if(iRow){
      fTableFile += '/';
      if(iRow.puid){
        fTableFile += '/' + iRow.puid;
      }
      if(iRow.guid){
        fTableFile += '/' + iRow.guid;
      }
    } 
    fTableFile += this.TableExt;
    
    return fTableFile;
  },
  existsData: function (iTableName, iRow, iPath) {
    try {
      if(this.TableCompression){
        var fZipFile = this.getDataZipName(iTableName, iRow, iPath);
        return this.existFileZip(fZipFile, iRow.guid);
      }else{
        var fRowFile = this.getDataFileName(iTableName, iRow, iPath);
        return fs.existsSync(fRowFile);
      }
    } catch (err) {
      console.log('Error setData', err);
    }

    return false;
  },
  setFolder: function(iFolder){
    if (this.Cache.FolderExists&&iFolder) {
      if (!this.Cache.FolderExists[iFolder]&&!fs.existsSync(iFolder)) {
        fs.mkdirSync(iFolder);
      }
      this.Cache.FolderExists[iFolder] = true;
    }

    return true;
  },
  setDataFolder: function (iTableName, iData, iRow, iPath){
    if (this.Cache.FolderExists) {

      var fPath = (iPath) ? iPath : this.getConfig('Storage.default.Path', 'data/');

      if (!this.Cache.FolderExists[fPath]&&!fs.existsSync(fPath)) {
        fs.mkdirSync(fPath);
      }
      this.Cache.FolderExists[fPath] = true;
      fPath += iTableName + '/';
      if (!this.Cache.FolderExists[fPath]&&!fs.existsSync(fPath)) {
        fs.mkdirSync(fPath);
      }
      this.Cache.FolderExists[fPath] = true;
      
      if(iRow&&iRow.puid){
        fPath += iRow.puid + '/';
      }
      if (!this.Cache.FolderExists[fPath]&&!fs.existsSync(fPath)) {
        fs.mkdirSync(fPath);
      }
      this.Cache.FolderExists[fPath] = true;
    }

    return true;
  },
  setData: function (iTableName, iData, iRow, iAction, iPath) {
    try {
      var fPath = (this.Config.Storage.default.Path) ? this.Config.Storage.default.Path : 'data/';
      if (iPath) { fPath = iPath; }

      if (iRow && iRow.guid && iAction) {
        switch (iAction) {
          case "save":
            iRow.rev = ((iRow.rev) ? parseInt(iRow.rev) : 0) + 1;

            if (iData) {
              iData[iRow.guid] = iRow;
            }

            for (var fStorageName in this.Config.Storage) {
              if (this.Config.Storage[fStorageName] && this.Config.Storage[fStorageName].Status) {
                switch (this.Config.Storage[fStorageName].Type.toLowerCase()) {
                  case "internal":
                    if(this.TableCompression){
                      //this.setDataFolder(iTableName, iData, iRow, iPath);
                      var fZipFile = this.getDataZipName(iTableName, iRow, iPath);
                      this.setFileZip(fZipFile, iRow.guid, iRow);
                    }else{
                      this.setDataFolder(iTableName, iData, iRow, iPath);
                      var fRowFile = this.getDataFileName(iTableName, iRow, iPath);
                      this.setFileJson(fRowFile, iRow);
                    }
                    break;
                  case "mysql":
                    this.setMysql(fStorageName, iTableName, iRow);
                    break;
                  case "mongo":
                    this.setMongo(fStorageName, iTableName, iRow);
                    break;
                }
              }
            }
            break;
          case "delete":
            iRow.rev = ((iRow.rev) ? parseInt(iRow.rev) : 0) + 1;
            iRow.deleted = true;
            /*if (fs.existsSync(fPath + fTableName)) {
              var fRowFile = this.getPath(fPath, fTableName, iRow.guid);//fPath + fTableName + "/" + iRow.guid + this.TableExt;
              this.setFileJson(fRowFile, iRow);
            }*/
            if (iData && iData[iRow.guid]) {
              delete (iData[iRow.guid]);
            }
            break;
        }
      }
      if (iData) {
        if(this.TableCompression){
          //this.setDataFolder(iTableName, iData, iRow, iPath);
          var fZipFile = this.getDataZipName(iTableName, iRow, iPath);
          this.setFileZip(fZipFile, 'list', iData);
        }else{
          var fTableFile = this.getDataFileName(iTableName, undefined, iPath);
          console.log('Data save', fTableFile);
          this.setFileJson(fTableFile, iData);
        }
      }
    } catch (err) {
      console.log('Error setData:', err);
    }

    return iData;
  },
  getDataItem: function (iDefault) {
    var fDefault = (iDefault) ? iDefault : {};
    fDefault.guid = this.getGuid();
    return fDefault;
  },
  getSources: function (iForced) {
    if ((fs.existsSync(this.SourcesFile)) && (iForced || !this.Cache || !this.Sources || !this.Sources.length)) {
      this.Sources = this.getFileJson(this.SourcesFile, this.Sources);
      this.Sources = this.getObject(this.Sources, 'guid');
    }

    return this.Sources;
  },
  setSources: function (iSources) {
    if (iSources) {
      this.Sources = this.getClone(iSources);
    }
    if (this.Sources) {
      this.setFileJson(this.SourcesFile, this.Sources);
    }

    return this.Sources;
  },
  getSecureOptions: function () {
    var fResult = {};
    if (fSecure = this.Config.Server.Http.Secure) {

      if (fSecure.key) {
        fResult.key = fs.readFileSync(fSecure.key);
      }
      if (fSecure.cert) {
        fResult.cert = fs.readFileSync(fSecure.cert);
      }
      if (fSecure.ca) {
        fResult.ca = fs.readFileSync(fSecure.ca);
      }
      if (fSecure.requestCert) {
        fResult.requestCert = fSecure.requestCert;
      }
      if (fSecure.rejectUnauthorized) {
        fResult.rejectUnauthorized = fSecure.rejectUnauthorized;
      }
    }

    return fResult;
  },
  getUser: function (iAuthType, iAuthData, iWrite) {
    var fType = iAuthType.toLowerCase().trim();
    var fData = iAuthData;
    var fDataFile = btoa(fData);
    var fUser = this.getMergeUser();
    var fFileAuth = this.getPath(this.Config.Auth.Path, fType, fDataFile);//this.Config.Storage.default.Path + "auth/" + fType + "/" + fDataFile + this.TableExt;
    if (fs.existsSync(fFileAuth)) {
      var fUserId = this.getFile(fFileAuth);
      var fFileUser = this.getPath(this.Config.Auth.Path, "user", fUserId);//this.Config.Storage.default.Path + "auth/user/" + fUserId + this.TableExt;
      fUser = this.getFileJson(fFileUser, fUser);
    } else {
      fUser.emails.push(iAuthData);
      iWrite = true;
    }

    if (iWrite) {
      fUser = this.getMergeUser(fUser);
      fUser.Auths[fType] = {
        Status: true,
        Id: fDataFile,
        Auth: fType
      }

      var fFileUser = this.getPath(this.Config.Auth.Path, "user", fUser.guid);//this.Config.Storage.default.Path + "auth/user/" + fUser.guid + this.TableExt;
      this.setFileJson(fFileUser, fUser);
      this.setFile(fFileAuth, fUser.guid);
    }

    return fUser;
  },
  getMergeUser: function (iA1, iA2) {
    var fResult = {};
    var fDefUser = {
      guid: undefined,
      rev: 0,
      state: "",
      firstname: "",
      familyname: "",
      image: "",
      sex: "",
      birthdate: "",
      birthplace: "",
      email: "",
      education: "",
      emails: [],
      points: 0,
      Auths: {
      }
    };

    if (!iA1 && !iA2) {
      return fDefUser;
    }

    var chkProp = function (iObj1, iObj2, iProp) {
      if (!iObj1[iProp] && iObj2[iProp]) {
        iObj1[iProp] = iObj2[iProp];
      }

      return iObj1;
    }

    if (typeof (iA1) == 'object') {
      fResult = this.mergeArray(fResult, iA1);
    }

    if (typeof (iA2) == 'object') {
      fResult = chkProp(fResult, iA2, "guid");
      fResult = chkProp(fResult, iA2, "state");
      fResult = chkProp(fResult, iA2, "firstname");
      fResult = chkProp(fResult, iA2, "familyname");
      fResult = chkProp(fResult, iA2, "image");
      fResult = chkProp(fResult, iA2, "birthdate");
      fResult = chkProp(fResult, iA2, "birthplace");
      fResult = chkProp(fResult, iA2, "sex");
      fResult = chkProp(fResult, iA2, "email");
      fResult = chkProp(fResult, iA2, "emails");
    } else {
      fResult = chkProp(fResult, fDefUser, "guid");
      fResult = chkProp(fResult, fDefUser, "state");
      fResult = chkProp(fResult, fDefUser, "firstname");
      fResult = chkProp(fResult, fDefUser, "familyname");
      fResult = chkProp(fResult, fDefUser, "image");
      fResult = chkProp(fResult, fDefUser, "birthdate");
      fResult = chkProp(fResult, fDefUser, "birthplace");
      fResult = chkProp(fResult, fDefUser, "sex");
      fResult = chkProp(fResult, fDefUser, "email");
      fResult = chkProp(fResult, fDefUser, "emails");

      if ((fResult) && (typeof (fResult.guid) === "undefined" || !fResult.guid)) {
        fResult.guid = this.getGuid();
      }
    }

    return fResult;
  },
  initSocket: function (iSocketId, iSocket) {
    var fSocketId = iSocketId;

    if ((fSocketId) && (this.Sockets[fSocketId])) {
      return this.Sockets[fSocketId];
    }

    var fClient = {};
    try {
      fClient = JSON.parse(decodeURIComponent(escape(atob(iSocket.handshake.query.Client))));
      //console.log('initSocket Client param', fClient);
    } catch (err) {
      console.log('Error:', err);
    }

    var fReturn = {
      id: fSocketId,
      session: fClient
    };
    /*if (fClient.CookieUID && (fCookie = this.Cookies[fClient.CookieUID])) {
      if (fCookie.UserUID && (fUser = this.Users[fCookie.UserUID])) {
        fReturn.session.UserAuth = fUser.UserAuth;
        fReturn.session.UserData = fUser.UserData;
        fReturn.session.UserEmail = fUser.UserEmail;
        fReturn.session.UserUID = fCookie.UserUID;
      }
    }*/

    if ((this.Config) && (this.Config.Routing) && (this.Config.Routing.host) && (iSocket.handshake.headers.host) && (this.Config.Routing.host[iSocket.handshake.headers.host])) {
      var fSiteKey = this.Config.Routing.host[iSocket.handshake.headers.host];
      if ((this.Config.Routing.site) && (this.Config.Routing.site[fSiteKey]) && (this.Config.Routing.site[fSiteKey].Config)) {
        fReturn.Config = this.Config.Routing.site[fSiteKey].Config;
      }
    }

    this.Sockets[fSocketId] = fReturn;
    if (!this.Cookies[fClient.CookieUID]) {
      this.Cookies[fClient.CookieUID] = {};
    }

    this.Cookies[fClient.CookieUID][fSocketId] = true;
    this.Users[fReturn.session.UserUID] = fReturn.session.UserData;
    //console.log('initSocket Result', fReturn);
    return fReturn;
  },
  setSocket: function (iSocketId, iSocket) {
    if ((iSocketId) && (this.Sockets[iSocketId])) {
      //this.Sockets[fSocketId] = 
    }
  },
  getSocket: function (iSocketId) {
    if ((iSocketId) && (this.Sockets[iSocketId])) {
      return this.Sockets[iSocketId];
    }
  },
  delSocket: function (iSocketId) {
    if ((iSocketId) && (this.Sockets[iSocketId])) {
      delete (this.Sockets[iSocketId]);
    }
  },
  setUser: function (iUser, iRemove) {
    var fUser = iUser;
    if (fUser.InfoUID) {
      fInfoUID = fUser.InfoUID;

      if (!this.Infos[fInfoUID]) {
        this.Infos[fInfoUID] = {};
      }

      if (this.Infos[fInfoUID][fUser.UserUID]) {
        if (iRemove) {
          delete (this.Infos[fInfoUID][fUser.UserUID]);
        } else {
          this.Infos[fInfoUID][fUser.UserUID] = fUser;
        }
      } else {
        this.Infos[fInfoUID][fUser.UserUID] = fUser;
      }
    }
    if (fUser.SocketId) {
      this.Sockets[fUser.SocketId] = fUser;
    }
  },
  existFile: function (iFile) {
    if (fs.existsSync(iFile)) {
      return true;
    }

    return false;
  },
  getFile: function (iFilename, iEncoding) {
    var fContent;
    var fEncoding = ((iEncoding) ? iEncoding : "utf8");

    if (fs.existsSync(iFilename)) {
      try {
        fContent = fs.readFileSync(iFilename, fEncoding);
      }
      catch (err) {
        console.log('Error', iFilename, err);
      }
    } else {
      this.echoTest('File not found:', iFilename);
    }

    return fContent;
  },
  setFile: function (iFilename, iContent, iEncoding) {
    try {
      var fEncoding = ((iEncoding) ? iEncoding : "utf8");
      fs.writeFileSync(iFilename, iContent, fEncoding);
      //console.log('File saved', iFilename);
    } catch (err) {
      console.log('ERROR setFile', err);
      return false;
    }

    return true;
  },
  getFileJson: function (iFileName, iDefault, iEncoding) {
    var fEncoding = ((iEncoding) ? iEncoding : "utf8");
    /**/ this.echoTest('Loading ', iFileName, 8); /**/
    if (fTxt = this.getFile(iFileName, fEncoding)) {
      try {
        return JSON.parse(fTxt);
      } catch (err) {
        console.log("Error:", iFileName, err);
      }
    } else {
      console.log("Error getFileJson not found", iFileName);
    };

    return iDefault;
  },
  setFileJson: function (iFileName, iContent) {
    var fContent = JSON.stringify(iContent);
    this.setFile(iFileName, fContent);
  },
  getDataZipName: function(iTableName, iRow, iPath){
    var fPath = (iPath) ? iPath : this.getConfig('Storage.default.Path', 'data/');
    var fTableFile = fPath + iTableName + '/';
    
    if(iRow&&iRow.puid){
      fTableFile += '/' + iRow.puid;
    }else{
      fTableFile += '/' + iTableName;
    }
    fTableFile += '.zip';
    
    return fTableFile;
  },
  existFileZip: function (iZipFile, iFilename) {
    if (fs.existsSync(iZipFile)) {
      const archiver = require('adm-zip');
      var zip = new archiver(iZipFile);
     
      if(fEntry = zip.getEntry(iFilename + '.json')){
        return true;
      }
    }

    return false;
  },
  setFileZip: function (iZipFile, iFilename, iContent) {
    const archiver = require('adm-zip');
    var zip;
    if (fs.existsSync(iZipFile)) {
      zip = new archiver(iZipFile);
    }else{
      zip = new archiver();
    }
   
    if(fEntry = zip.getEntry(iFilename + '.json')){
      zip.updateFile(iFilename + '.json', Buffer.from(JSON.stringify(iContent), "utf8"));
    }else{
      zip.addFile(iFilename + '.json', Buffer.from(JSON.stringify(iContent), "utf8"), "entry comment goes here");
    }
    zip.writeZip(iZipFile);

    return true;
  },
  getFileZip: function (iZipFile, iFilename, iDefault) {
    if (fs.existsSync(iZipFile)) {
      const archiver = require('adm-zip');
      var zip = new archiver(iZipFile);
    
      if(iFilename){
        if(fEntry = zip.getEntry(iFilename + '.json')){
          return JSON.parse(zip.readAsText(iFilename + '.json'));
        } 
      }else{
        var fData = [];
        var zipEntries = zip.getEntries();

        zipEntries.forEach(function (zipEntry) {
          fData.push(JSON.parse(zipEntry.getData().toString("utf8")));
        });
        return fData;
      }
    }

    return iDefault;
  },
  setMail(iRow) {
    var fDefaultMail = (this.Config.Server && this.Config.Server.Mail) ? this.Config.Server.Mail : {
      host: 'mailcluster.loopia.se', port: 587, secure: false,
      auth: {
        user: 'auth@dd.in.rs',
        pass: 'auth4123!'
      }
    };
    var transporter = (iRow.MailService) ? iRow.MailService : mail.createTransport(fDefaultMail);

    var mailOptions = {
      from: iRow.MailFrom,
      to: iRow.MailTo,
      subject: iRow.MailSubject,
      text: iRow.MailBody
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log('Error mail:', error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  },
  setFilter: function (iTable, iSection, iSession) {
    var fTable = {};
    var isView = function (iSection, iItem, iSession) {
      if (iItem && (!iItem.visibility || iItem.visibility == "open" || iItem.visibility == "registration")) {
        return true;
      }

      if (iSession && iSession.UserUID && iItem.session && iItem.session.UserUID && iSession.UserUID == iItem.session.UserUID) {
        return true;
      }

      if (iSession && iSession.UserUID && iItem.session && iItem.session.OwnerUID && iSession.UserUID == iItem.session.OwnerUID) {
        return true;
      }

      if (iSection) {
        if (iSession && iSession.UserAdmin && iSession.UserAdmin[iSection.toLowerCase()]) {
          return true;
        }
      }

      if (iItem && iItem.moderators) {
        if (iSession && iSession.UserEmail && -1 != iItem.moderators.indexOf(iSession.UserEmail)) {
          return true;
        }
      }

      if (iItem && iItem.parcipients) {
        if (iSession && iSession.UserEmail && -1 != iItem.parcipients.indexOf(iSession.UserEmail)) {
          return true;
        }
      }

      return false;
    }

    for (var fKey in iTable) {
      if (isView(iSection, iTable[fKey], iSession)) {
        fTable[fKey] = iTable[fKey];
      }
    }

    return fTable;
  },
  getWordStat: function (iData, iWords) {
    var fData = {};
    if (typeof (iData) === "object") {
      fData = iData;
    }
    var fWords = iWords.split(' ');
    for (var fKey in fWords) {
      var fWord = fWords[fKey].toString().toLowerCase();
      if (2 < fWord.length) {
        var fCount = ((fWord && fData[fWord] && fData[fWord].Count) ? fData[fWord].Count : 0) + 1;
        fData[fWord] = { Word: fWord, Count: fCount };
      }
    }

    return fData;
  },
  mergeArray: function (iA, iB) {
    var fResult = iA;
    try {
      fResult = Object.assign(iA, iB);
    } catch (err) {
      for (var key in iB) {
        iA[key] = iB[key];
      }
    }

    return fResult;
  },
  getMerge: function (iA1, iA2, iA3) {
    var fResult = {};

    if (typeof (iA1) == 'object') {
      fResult = this.mergeArray(fResult, iA1);
    }

    if (typeof (iA2) == 'object') {
      fResult = this.mergeArray(fResult, iA2);
    }

    if (typeof (iA3) == 'object') {
      fResult = this.mergeArray(fResult, iA3);
    }

    return fResult;
  },
  getObject: function (iValue, iKey) {

    if (Array.isArray(iValue)) {
      var fResult = {};

      for (i = 0; i < iValue.length; i++) {
        var fKey = iValue[i][iKey];
        fResult[fKey] = iValue[i];
      }

      return fResult;
    }

    return iValue;
  },
  getArray: function (iData, field, reverse, primer) {
    var fData = [];
    var fKeepProp = false;
    if (Array.isArray(iData)) {
      fData = iData;
    } else {
      for (var key in iData) {
        fData.push(iData[key]);
        fKeepProp = true;
      }
    }

    reverse = !reverse ? 1 : -1;
    var key = primer ?
      function (x) { return primer(x[field]) } :
      function (x) { return x[field] };

    fData.sort(function (a, b) {
      var a = key(a);//.ChangeDate;
      var b = key(b);//.ChangeDate;
      return reverse * ((a > b) - (b > a));
    });

    if (fKeepProp) {
      var fTemp = {};
      for (i = 0; i < fData.length; i++) {
        //fTemp[fData[i].PageUID] = fData[i]; 
      }

      //fData = fTemp;
    }

    return fData;
  },
  getClone: function (obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  },
  getShort: function (iLength) {
    if (!iLength) iLength = 4;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < iLength; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },
  getPin: function (iLength) {
    if (!iLength) iLength = 4;
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < iLength; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },
  getGuid: function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  },
  getMD5: function(iValue){
    const crypto = require('crypto');
    return crypto.createHash('md5').update(iValue).digest("hex");
  },
  getStorage: function (iConfig, iTableName, iAddDir) {
    var fPath = 'data/';

    if (iConfig && iConfig.Trace && iConfig.Trace.Config && iConfig.Trace.Config.Storage.default.Path) {
      fPath = iConfig.Trace.Config.Storage.default.Path;
    } else {
      if (this.Config.Storage.default.Path) {
        fPath = this.Config.Storage.default.Path;
      }
    }

    if (iAddDir) {
      fPath = this.getPathMarge(fPath, iAddDir);
    }

    return fPath;
  },
  getPathMarge(iA1, iA2, iMake) {
    var fPath = iA1;

    if (iMake && !fs.existsSync(fPath)) {
      fs.mkdirSync(fPath);
    }
    fPath = iA1 + iA2 + "/";
    fPath = fPath.replace("//", "/").replace("//", "/").replace("//", "/");
    if (iMake && !fs.existsSync(fPath)) {
      fs.mkdirSync(fPath);
    }
    return fPath;
  },
  getPathFile: function (iA1, iA2, iA3, iA4, iA5) {
    var fPath = iA1;

    if (iA2) { fPath = this.getPathMarge(fPath, iA2, true); }
    if (iA3) { fPath = this.getPathMarge(fPath, iA3, true); }
    if (iA4) { fPath = this.getPathMarge(fPath, iA4, true); }
    if (iA5) { fPath = this.getPathMarge(fPath, iA5, true); }

    return fPath;
  },
  getPath2: function (iA1, iA2, iA3, iA4, iA5) {
    var fPath = iA1;

    if (iA2) { fPath = this.getPathMarge(fPath, iA2, true); }
    if (iA3) { fPath = this.getPathMarge(fPath, iA3, true); }
    if (iA4) { fPath = this.getPathMarge(fPath, iA4, true); }
    if (iA5) { fPath = this.getPathMarge(fPath, iA5, true); }

    return fPath;
  },
  getPath: function (iA1, iA2, iA3) {
    var fFilePath = this.TableExt;

    if (!fs.existsSync(iA1)) {
      fs.mkdirSync(iA1);
    }

    if (iA3) {
      var fFileDir = iA1 + '/' + iA2 + '/';
      if (!fs.existsSync(fFileDir)) {
        fs.mkdirSync(fFileDir);
      }

      fFilePath = fFileDir + iA3 + fFilePath;
    } else {
      if (iA2) {
        fFilePath = iA2 + fFilePath;
      }
      if (iA1) {
        fFilePath = iA1 + fFilePath;
      }
    }

    return fFilePath;
  },
  getUuid: function () {
    return this.getShort(12).toLowerCase();
  },
  getDate: function (iValue) {
    if (iValue) {
      return new Date(iValue).toJSON();
    } else {
      return new Date().toJSON();
    }
  },
  getDateYMDShort: function (date) {
    var d = ((date) ? new Date(date) : new Date()),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = '' + d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('');
  },
  getBuffer: function (iValue) {
    return new Buffer(JSON.stringify(iValue));
  },
  isIn: function (iArray, iFind) {
    return (iArray.toString().toLowerCase().indexOf(iFind.toLowerCase()) != -1);
  },
  getValue: function (iRow, iPos, iDefault) {
    if (iRow && Array.isArray(iRow) && iPos < iRow.length) {
      return iRow[iPos];
    }

    return (iDefault) ? iDefault : "";
  },
  isEmpty: function (iValue) {
    return !(iValue);
  },
  isNotEmpty: function (iValue) {
    return ((iValue) && (iValue.toString().trim() != ""));
  },
  getTextClean: function (str) {
    if ((str === null) || (str === ''))
      return false;
    else
      str = str.toString();

    return str.replace(/<[^>]*>/g, '');
  },
  getBase64: function (iValue) {
    if (iValue) {
      return btoa(iValue);
    }

    return iValue;
  },
  startsWith: function (iValue, iStartChar) {
    if (typeof (iValue) == "undefined") {
      return false;
    }
    try {
      return iValue.startsWith(iStartChar);
    } catch (err) {
      return (iValue.indexOf(iStartChar) == 0);
    }
  },
  Event: function (iAction, iData, iFrom, iTo) {
    this.io.emit('event', iAction, iData, iFrom, iTo);
  },
  Sync: function (iData, iCallback, iSocket) {
    var fResult;
    var self = this;
		/**/ this.echoTest('Buildin.Sync', iData, 9); /**/
    switch (iData.Name) {
      case "Buildin-Config":
        switch (iData.Action) {
          case "save":
          break;
        }
        try {
          fResult = {};
          fResult.session = this.getClone(this.Config.session);
          fResult.Version = this.getClone(this.Config.Version);
          fResult.Licence = this.getClone(this.Config.Licence);
          fResult.Server = this.getClone(this.Config.Server);
          fResult.Vendor = this.getClone(this.Config.Vendor);
          fResult.Client = this.getClone(this.Config.Client);
          fResult.Storage = {};
          for (var fKey in this.Config.Storage) {
            fResult.Storage[fKey] = { Status: this.Config.Storage[fKey].Status, Type: this.Config.Storage[fKey].Type };
          }
          fResult.Auth = this.getClone(this.Config.Auth);
          fResult.Sources = this.getClone(this.Config.Sources);
          fResult.Service = this.getClone(this.Config.Service);
          fResult.Routing = this.getClone(this.Config.Routing);

          fResult.Stats = this.getClone(this.Stats);
          fResult.Stats.UpTime = parseInt(((new Date().getTime()) - this.Stats.UpTime) / 1000);
          try {
            if ((fResult.Stats.MemorySize == 0) && (fRes = process.memoryUsage())) {
              fResult.Stats.MemorySize = (parseInt(fRes.heapTotal) / 1024 / 1024);
              fResult.Stats.MemoryUsed = (parseInt(fRes.heapUsed) / 1024 / 1024);
              fResult.Stats.MemoryAll = (parseInt(fRes.external) / 1024 / 1024);
              delete (fRes);
            }
          }
          catch (err) {
            console.log('Error heapUsed', err);
          }

          if (this.Redis) {
            this.Redis.info(function (err, res) {
              //console.log(res); 
              res.split("\n").map((line) => {
                if (line.match(/used_memory_human/)) {
                  self.Stats.RedisMemoryUsed = line.split(":")[1];
                }
              });
            });
          }

          fResult.Live = {
            Sockets: this.getClone(this.Sockets),
            Users: this.getClone(this.Users),
            Cookies: this.getClone(this.Cookies)
          }

        }
        catch (err) {
          console.log('Error', err);
        }
        break;
      case "Buildin-Sources":
        switch (iData.Action) {
          case "detail":
            fResult = this.Sources[iData.Row.guid]; //this.getData('sources', this.Sources, iData.Row);
            break;
          case "insert":
          case "update":
          case "save":
          case "delete":
            this.Sources = this.setData('sources', this.Sources, iData.Row, iData.Action);
            fResult = this.getClone(this.Sources);
            break;
          default:
            fResult = this.getClone(this.Sources);
            break;
        }
        break;
      case "Buildin-User":
        switch (iData.Action) {
          case "detail":
            var fFileUser = this.Config.Storage.default.Path + "auth/user/" + iData.Row.guid + this.TableExt;
            fResult = this.getFileJson(fFileUser, iData.Row);
            break;
          case "insert":
          case "update":
          case "save":
          case "delete":
            var fRow = this.getMergeUser(iData.Row);
            fRow.rev = (fRow.rev) ? fRow.rev + 1 : 1;
            var fFileUser = this.Config.Storage.default.Path + "auth/user/" + fRow.guid + this.TableExt;
            this.setFileJson(fFileUser, fRow);
            if (this.Users[fRow.guid]) {
              if (fRow.firstname) {
                this.Users[fRow.guid].UserName = fRow.firstname;
              }

              this.Users[fRow.guid].UserData = fRow;
            }
            var fSocket = this.getSocket(iSocket.id, iSocket);
            iSocket.emit('auth', fSocket, this.Sockets);

            fResult = fRow;
            break;
          default:
            if (iData.Row && iData.Row.guid) {
              var fFileUser = this.Config.Storage.default.Path + "auth/user/" + iData.Row.guid + this.TableExt;
              fResult = this.getFileJson(fFileUser, iData.Row);
            } else {
              fResult = this.getMergeUser();
            }
            break;
        }
        break;
      case "Buildin-Auth-Start":
        var fSocket = this.Sockets[iSocket.id];
        var fPin = (iData.Row.checkpin) ? iData.Row.checkpin : this.getPin();
        var fMail = {
          MailFrom: 'auth@dd.in.rs',
          MailTo: iData.Row.email,
          MailSubject: 'PIN ' + fPin,
          MailBody: 'Enter PIN ' + fPin + ' to continue using portal.'
        };
        this.setMail(fMail);
        this.Cache.Pins[iSocket.id] = fPin;
        fResult = {
          status: true,
          Session: fSocket.session
        };
        break;
      case "Buildin-Auth-Check":
        var formatName = function (iValue) {
          if ((iValue) && (fIntex = iValue.indexOf('@'))) {
            return iValue.substring(0, fIntex);
          }
          return iValue;
        }
        var fSession = this.Sockets[iSocket.id].session;
        var fPin = (iData.Row.checkpin) ? iData.Row.checkpin : this.Cache.Pins[iSocket.id];
        var fIsAuth = (iData.Row.pin == fPin);
        if (fSession.UserAuth) {
          delete (fSession.UserAuth);
        }
        delete (this.Cache.Pins[iSocket.id]);
        if (fSession && fIsAuth) {
          fSession.UserData = this.getUser('email', iData.Row.email);
          fSession.UserAuth = true;
          fSession.UserEmail = iData.Row.email;
          fSession.UserName = formatName(iData.Row.email);
          fSession.UserUID = fSession.UserData.guid;
          fSession.UserAdmin = {};
          if ((fAdmins = this.Config.Sources.Admins) && (fAdmins.indexOf(fSession.UserEmail) != -1)) {
            fSession.UserAdmin["source"] = true;
          }
          if ((fAdmins = this.Config.Server.Admins) && (fAdmins.indexOf(fSession.UserEmail) != -1)) {
            fSession.UserAdmin["server"] = true;
            fSession.UserAdmin["config"] = true;
          }
          for (var fKey in this.Config.Service) {
            if ((fAdmins = this.Config.Service[fKey].Admins) && (fAdmins.indexOf(fSession.UserEmail) != -1)) {
              fSession.UserAdmin[fKey.toLocaleLowerCase()] = true;
            }
          }
          if (fSession.UserData.firstname) {
            fSession.UserName = fSession.UserData.firstname;
          }
          this.Sockets[iSocket.id].session = fSession;
          this.Cookies[fSession.CookieUID] = {
            CookieUID: fSession.CookieUID,
            UserUID: fSession.UserUID
          };
          this.Users[fSession.UserUID] = {
            Cookies: [fSession.CookieUID],
            UserUID: fSession.UserUID,
            UserAuth: fSession.UserAuth,
            UserEmail: fSession.UserEmail,
            UserAdmin: fSession.UserAdmin,
            UserName: fSession.UserName,
            UserData: fSession.UserData
          };
          iSocket.emit('auth', { session: fSession });
        }
        var fResult = {
          status: fIsAuth,
          Session: fSession
        };

        break;
      case "Buildin-Auth-Logout":
        var fSession = this.Sockets[iSocket.id].session;

        if (fSession.UserAuth) {
          delete (fSession.UserAuth);
        }
        if (fSession) {
          this.Sockets[iSocket.id].session = fSession;
          this.Cookies[fSession.CookieUID] = {
            CookieUID: fSession.CookieUID,
            UserUID: fSession.UserUID
          };
          delete (this.Users[fSession.UserUID]);
          fSession.UserData = {};
          fSession.UserAuth = false;
          fSession.UserEmail = undefined;
          fSession.UserUID = undefined;
          fSession.UserName = undefined;
          iSocket.emit('auth', { session: fSession });
        }
        var fResult = {
          status: false,
          Session: fSession
        };

        break;
      case "Buildin-Auth":
        var fAuths = {};

        for (var fItem in this.Config.Auth) {
          if (this.Config.Sources.Auths.indexOf(fItem) !== -1) {
            fAuths[fItem] = this.Config.Auth[fItem];
          }
        }

        fResult = fAuths;
        break;
      case "Buildin-AuthCheck":
        var fStatus = false;
        var fType = iData.Row.Auth.Type;
        var fAuth = iData.Row.Auth;
        var fUser = iData.Row.User;
        var fData = iData.Row.Data;
        var fSrvAuth = this.Config.Auth[fType];
        var fSrvUser = {
          guid: undefined,
          rev: 0,
          state: "",
          firstname: "",
          familyname: "",
          image: "",
          sex: "",
          emails: [],
          points: 0,
          birthdate: "",
          birthplace: "",
          Auths: {
          }
        };
        var fFileAuth = this.Config.Storage.default.Path + "auth/" + fType.toLowerCase().trim() + "/" + fData.Id + ".row";
        if (fs.existsSync(fFileAuth)) {
          var fUserId = this.getFile(fFileAuth);
          var fFileUser = this.Config.Storage.default.Path + "auth/user/" + fUserId + ".row";
          fSrvUser = this.getFileJson(fFileUser, fSrvUser);
        }
        fUser = this.getMergeUser(fUser, fSrvUser);
        switch (fType.toLowerCase().trim()) {
          case "google":
            fStatus = true;
            break;
          case "jmbg":
            fStatus = true;
            break;
        }
        if (fStatus) {
          fUser = this.getMergeUser(fUser, fData);
          if ((fUser) && (typeof (fUser.guid) === "undefined" || !fUser.guid)) {
            fUser.guid = this.getGuid();
          }
          if (fSrvAuth.Points && (!fUser.Points || fUser.Points < fSrvAuth.Points)) {
            fUser.Points = fSrvAuth.Points;
          }
          if (typeof (fUser.Auths) !== "object") fUser.Auths = {};
          fUser.Auths[fType] = {
            Status: fStatus,
            Id: fData.Id,
            Auth: fType,
            Data: fData,
          }

          var fFileUser = this.Config.Storage.default.Path + "auth/user/" + fUser.guid + ".row";
          this.setFileJson(fFileUser, fUser);
          this.setFile(fFileAuth, fUser.guid);
        }
        fResult = {
          Status: fStatus,
          Msg: "",
          User: fUser
        }
        break;
    }
    if (iCallback) {
      iCallback(undefined, iData, fResult);
    }
  },
	getRoutingSite: function (iRequest) {
		//console.log('Url', iRequest.url, this.Routing);
		var fUrl = iRequest.url;
		var fRouting = this.Routing.default;

		if ((fUrl) && (fUrl.indexOf('?') !== -1)) {
			fUrl = fUrl.substring(0, fUrl.indexOf('?'));
		}
		if ((this.Routing) && (this.Routing.host) && (iRequest.headers.host) && (this.Routing.host[iRequest.headers.host])) {
			fRouting = this.Routing.host[iRequest.headers.host];

			if ((typeof (fRouting) == "string") && (this.Routing.site) && this.Routing.site[fRouting]) {

				var fKey = fRouting;
        if(this.Routing.site[fKey]){
          fRouting = this.Routing.site[fKey];
        }else{
          var fHost  = iRequest.headers.host;
          var fHost0 = fHost.split('.')[0];
          fHost = fHost.replace(fHost0, "*");
    
          if(this.Routing.host[fHost]){
            var fRoutingKey = this.Routing.host[fHost];
    
            console.log('fRouting404 1', fRoutingKey, fHost);
            if ((typeof(fRoutingKey) == "string") && (this.Routing.site)) {
              
              if(this.Routing.site[fRoutingKey]&&this.Routing.site[fRoutingKey].Cloud){
                var fCloudPath = this.Routing.site[fRoutingKey].Cloud;
                var fCloudFile = fCloudPath + fHost0 + "/" + fHost0 + this.TableExt;
                var fCloudJson = this.getFileJson(fCloudFile, undefined);
                if(fCloudJson){
                  fRouting = fCloudJson;
                }
              }
            }
          }
        }
			}
		}else{
      var fHost  = iRequest.headers.host;
      var fHost0 = fHost.split('.')[0];
      fHost = fHost.replace(fHost0, "*");

      if(this.Routing.host[fHost]){
        var fRoutingKey = this.Routing.host[fHost];

        console.log('fRouting404 1', fRoutingKey, fHost);
        if ((typeof(fRoutingKey) == "string") && (this.Routing.site)) {
          
          if(this.Routing.site[fRoutingKey]&&this.Routing.site[fRoutingKey].Cloud){
            var fCloudPath = this.Routing.site[fRoutingKey].Cloud;
            var fCloudFile = fCloudPath + fHost0 + "/" + fHost0 + this.TableExt;
            var fCloudJson = this.getFileJson(fCloudFile, undefined);
            if(fCloudJson){
              fRouting = fCloudJson;
            }
          }
        }
      }          
    }

		return fRouting;
	},
	getRouting: function (iRequest) {
		console.log('Url', iRequest.headers.host, iRequest.url);
		var fUrl = iRequest.url;
		var fResult = "undefined";
		var fRouting = this.Routing.default;

		if ((fUrl) && (fUrl.indexOf('?') !== -1)) {
			fUrl = fUrl.substring(0, fUrl.indexOf('?'));
		}
		if ((this.Routing) && (this.Routing.host) && (iRequest.headers.host) && (this.Routing.host[iRequest.headers.host])) {
			var fRoutingKey = this.Routing.host[iRequest.headers.host];
      console.log('fRouting1', fRoutingKey, this.Routing.site);
      if ((typeof(fRoutingKey) == "string") && (this.Routing.site)) {

        switch(fRoutingKey){
          case "default":
            fRouting = this.Routing.default;
          break;
          default:
            if(this.Routing.site[fRoutingKey]){
              fRouting = this.Routing.site[fRoutingKey];
            }else{
              console.log("Not found:", iRequest.headers.host);
              if(this.Routing.host['notfound']){
                return this.Routing.host['notfound'];
              }
            }
            break;
        }
        console.log('fRouting2', fRouting);
			}
		}else{
      console.log("Not found:", iRequest.headers.host);
      var fHost  = iRequest.headers.host;
      var fHost0 = fHost.split('.')[0];
      fHost = fHost.replace(fHost0, "*");

      if(this.Routing.host[fHost]){
        var fRoutingKey = this.Routing.host[fHost];

        console.log('fRouting404 1', fRoutingKey, fHost);
        if ((typeof(fRoutingKey) == "string") && (this.Routing.site)) {
          
          if(this.Routing.site[fRoutingKey]&&this.Routing.site[fRoutingKey].Cloud){
            var fCloudPath = this.Routing.site[fRoutingKey].Cloud;
            var fCloudFile = fCloudPath + fHost0 + "/" + fHost0 + this.TableExt;
            var fCloudJson = this.getFileJson(fCloudFile, undefined);
            if(fCloudJson){
              fRouting = fCloudJson.Routing;
            }else{
              console.log("Not found:", iRequest.headers.host);
              if(this.Routing.host['notfound']){
                return this.Routing.host['notfound'];
              }
            }
          }
        }
      }
    }

		if ((typeof (fUrl) == "string") && (this.Routing) && (this.Routing.site)) {
			for (var fSite in this.Routing.site) {
				if (fUrl.startsWith(fSite)) {
					fRouting = this.Routing.site[fSite];
					fUrl = fUrl.replace(fSite, '');
					console.log('Site', fSite, fUrl, fRouting);
				}
			}
			//var fKey = fRouting;
			//fRouting =	this.Routing.site[fKey];
		}

		if ((fRouting) && (fRouting['/res/']) && (fUrl.startsWith('/res/'))) {
			fResult = fRouting['/res/'] + fUrl.replace('/res/', '');
		}

		if ((fRouting) && (fRouting[fUrl])) {
			fResult = fRouting[fUrl];
		}

		return fResult;
	},
	getHttpContent: function (iFilename, iEncoding) {
		var fReturn = { Code: 404, Status: true, Head: { "Content-Type": "text/html" }, Html: "404 Http not found!" };
		//var fEncoding = ((iEncoding) ? iEncoding : "utf8");

		fReturn.Html = '@import url("' + iFilename + '");';

		return fReturn;
	},
	getFileContent: function (iFilename, iEncoding) {
		var fReturn = { Code: 404, Status: true, Head: { "Content-Type": "text/html" }, Html: "404 File not found!" };
		var fEncoding = ((iEncoding) ? iEncoding : "utf8");

		if (fs.existsSync(iFilename)) {
			var fMime = this.getFileMime(iFilename);
			fReturn.Head["Content-Type"] = fMime;
			if ((fMime != "text/css") && (fMime != "text/html")) {
				//fEncoding = "binary";
				//console.log('binary');
			}
			fReturn.Html = fs.readFileSync(iFilename, fEncoding);
			var stats = fs.statSync(iFilename);
			var mtime = new Date(stats.mtime);

			//fReturn.Head["Accept-Ranges"] = "bytes";
			//fReturn.Head["Content-Length"] = fReturn.Html.length;
			if(fReturn.Html.byteLength){
				//fReturn.Head["Content-Length"] = fReturn.Html.byteLength;
			}
			fReturn.Head["Last-Modified"] = mtime;

			fReturn.Code = 200;
			//console.log(iFilename, fReturn.Code, fReturn.Head);
		} else {
			this.echoTest('File not found:', iFilename, 5);
			return fReturn;
		}

		return fReturn;
	},
	getFileExt: function (iFileName) {
		var fResult = (/[.]/.exec(iFileName)) ? /[^.]+$/.exec(iFileName)[0] : '';

		return fResult;
	},
	getFileMime: function (iFileName) {
		var fResult = "text/html";
		var fExt = this.getFileExt(iFileName);

		switch (fExt.toLowerCase()) {
			case 'ico': fResult = "image/x-icon"; break;
			case 'css': fResult = "text/css"; break;
			case 'js': fResult = "application/javascript"; break;
			case 'png': fResult = "image/png"; break;
			case 'gif': fResult = "image/gif"; break;
			case 'jpg':
			case 'jpeg': fResult = "image/jpeg"; break;
			case 'woff':
			case 'woff2': fResult = "image/font-woff"; break;
			case 'ttf': fResult = "application/octet-stream"; break;
		}
		/*TraceOn this.echoTest("Ext ", fExt+"-"+fResult+" for "+iFileName, 5); /*TraceOn*/

		return fResult;
	},
	getHeadCache: function (iRequest) {
		if ((iRequest) && (iRequest.headers) && (iRequest.headers.pragma) && (iRequest.headers.pragma == 'no-cache'))
			return false;

		if ((iRequest) && (iRequest.headers) && (iRequest.headers['cache-control']) && (iRequest.headers['cache-control'] == 'no-cache'))
			return false;

		return true;
	},
	getHeadCacheBrowser: function (iRequest, iHead) {
		if ((iRequest) && (iRequest.rawHeaders) && (iHead)) {
			var fBh = iRequest.rawHeaders;

			for (i = 0; i < (fBh.length - 1); i++)
				if (fBh[i] == "If-Modified-Since") {
					if (fBh[i + 1] == iHead["Last-Modified"])
						return true;
				}
		}

		return false;
	},
	setHttpCache: function (iRequest, iResponse) {
		var fResponse = this.getClone(iResponse);
		if (this.UseCache && iRequest && fResponse && fResponse.Head) {
      console.log("a", fResponse);
			fResponse.Head["Cache-Control"] = "public,max-age=14399";
			var fCacheKey = btoa(iRequest.headers.host) + iRequest.url;
			this.Cache.Pages[fCacheKey] = fResponse;
		}

		return fResponse;
	},
	setCacheReset: function () {
		this.Cache = {
			Pages: [],
			Resource: []
		};

		return true;
	},
	getPageName: function (iRequest) {
		return iRequest.url.replace('/', '');
	},
	getImage: function (iRequest) {
		var fResFile = '/usr/src/app/html' + iRequest.url;
		var fReturn = { Code: 404, Status: true, Head: { "Content-Type": "image/jpeg" }, Html: "" };
		var fHtml = "";

		if (fHtml = this.getFileContent(fResFile)) {
			fReturn.Code = 200;
			fReturn.Html = fHtml;

			switch (this.getFileExt(fResFile)) {
				case 'ico': fReturn.Head = { "Content-Type": "image/x-icon" }; break;
				case 'png': fReturn.Head = { "Content-Type": "image/png" }; break;
				case 'gif': fReturn.Head = { "Content-Type": "image/gif" }; break;
				case 'jpg':
				case 'jpeg': fReturn.Head = { "Content-Type": "image/jpeg" }; break;
			}

			fReturn.Head["Accept-Ranges"] = "bytes";
			fReturn.Head["Content-Length"] = fReturn.Html.length;
		}

		return fReturn;
	},
	getStatic: function (iUrl, iRequest) {
		var fUrl = iUrl;
		var fReturn = { Code: 404, Status: true, Head: { "Content-Type": "text/html" }, Html: "404 - Page not found" };

		if ((fUrl) && (fUrl.indexOf('?') !== -1)){
			fUrl = fUrl.substring(0, fUrl.indexOf('?'));
		}
		//var fResFile = this.Config.PageResFolder + fUrl;
		var fResFile = fUrl;
		/*TraceOn*/ this.echoTest("File", fUrl, 9); /*TraceOn*/
		if ((fResFile)&&(fResFile!="undefined")&&(fHtml = this.getFileContent(fResFile))) {
			fReturn = this.getMerge(fReturn, fHtml);
			var stats = "";
			try {
				stats = fs.statSync(fResFile);
			}
			catch (err) {
				console.log('File error:', fResFile, err);
			}
			var mtime = new Date(stats.mtime);

			//fReturn.Head["Accept-Ranges"] = "bytes";
			//fReturn.Head["Content-Length"] = fReturn.Html.length;
			//fReturn.Head["Cache-Control"] = (this.UseCache) ? "public,max-age=14399" : "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
			fReturn.Head["Last-Modified"] = mtime;
			fReturn.Head["cross-site-cookie"] = 'name';
			fReturn.Head["SameSite"] = 'None';
			fReturn.Head["Secure"] = '';
			fReturn.Head["X-Server"] = "DD-Server";//this.Config.Server;
		}

		return fReturn;
	},
	setReplace: function (iValue, iFrom, iTo, iIsVariable) {
		return iValue.replace(new RegExp(iFrom, 'g'), iTo);
	},
	setParse: function (iContent, iObject, iLng) {
		var fContent = (iContent) ? iContent : "";

		fContent = this.setReplace(fContent, "{{File.Menu}}", this.getFile("html/components/menu.html"));
		fContent = this.setReplace(fContent, "{{File.Meta}}", this.getFile("html/components/meta.html"));
		fContent = this.setReplace(fContent, "{{File.Css}}", this.getFile("html/components/css.html"));
		fContent = this.setReplace(fContent, "{{File.Js}}", this.getFile("html/components/js.html"));
		fContent = this.setReplace(fContent, "{{File.Footer}}", this.getFile("html/components/footer.html"));
		fContent = fContent.toString().replace("{{rnd}}", this.getShort());

		var fNode = "Site";
		var fLng = (iLng)? iLng:Lng;
		console.log('AA', iLng, fLng[fNode]);
		if (fLng[fNode]) {
			for (var fKey in fLng[fNode]) {
				var fValue = "";
				switch (fKey) {
					case "MenuSocial":
						for (var fKeyMenu in fLng[fNode][fKey]) {
							fValue += ((fValue) ? " " : "") + '<a class="btn btn-icon btn-pure" target="_blank" href="' + fLng[fNode][fKey][fKeyMenu].Link + '">' + fLng[fNode][fKey][fKeyMenu].Caption + '</a>';
						}
            break;
          case "MenuMain":
          case "MenuFooter":
          case "MenuResource":
            for (var fKeyMenu in fLng[fNode][fKey]) {
							//fValue += ((fValue)? "&nbsp;|&nbsp;":"") + '<a href="'+Lng[fNode][fKey][fKeyMenu].Link+'">'+Lng[fNode][fKey][fKeyMenu].Caption+'</a>';
							fValue += ((fValue) ? " " : "") + '<a href="' + fLng[fNode][fKey][fKeyMenu].Link + '#">' + fLng[fNode][fKey][fKeyMenu].Caption + '</a>&nbsp;&nbsp;';
						}
						break;
					default:
						fValue = fLng[fNode][fKey];
						break;
				}
				console.log('CCCC', fNode, fKey, fValue);
				fContent = this.setReplace(fContent, "{{" + fNode + '.' + fKey + "}}", fValue);
			}
		}
		//fContent = this.setReplace(fContent, "{{" + fNode+'.'+fKey + "}}", fValue);
		if (this.Utils && this.Config.Vendor) {
			for (var fKey in this.Config.Vendor) {
				if (typeof (this.Config.Vendor[fKey]) === "string") {
					fContent = this.setReplace(fContent, "{{" + fKey + "}}", this.Config.Vendor[fKey]);
				}
			}
		}

		if (iObject) {
			for (var fKey in iObject) {
				fContent = this.setReplace(fContent, "{{" + fKey + "}}", iObject[fKey]);
			}
		}

		return fContent;
	},
	bindHttp: function (request, response) {
		/*TraceOn this.echoTest("bindHttp start", request.url, -1); /*TraceOn*/
		var self = this;
		var fRequest = request;
		var fResponse = { Status: false };
		var fResponseDo = true;
		var fResponseProcess = true;
		var fCacheKey = btoa(fRequest.headers.host) + fRequest.url;
		try {
			if (this.UseCache&&this.getHeadCache(fRequest)) {
				if (this.Cache.Resource[fCacheKey]&&this.Cache.Resource[fCacheKey].Html) {
					fResponse = this.Cache.Resource[fCacheKey];
					/*TraceOn*/ this.echoTest("Cache Resource", fCacheKey, 8); /*TraceOn*/
					fResponseProcess = false;
				}
				if (this.Cache.Pages[fCacheKey]&&this.Cache.Pages[fCacheKey].Html) {
					fResponse = this.Cache.Pages[fCacheKey];
					/*TraceOn*/ this.echoTest("Cache Page", fRequest.url, 8); /*TraceOn*/
					fResponseProcess = false;
				}
			}

			if (fResponseProcess) {
				var fFile = self.getRouting(fRequest);
				//console.log("FILE", fFile);
				/*TraceOn*/ this.echoTest("Routing", fFile, 8); /*TraceOn*/
				switch (typeof (fFile)) {
					case "undefined":
						fResponse.Code = "404";
						break;
					case "string":
						/*TraceOn this.echoTest("Static", fRequest.url, 8); /*TraceOn*/
						var fFileMime = this.getFileMime(fFile);
						if(fFileMime=="text/html"){
							fResponse = this.getStatic(fFile, request);
							var fTitle = this.getPageName(fRequest);
							var fLng = Lng;
							if(fNode = self.getRoutingSite(fRequest)){
								fLng = fNode.Language;
                console.log('fLng:', fNode);
							}
							if (fLng) {
								var fMenus = this.getObject(fLng.Site.MenuMain, 'Link');
								if (fMenus[fRequest.url]) {
									fTitle = fMenus[fRequest.url].Caption;
								}
							}
							var fParseStr = {
								Title: fTitle
							}
							fResponse.Html = this.setParse(fResponse.Html, fParseStr, fLng);
						}else{
							try{
								fs.readFile(fFile, function(err, data) {
									if(err){ console.log('Error no file', err); }
									response.writeHead(200, {'Content-Type': fFileMime});
									response.end(data); 
								});
							}catch(err){
								console.log('Error', err);
							}
							return;
						}

						if (this.UseCache) {
							this.Cache.Resource[fCacheKey] = fResponse;
							fResponse = this.setHttpCache(fRequest, fResponse);
						}
			
						break;
					case "object":
						fResponse.Code = 200
						fResponse.Status = true
						fResponse.Head = ((fFile.Head) ? fFile.Head : {}),
						fResponse.Html = "";
						if (fFile["Content-Type"]) {
							fResponse.Head["Content-Type"] = fFile["Content-Type"];
						}
						if (fFile.http) {
							var fFiles = (Array.isArray(fFile.http)) ? fFile.http : fFile.http.split(",");

							for (i = 0; i < fFiles.length; i++) {
								fResponse.Html += this.getHttpContent(fFiles[i]).Html;
							}
						}
						if (fFile.file) {
							var fFiles = (Array.isArray(fFile.file)) ? fFile.file : fFile.file.split(",");

							for (i = 0; i < fFiles.length; i++) {
								fResponse.Html += this.getFileContent(fFiles[i]).Html;
							}
						}
						if (fFile.config) {
							fResponse.Html = 'ClientConfig = ' + JSON.stringify(this.Config.Client) + ";";
						}
						if (fFile.configserver) {
							fResponse.Html = JSON.stringify(this.Config);
						}
						if (fFile.api) {
							/*TraceOn*/ this.echoTest("API-"+request.method, url.parse(request.url, true).query, 9); /*TraceOn*/
							if (fRequest.method === 'POST') {
								var body = '';
								fRequest.on('data', chunk => {
									if(chunk){
										body += chunk.toString(); // convert Buffer to string
									}
								});
								fRequest.on('end', () => {
									console.log(body);
									var fAction = url.parse(request.url, true).query.action;
									switch(fAction){
										case "tenderadd":
											var fTenders = JSON.parse(body); 
											var fTableName = "tender";
											var fTable;// = 	this.getData(fTableName, {});
											
											for(var i=0; i<fTenders.length; i++){
												console.log('+');
												var fRow = {
													guid: fTenders[i].KeyValue,
													rev: 1,
													date: new Date()
												}
												for(var fKey in fTenders[i].Items){
													fRow[fKey] = fTenders[i].Items[fKey];
												}
												fTable = this.setData(fTableName, fTable, fRow, "save");
											}
											if(fTable) {
												delete(fTable);
											}
											this.setDataMap(fTableName);
											break;
									}
								});
							}

							fResponse.Html = "true";
						}
						if (fFile.cache && this.UseCache) {
							fResponse = this.setHttpCache(fRequest, fResponse);
						}
						if (fFile.out) {
							this.setFile(fFile.out, fResponse.Html);
						}
						break;
				}
			}
		}
		catch (err) {
			console.error("Server Error 1", fResponse, err);
		}

		if (fResponseDo) {
			if ((fResponse) && (fResponse.Status)) {
				response.writeHead(fResponse.Code, fResponse.Head);
				response.write(fResponse.Html);
				response.end();
			} else {
				console.error("Server Error 2", fResponse);
				response.writeHead(500, { "Content-Type": "text/html" });
				response.end();
			}
		}
		/*TraceOn this.echoTest("bindHttp end", request.url, 1); /*TraceOn*/
	}
}