module.exports = {
	Utils: null,
	UseCache: false,
	Cache: {
		Table: {}
	},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('History.Cache', this.UseCache);
	},
	echoTest: function (iMessage, iMessageObject, iDebuglevel) {

		if (this.Utils)
			return this.Utils.echoTest(iMessage, iMessageObject, iDebuglevel);

		if (iMessageObject)
			console.log(iMessage, iMessageObject);
		else
			console.log(iMessage);
	},
	echoError: function (iMessage, iMessageObject) {

		if (this.Utils)
			return this.Utils.echoError(iMessage, iMessageObject);

		console.log("ERROR!!!");

		if (iMessageObject)
			console.log(iMessage, iMessageObject);
		else
			console.log(iMessage);

		return true;
	},
	Sync: function (iData, iCallback) {
		var fResult;

		/**/ this.echoTest('History.Sync', iData, 1); /**/
		switch (iData.Name) {
			case "History-Init":
				this.Cache.Table = this.Utils.getData('history', this.Cache.Table);
				fResult = this.Utils.getClone(this.Cache.Table);
			break;
			case "History-List":
				if (iData.Action&&iData.Action!="default") {
					this.Cache.Table = this.Utils.setData('history', this.Cache.Table, iData.Row, iData.Action);
					this.Utils.setEvent({ Name:  iData.Name});
				}
				fResult = this.Utils.getClone(this.Cache.Table);
			break;
		}
		if (iCallback) {
			iCallback(undefined, iData, fResult);
		}
	}
}