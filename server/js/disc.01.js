module.exports = {
	Utils: null,
	UseCache: false,
	TableName: "disc",
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Disc.Cache', this.UseCache);
	},
	Sync: function (iData, iCallback, iSocket) {
		var fResult;
		var fPath = this.Utils.getStorage('', iData);
		var fCallbackExec = true;
		var fTable;
		if(this.UseCache){

		}
		if(!fTable){
			fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);
		}
		/**/ this.Utils.echoTest('Sync Disc', iData, 12); /**/
		switch (iData.Name) {
			case "Disc-Init":
				
			break;
			case "Disc-List":
				switch(iData.Action) {
					case "detail":
						fResult = this.Utils.getData(this.TableName, fTable, iData.Row, fPath);
					break;
					case "insert": case "update": case "save": case "delete":
						fTable = this.Utils.setData(this.TableName, fTable, iData.Row, iData.Action, fPath);
						fResult = this.Utils.setFilter(fTable, this.TableName, iData.Trace.session);
						this.Utils.Event('Disc.Update', iData.Row, iData.Trace.session);
					break;
					default:
						fResult = this.Utils.setFilter(fTable, this.TableName, iData.Trace.session);
					break;
				}
			break;
			case "Disc-Msg":
				var fMsgFile = 'msgs';
				var fMsgPath = this.Utils.getPath2(fPath, this.TableName, iData.Row.pguid);
				var fMsgTable = this.Utils.getData(fMsgFile, {}, undefined, fMsgPath);
				var fDiscRow = (fTable[iData.Row.pguid])? fTable[iData.Row.pguid]:undefined;
				var fSession = iData.Trace.session;

				switch(iData.Action) {
					case "new":
						fMsgTable = this.Utils.setData(fMsgFile, fMsgTable, iData.Row, 'save', fMsgPath);
						if(fDiscRow){
							fDiscRow.msg_count =  Object.keys(fMsgTable).length;
							fDiscRow.msg_counter =  ((fDiscRow.msg_counter)? fDiscRow.msg_counter:1)+1;

							if(!fDiscRow.parcipients){ fDiscRow.parcipients = []; }
							if(fSession&&fSession.UserEmail&&fDiscRow.parcipients&&-1==fDiscRow.parcipients.indexOf(fSession.UserEmail)){
								fDiscRow.parcipients.push(fSession.UserEmail);
							}
						}
						this.Utils.Event('Disc.MsgNew', iData.Row, fSession);
						this.Utils.Event('Disc.Update', fDiscRow, fSession);
						break;
					case "save":
						fMsgTable = this.Utils.setData(fMsgFile, fMsgTable, iData.Row, 'save', fMsgPath);
						this.Utils.Event('Disc.MsgSave', iData.Row, fSession);
					break;
					case "delete":
						fMsgTable = this.Utils.setData(fMsgFile, fMsgTable, iData.Row, 'delete', fMsgPath);
						this.Utils.Event('Disc.MsgDelete', iData.Row, fSession);
					break;
					case "up":
					case "down":
						var fVoteFile = 'vote-'+iData.Trace.session.UserUID;
						var fVotePath = this.Utils.getPath2(fPath, this.TableName, iData.Row.pguid);
						var fMsgRow = fMsgTable[iData.Row.guid];
						var fVoteTable = this.Utils.getData(fVoteFile, {}, undefined, fVotePath);

						if(fVoteRowOld=fVoteTable[iData.Row.guid]){
							if(fVoteRowOld==iData.Action){
								delete(fVoteTable[iData.Row.guid]);
								if(iData.Action=="up"){	fMsgRow.vote_up = (fMsgRow.vote_up? fMsgRow.vote_up:1)-1;	}
								if(iData.Action=="down"){	fMsgRow.vote_down = (fMsgRow.vote_down? fMsgRow.vote_down:1)-1;	}
							}else{
								fVoteTable[iData.Row.guid] = iData.Action;
								if(fVoteRowOld=="up"){	fMsgRow.vote_up = (fMsgRow.vote_up? fMsgRow.vote_up:1)-1;	}
								if(fVoteRowOld=="down"){	fMsgRow.vote_down = (fMsgRow.vote_down? fMsgRow.vote_down:1)-1;	}
								if(iData.Action=="up"){	fMsgRow.vote_up = (fMsgRow.vote_up? fMsgRow.vote_up:0)+1;	}
								if(iData.Action=="down"){	fMsgRow.vote_down = (fMsgRow.vote_down? fMsgRow.vote_down:0)+1;	}
							}
						}else{
							fVoteTable[iData.Row.guid] = iData.Action;
							if(iData.Action=="up"){	fMsgRow.vote_up = (fMsgRow.vote_up? fMsgRow.vote_up:0)+1;	}
							if(iData.Action=="down"){	fMsgRow.vote_down = (fMsgRow.vote_down? fMsgRow.vote_down:0)+1;	}
						}

						fVoteTable = this.Utils.setData(fVoteFile, fVoteTable, undefined, undefined, fVotePath);
						fMsgTable = this.Utils.setData(fMsgFile, fMsgTable, fMsgRow, 'save', fMsgPath);
						this.Utils.Event('Disc.MsgSave', fMsgRow, fSession);
					break;
				}
				fTable = this.Utils.setData(this.TableName, fTable, fDiscRow, 'save', fPath);
				fResult = this.Utils.getClone(fMsgTable);
			break;
			case "Disc-Vote":
				var fVoteFile = 'vote-'+iData.Trace.session.UserUID;
				var fVotePath = this.Utils.getPath2(fPath, this.TableName, iData.Row.pguid);
				console.log('Vote', fVotePath, fVoteFile);
				var fVoteTable = this.Utils.getData(fVoteFile, {}, undefined, fVotePath);

				switch(iData.Action) {
					case "up":
					case "down":
						fVoteTable[iData.Row.guid] = iData.Action;
						fVoteTable = this.Utils.setData(fVoteFile, fVoteTable, undefined, undefined, fVotePath);
					break;
				}
				fResult = fVoteTable;
			break;
			case "Disc-Viewers":
				/*fResult = {
					Sockets: this.Utils.getClone(this.Utils.Sockets),
					Sessions: this.Utils.getClone(this.Utils.Sessions)
				};*/
				var fSockets = this.Utils.getClone(this.Utils.Sockets);
				var fViewers = {};
				for(var fSocketKey in fSockets){
					var fSocket = fSockets[fSocketKey];
					if(fSocket&&fSocket.session&&fSocket.session.CookieUID&&fSocket.session.UserName){
						fViewers[fSocket.session.CookieUID] = {
							UserName: fSocket.session.UserName
						}
					}else{
						fViewers[fSocket.session.CookieUID] =  {
							UserName: 'Anonimus'
						}
					}
				}
				//this.Utils.Event({ Name:  iData.Name});
				fResult = this.Utils.getClone(fViewers);

			break;
			case "Disc-Req":
				var fReqFile = 'req';
				var fReqPath = this.Utils.getPath2(fPath, this.TableName, iData.Row.pguid);
				var fReqTable = this.Utils.getData(fReqFile, {}, undefined, fReqPath);

				switch((iData.Action||"")) {
					case "delete":
						if(fReqTable[iData.Row.guid]){
							delete(fReqTable[iData.Row.guid]);
							fReqTable = this.Utils.setData(fReqFile, fReqTable, undefined, undefined, fReqPath);
						};
					break;
					case "Reply":
					case "Talk":
						fReqTable[iData.Row.guid] = { Action: iData.Action, guid: iData.Row.guid, pguid: iData.Row.pguid, date: iData.Row.date, User: iData.Trace.session } ;
						fReqTable = this.Utils.setData(fReqFile, fReqTable, undefined, undefined, fReqPath);
					break;
				}
				fResult = fReqTable;
				this.Utils.Event('Disc.Req', { Reqs: fReqTable, pguid: iData.Row.pguid }, iData.Trace.session);
			break;
		}

		if (iCallback&&fCallbackExec&&fResult) {
			iCallback(undefined, iData, fResult);
		}
	}
}