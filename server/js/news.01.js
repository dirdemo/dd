const file = require('fs');
const fs = require('fs');

module.exports = {
  Type: "news",
  Utils: null,
  UseCache: false,
  Path: "",
  CronActive: 0,
  CronNew: 0,
  CronTimer: undefined,
  Date: {},
  Cache: {},
  setUtils: function (iUtils) {
    if (!this.Utils) {
      this.Utils = iUtils;
    }
    if (!this.Utils.getConfig('Service.News', false)) {
      console.log('Error no news config in', this.Utils.ConfigFile);
    }
    this.UseCache = this.Utils.getConfig('Service.News.Cache', this.UseCache);
    this.Path = this.Utils.getConfig('Service.News.Path', this.Path);
  },
  echoTest: function (iMessage, iMessageObject, iDebuglevel) {

    if (this.Utils)
      return this.Utils.echoTest(iMessage, iMessageObject, iDebuglevel);

    if (iMessageObject)
      console.log(iMessage, iMessageObject);
    else
      console.log(iMessage);
  },
  echoError: function (iMessage, iMessageObject) {

    if (this.Utils)
      return this.Utils.echoError(iMessage, iMessageObject);

    console.log("ERROR!!!");

    if (iMessageObject)
      console.log(iMessage, iMessageObject);
    else
      console.log(iMessage);

    return true;
  },
  FormatItem: function (iItem, iSource, iType) {
    var fItem = {
      uid: "",
      guid: "",
      puid: ((iSource.guid) ? iSource.guid : ""),
      date: "",
      link: "",
      title: "",
      description: "",
      source_id: ((iSource.guid) ? iSource.guid : ""),
      type: this.Type,
      session: {
        SessionUID: "Cron",
        CookieUID: "",
        UserUID: "",
        UserName: "Cron"
      }
    };
    switch (iType) {
      case 'youtube':
        fItem.uid = ((iItem.id) ? iItem.id : iItem.link);
        fItem.date = this.Utils.getDate(iItem.published);
        fItem.link = ((iItem.link) ? iItem.link : "");
        fItem.title = ((iItem.title) ? iItem.title : "");
        fItem.description = ((iItem.description) ? iItem.description : ((iItem.content) ? iItem.content : ""));
        //fItem.description = iItem["media:group"];
        break;
      case 'twitter':
        fItem.uid = ((iItem.guid) ? iItem.guid : iItem.link);
        fItem.date = this.Utils.getDate(iItem.pubDate);
        fItem.link = ((iItem.link) ? iItem.link : "");
        fItem.title = this.Utils.getTextClean(((iItem.description) ? iItem.description : ((iItem.content) ? iItem.content : "")));
        fItem.description = ((iItem.description) ? iItem.description : ((iItem.content) ? iItem.content : ""));
        break;
      default:
        fItem.uid = ((iItem.guid) ? iItem.guid : iItem.link);
        fItem.date = this.Utils.getDate(iItem.pubDate);
        fItem.link = ((iItem.link) ? iItem.link : "");
        fItem.title = ((iItem.title) ? iItem.title : "");
        fItem.description = ((iItem.description) ? iItem.description : ((iItem.content) ? iItem.content : ""));
        break;
    }

    fItem.guid = this.Utils.getMD5(fItem.uid);

    return fItem;
  },
  AddItem: function (iItem) {
    if(this.Utils.existsData(this.Type, iItem)){
      process.stdout.write("-");
    }else{
      process.stdout.write("+");
      this.Utils.setData(this.Type, undefined, iItem, 'save');

      var fDate = this.Utils.getDateYMDShort(iItem.date);
      var fDateFile = this.Path + 'd' + fDate + this.Utils.TableExt;
      if (!this.Date[fDate]) {
        this.Date[fDate] = [];
        if (this.Utils.existFile(fDateFile)) {
          this.Date[fDate] = this.Utils.getFileJson(fDateFile, []);
        }
      }
      this.Date[fDate].push(iItem);
      /*if (this.Cache[fDate]) {
        delete (this.Cache[fDate]);
      }*/
      //console.log('end');
      this.Utils.Event('News.New', iItem, { UserId: "Cron" });
      return true;
    }

    return false;
  },
  CronProcesses: function (iValue) {
    if (iValue) {
      this.CronActive = this.CronActive + iValue;
      if (iValue < 0) {
        this.Utils.setSources();
      }
    } else {
      this.CronActive++;
    }
    if ((!this.CronActive) || (this.CronActive == 0) || (this.CronActive < 0)) {
      this.CronActive = 0;

      for (var fDate in this.Date) {
        var fDateFile;
        if(this.Utils.TableCompression){
          fDateFile = this.Path + 'd' + fDate + '.zip';
          this.Utils.setFileZip(fDateFile, 'd' + fDate, this.Date[fDate]);
        }else{
          fDateFile = this.Path + 'd' + fDate + this.Utils.TableExt;
          this.Utils.setFileJson(fDateFile, this.Date[fDate]);
        }
				/*TraceOn*/ this.echoTest("Write news date", fDateFile, 5); /*TraceOn*/
      }
    }
		/*TraceOn*/ this.echoTest("Cron processes", this.CronActive, 5); /*TraceOn*/
  },
  PullRSS: function (iUrl, iSource, iIndex) {
    var fUrl = iUrl;
    const RSSParser = require('rss-parser');
    var fRSSParser = new RSSParser();
    self.CronProcesses(+1);
    try {
      fRSSParser.parseURL(fUrl, function (err, feed) {
        if (err) {
          console.log('RSS Error:', iSource.title, err);
          if (self.Utils.Sources[iSource.guid].Cron) {
            self.Utils.Sources[iSource.guid].Cron.RSSDate = new Date().toString();
          } else {
            self.Utils.Sources[iSource.guid] = { Cron: { RSSDate: new Date().toString() } };
          }
          if (err.reason) {
            err = err.reason;
          }
          if (err.message) {
            err = err.message;
          }
          if (err.cert) {
            delete (err.cert);
          }
          if (typeof (err) == "object")
            try {
              err = JSON.stringify(err);
            } catch (err) {
              console.log("!");
            }
          self.Utils.Sources[iSource.guid].Cron.RSSError = err;
        } else {
          /*TraceOn*/ self.echoTest("RSS Result", iIndex+' '+iSource.title+' '+feed.items.length, 5); /*TraceOn*/
          var fFilesNew = 0;
          feed.items.forEach(function (entry) {
            var fItem = self.FormatItem(entry, iSource, 'rss');
            if (self.AddItem(fItem)) fFilesNew++;
          });
          console.log("=", fFilesNew);
          self.CronNew = self.CronNew + fFilesNew;
          if ((self.Utils.Sources) && (iSource.guid) && (self.Utils.Sources[iSource.guid])) {
            if (!self.Utils.Sources[iSource.guid].Cron) {
              self.Utils.Sources[iSource.guid].Cron = {};
            }
            self.Utils.Sources[iSource.guid].Cron.RSSDate = new Date().toString();
            self.Utils.Sources[iSource.guid].Cron.RSSNew = fFilesNew;
            self.Utils.Sources[iSource.guid].Cron.RSSCount = feed.items.length;
            self.Utils.Sources[iSource.guid].Cron.RSSError = undefined;
          }
        }
        self.CronProcesses(-1);
      });
    }
    catch (err) {
      console.log('RSS Error', iSource.title, err, fUrl);
      self.Utils.Sources[iSource.guid].Cron.RSSDate = new Date().toString();
      self.Utils.Sources[iSource.guid].Cron.RSSError = err;
    }
    /* END PullRSS */
  },
  PullTwitter: function (iUrl, iSource, iIndex) {
    var fUrl = iUrl;
    if (fUrl.indexOf("https://twitter.com/") != -1) {
      var fChannel = fUrl.replace("https://twitter.com/", "");
      fChannel = fUrl.replace("?lang=en", "");
      fUrl = "https://queryfeed.net/tw?q=" + fChannel;
    }
    const RSSParser = require('rss-parser');
    var fRSSParser = new RSSParser();
    self.CronProcesses(+1);
    try {

      fRSSParser.parseURL(fUrl, function (err, feed) {
        if (err) {
          console.log('Twitter Error', iSource.title, err);
          if (err) {
            console.log('Twitter ERRROR:', typeof (err), err, fUrl);
          }
          if (err.Error) {
            err = err.Error;
          }
          if (err.message) {
            err = err.message;
          }
          if (typeof (err) == "object")
            try {
              err = JSON.stringify(err);
            } catch (err) {
            }
          self.Utils.Sources[iSource.guid].Cron.TwitterDate = new Date().toString();
          self.Utils.Sources[iSource.guid].Cron.TwitterError = err;
        } else {
          console.log("Twitter Result", iSource.title, feed.items.length);
          var fFilesNew = 0;
          feed.items.forEach(function (entry) {
            var fItem = self.FormatItem(entry, iSource, 'twitter');
            if (self.AddItem(fItem)) fFilesNew++;
          });
          console.log("!", fFilesNew);
          self.CronNew = self.CronNew + fFilesNew;
          if ((self.Utils.Sources) && (iSource.guid) && (self.Utils.Sources[iSource.guid])) {
            if (!self.Utils.Sources[iSource.guid].Cron) {
              self.Utils.Sources[iSource.guid].Cron = {};
            }
            self.Utils.Sources[iSource.guid].Cron.TwitterDate = new Date().toString();
            self.Utils.Sources[iSource.guid].Cron.TwitterNew = fFilesNew;
            self.Utils.Sources[iSource.guid].Cron.TwitterCount = feed.items.length;
            self.Utils.Sources[iSource.guid].Cron.TwitterError = undefined;
          }
        }
        self.CronProcesses(-1);
      });
    }
    catch (err) {
      console.log('Twitter Error', iSource.title, err, fUrl);
      self.Utils.Sources[iSource.guid].Cron.TwitterDate = new Date().toString();
      self.Utils.Sources[iSource.guid].Cron.TwitterError = err;
    }
    /* END PullTwitter */
  },
  PullYoutube: function (iUrl, iSource, iIndex) {
    var fUrl = iUrl;

    if (fUrl.indexOf("https://www.youtube.com/channel/") != -1) {
      var fChannel = fUrl.replace("https://www.youtube.com/channel/", "");
      fUrl = "https://www.youtube.com/feeds/videos.xml?channel_id=" + fChannel;
    }
    const RSSParser = require('rss-parser');
    var fRSSParser = new RSSParser();
    self.CronProcesses(+1);
    try {

      fRSSParser.parseURL(fUrl, function (err, feed) {
        if (err) {
          console.log('Youtube Error:', iSource.guid, iSource.title, err, fUrl);
          if (err) {
            console.log('Youtube ERRROR:', typeof (err), err, fUrl);
          }
          if (err.Error) {
            err = err.Error;
          }
          if (err.message) {
            err = err.message;
          }
          if (typeof (err) == "object")
            try {
              err = JSON.stringify(err);
            } catch (err) {
            }
          self.Utils.Sources[iSource.guid].Cron.YoutubeDate = new Date().toString();
          self.Utils.Sources[iSource.guid].Cron.YoutubeError = err;
        } else {
          console.log("Youtube Result", iSource.title, feed.items.length);
          var fFilesNew = 0;
          feed.items.forEach(function (entry) {
            var fItem = self.FormatItem(entry, iSource, 'youtube');
            if (self.AddItem(fItem)) fFilesNew++;
          });
          console.log("!", fFilesNew);
          self.CronNew = self.CronNew + fFilesNew;
          if ((self.Utils.Sources) && (iSource.guid) && (self.Utils.Sources[iSource.guid])) {
            if (!self.Utils.Sources[iSource.guid].Cron) {
              self.Utils.Sources[iSource.guid].Cron = {};
            }
            self.Utils.Sources[iSource.guid].Cron.YoutubeDate = new Date().toString();
            self.Utils.Sources[iSource.guid].Cron.YoutubeNew = fFilesNew;
            self.Utils.Sources[iSource.guid].Cron.YoutubeCount = feed.items.length;
            //self.Utils.Sources[iSource.guid].Cron.YoutubeDate = undefined;
            self.Utils.Sources[iSource.guid].Cron.YoutubeError = undefined;
          }
        }
        self.CronProcesses(-1);
      });
    }
    catch (err) {
      console.log('Youtube Error', iSource.title, err, fUrl);
      self.Utils.Sources[iSource.guid].Cron.YoutubeDate = new Date().toString();
      self.Utils.Sources[iSource.guid].Cron.YoutubeError = err;
    }
    /* END PullYoutube */
  },
  Cron: function (iData, iCallback) {
    self.Date = {};
    self.CronNew = 0;
    /*TraceOn*/ self.echoTest("Cron news started", '', 5); /*TraceOn*/
    for (var i in self.Utils.Sources) {
      try {
        if (self.Utils.isNotEmpty(self.Utils.Sources[i].rss_url)) {
          /*TraceOn*/ self.echoTest("RSS Source", self.Utils.Sources[i].title+'>'+self.Utils.Sources[i].rss_url, 5); /*TraceOn*/
          self.PullRSS(self.Utils.Sources[i].rss_url, self.Utils.Sources[i], i);
        }
        if (self.Utils.isNotEmpty(self.Utils.Sources[i].twitter_url)) {
          console.log('Twitter Source', self.Utils.Sources[i].title, self.Utils.Sources[i].twitter_url);
          self.PullTwitter(self.Utils.Sources[i].twitter_url, self.Utils.Sources[i], i);
        }
        if (self.Utils.isNotEmpty(self.Utils.Sources[i].youtube_url)) {
          console.log('Youtube Source', self.Utils.Sources[i].title, self.Utils.Sources[i].youtube_url);
          self.PullYoutube(self.Utils.Sources[i].youtube_url, self.Utils.Sources[i], i);
        }
      }
      catch (err) {
        console.log('Error cron', err);
        self.CronProcesses(0);
      }
    }

    if (iCallback) {
      iCallback(undefined, iData, self.Utils.Sources);
    }
  },
  Sync: function (iData, iCallback) {
    var fReturn;

		/**/ this.echoTest('News.Sync', iData, 1); /**/
    switch (iData.Name) {
      case "News-Init":
        if ((this.Utils.Config.Service.News.Cron) && (this.Utils.Config.Service.News.Cron.Active)) {
          self = this;
          var fMin = ((60 * 1000) * this.Utils.Config.Service.News.Cron.IntervalMin);
          if(0<fMin){
            this.CronTimer = setInterval(this.Cron, fMin);
          }
        }
        /**/ this.echoTest('News.Cron', this.Utils.Config.Service.News.Cron, 5); /**/
        break;
      case "News-Sources":
        var fData = this.Utils.getSources();
        if (iCallback) {
          iCallback(undefined, iData, fData);
        }
        fReturn = fData;
        break;
      case "News-Source":
        var fData = [];
        var fFiles = [];
        var fPath = (this.Utils.Config.Storage.default.Path) ? this.Utils.Config.Storage.default.Path : 'data/';
        dir = fPath + "news/" + iData.Row.guid + "/";
        var fWordStat = {};
        fs.readdir(dir, function (err, oFiles) {
          if (err) {
            console.log('Error', err);
          } else {
            fFiles = oFiles.map(function (fileName) {
              return {
                name: fileName,
                time: fs.statSync(dir + '/' + fileName).mtime.getTime()
              };
            })
              .sort(function (a, b) {
                return b.time - a.time;
              })
              .map(function (v) {
                return v.name;
              });
          }
          for (var i = 0; i < fFiles.length; i++) {
            if (50 < i) {
              break;
            }
            fFile = fFiles[i];
            if (fItem = self.Utils.getFileJson(dir + fFile)) {
              fData.push(fItem);
            }
          }
          if (iCallback) {
            iCallback(undefined, iData, fData);
          }
        });
        fReturn = fData;
        break;
      case "News-Source-Words":
        var fData = [];
        var fFiles = [];
        var fPath = (this.Utils.Config.Storage.default.Path) ? this.Utils.Config.Storage.default.Path : 'data/';
        dir = fPath + "news/" + iData.Row.guid + "/";
        var fWordStat = {};
        fs.readdir(dir, function (err, oFiles) {
          if (err) {
            console.log('Error', err);
          } else {
            fFiles = oFiles.map(function (fileName) {
              return {
                name: fileName,
                time: fs.statSync(dir + '/' + fileName).mtime.getTime()
              };
            })
              .sort(function (a, b) {
                return b.time - a.time;
              })
              .map(function (v) {
                return v.name;
              });
          }
          for (var i = 0; i < fFiles.length; i++) {
            if (50 < i) {
              break;
            }
            fFile = fFiles[i];
            if (fItem = self.Utils.getFileJson(dir + fFile)) {
              fWordStat = self.Utils.getWordStat(fWordStat, fItem.title);
            }
          }
          fData = self.Utils.getArray(fWordStat, 'Count', true);
          if ((fCount = iData.Row.count) && (fCount < fData.length)) {
            fData = fData.slice(0, fCount);
          }
          if (iCallback) {
            iCallback(undefined, iData, fData);
          }
        });
        fReturn = fData;
        break;
      case "News-Live":
        var addItem = function (iLink) {
          var fItem = {
            uid: iLink,
            guid: "",
            date: "",
            link: iLink,
            title: "",
            description: "",
            source_id: "",
            type_id: 'youtube'
          };
          fItem.guid = this.Utils.getMD5(fItem.uid);

          return fItem;
        }
        var fData = { Items: [] };
        fData.Items.push(addItem("https://www.youtube.com/watch?v=KPHa9AyjGnk"));
        fData.Items.push(addItem("https://www.youtube.com/watch?v=NMre6IAAAiU"));
        fData.Items.push(addItem("https://www.youtube.com/watch?v=qhfFgKlDM8E"));

        if (iCallback) {
          iCallback(undefined, iData, fData);
        }
        break;
      case "News-Day":
        //this.UseCache = true;
        if (this.UseCache && iCallback && this.Cache[iData.Row.Date]) {
          iCallback(undefined, iData, this.Cache[iData.Row.Date]);
					/*TraceOn*/ this.echoTest("Cache hit", iData.Row.Date, 5); /*TraceOn*/
          return;
        }

        var fData = [];
        var fDateFile = this.Path + 'd' + iData.Row.Date + '.db';
        console.log('Loading:', fDateFile);
        if (file.existsSync(fDateFile)) {
          try {
            fData = JSON.parse(file.readFileSync(fDateFile));
          }
          catch (err) {
            console.log('Error', fDateFile, err);
          }
        }

        var fChoise = [];
        var fChoiseSet = {};
        var fNews = [];
        var fSocial = [];
        var fSources = this.Utils.getObject(this.Utils.getSources(), 'guid');

        for (i = 0; i < fData.length; i++) {
          var fSourceSection = ((fSources[fData[i].source_id]) && (fSources[fData[i].source_id].source_section)) ? fSources[fData[i].source_id].source_section.toLowerCase() : "";
          var fSourceId = fData[i].source_id;
          //console.log('fSourceSection', fSourceSection);
          if (fSourceSection.indexOf('choise') != -1) {
            if (fChoiseSet[fSourceId]) {
              var fIndex = fChoiseSet[fSourceId] - 1;
              if (fChoise[fIndex].items.length < 3) {
                fChoise[fIndex].items.push(fData[i]);
              }
            } else {
              var fRow = fData[i];
              fRow.items = [];
              fChoise.push(fRow);
              fChoiseSet[fSourceId] = fChoise.length;
            }

          }
          if (fSourceSection.indexOf('news') != -1) {
            fNews.push(fData[i]);
          }
          if (fSourceSection.indexOf('social') != -1) {
            fSocial.push(fData[i]);
          }
        }
        fData = { Choise: fChoise, News: fNews, Social: fSocial };

        if (iCallback) {
          iCallback(undefined, iData, fData);
        }
        this.Cache[iData.Row.Date] = fData;
        break;
      case "News-Cron":
        this.Cron(iData, iCallback);
        break;
    }

    return fReturn;
  }
}