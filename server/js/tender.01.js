module.exports = {
	Utils: null,
	UseCache: false,
	Cache: {
		Index: [],
		Table: {}
	},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Tender.Cache', this.UseCache);
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fCallbackExec = true;
		/**/ this.Utils.echoTest('Tender.Sync', iData, 1); /**/
		switch (iData.Name) {
			case "Tender-Init":
        var self = this;
				this.Utils.setDataMap('tender', undefined, function(){
					self.Cache.Table = self.Utils.getData('tender', self.Cache.Table, { Page:0 });
					if(Array.isArray(self.Cache.Table)){
						self.Cache.Index = self.Cache.Table.slice(0, 4);
					}
					fResult = self.Utils.getClone(self.Cache.Table);
				});
			break;
			case "Tender-List":
				if (iData.Action) {
					switch(iData.Action){
						case "detail":
							fResult = this.Utils.getData('tender', this.Cache.Table, iData.Row);
						break;
						case "view":
						break;
						case "delete":
						case "save":
							this.Cache.Table = this.Utils.setData('tender', this.Cache.Table, iData.Row, iData.Action);
							this.Utils.setEvent({ Name:  iData.Name});
							fResult = this.Utils.getClone(this.Cache.Table);
						break;
						default:
							var fPage = 0;
							if(iData.Row&&iData.Row.Page){
								fPage = iData.Row.Page;
							}
							fResult = this.Utils.getData('tender', this.Cache.Table, { Page: fPage });
							if(fPage==0){
								this.Cache.Table = fResult;
								if(Array.isArray(this.Cache.Table)){
									this.Cache.Index = this.Cache.Table.slice(0, 4);
								}
							}
						break;
					}
				}else{
					fResult = this.Utils.getClone(this.Cache.Table);
				}
			break;
			case "Tender-Index":
				this.Cache.Table = this.Utils.getArray(this.Utils.getData('tender', this.Cache.Table, { Page:0 }));
				if(Array.isArray(this.Cache.Table)){
					this.Cache.Index = this.Cache.Table.slice(0, 4);
				}
				fResult = this.Cache.Index;
			break;
		}
		if (iCallback&&fCallbackExec) {
			iCallback(undefined, iData, fResult);
		}
	}
}