const csv=require('csvtojson');
var atob = require('atob');
var CronStats;

module.exports = {
	Utils: null,
	UseCache: false,
	TableName: "election",
	Cache: {
		Index: [],
		Table: {},
		Users: {}
	},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Service.Election.Cache', this.UseCache);
	},
	setStats: function(iTable){
		if(CronStats) {
			clearTimeout(CronStats);
		}
		fTable = (iTable)? iTable:this.Utils.getData(this.TableName, {});
/*
		var fStat = {
			Status: {},
			Leads: {

			}
		};
		var fCodebook = SiteLng.Participant.status;
		for(var fKey in iData){
			var fRow = iData[fKey];
			if(fRow.leadname){
				if(fStat.Leads[fRow.leadname]){
					fStat.Leads[fRow.leadname].leadcount = fStat.Leads[fRow.leadname].leadcount+1;
				}else{
					fStat.Leads[fRow.leadname] = {
						leadname: fRow.leadname,
						leadcontact: fRow.leadcontact,
						leadcount: 1
					}
				}
			}
			if(fStat.Status[fRow.status]){
				fStat.Status[fRow.status].Value = fStat.Status[fRow.status].Value+1;
				if(fRow.votestatus){
					fStat.Status[fRow.status].Vote = fStat.Status[fRow.status].Vote+1;
				}
			}else{
				fStat.Status[fRow.status] = {
					Key: fRow.status,
					Title: fRow.status,
					Value: 1,
					Vote: ((fRow.votestatus)? 1:0),
					Color: undefined
				}
				if(fCodeItem=fCodebook.Rows[fRow.status]){
					fStat.Status[fRow.status].Title = fCodeItem.title;
					fStat.Status[fRow.status].Color = fCodeItem.color;
				}
			}
		}
		//STAT
		var fChart = {
			labels: [],
			datasets: [{
				data: [],
				backgroundColor: [],
				label: 'Test'
			}]
		};
		try{
			fStat.StatusSort = Client.getSortBy(Client.getArray(fStat.Status), 'Value', 1);
			if ((fStat.Status)&&(fData = Client.getSortBy(Client.getArray(fStat.Status), 'Value', 1))) {
				for (r = 0; r < fData.length; r++) {
					var fTitle = fData[r].Title;
					var fValue = fData[r].Value;
					var fColor = fData[r].Color;
					fChart.datasets[0].data.push(fValue);
					fChart.datasets[0].backgroundColor.push(fColor);
					fChart.labels.push(fTitle);
				}
			};
		}catch(err){

		}

		self.set('StatStatus', fChart);
		self.set('Stat', fStat);

		console.log('Stats');
		this.Utils.Event('Election-Stats', fStat, undefined, 'x');
		*/
	},
	getStats: function(iTable, iStats){
		var fStat = (iStats)? iStats:{
			guid: iData.Row.guid,
			Status: {},
			Leads: {}
		};
		var fCodebook = {
			default: { guid: "", title: "(prazno)", color: "#414a45" },
			S: { guid: "S", title: "siguran", color: "#8bc7a3" },
			R: { guid: "R", title: "razgovor", color: "#4287f5" },
			P: { guid: "P", title: "potencijal", color: "#0c3475" },
			I: { guid: "I", title: "inostranstvo", color: "#d0d3d9" },
			O: { guid: "O", title: "opozicija", color: "#c78b91" },
			N: { guid: "N", title: "nepoznati", color: "#414a45" },
			A: { guid: "A", title: "apstinent", color: "#c1c482" },
			U: { guid: "U", title: "umrli", color: "#414a45" }
		};
		for(var fKey in iTable){
			var fRow = iTable[fKey];
			fRow.status = fRow.status.toUpperCase();
			fRow.guid = this.Utils.getBase64(fRow.leadname).replace('==', '').replace('=', '');
			if(fRow.leadname&&fRow.status.toUpperCase()=="S"){
				if(fStat.Leads[fRow.guid]){
					fStat.Leads[fRow.guid].leadcount = fStat.Leads[fRow.guid].leadcount+1;
					fStat.Leads[fRow.guid].title = fRow.leadname+' ('+fStat.Leads[fRow.guid].leadcount+')';
					fStat.Leads[fRow.guid].vote = fStat.Leads[fRow.guid].vote+((fRow.votestatus&&(fRow.votestatus=="vote"))? 1:0);
					fStat.Leads[fRow.guid].users.push(fRow);
				}else{
					fStat.Leads[fRow.guid] = {
						guid: fRow.guid,
						title: fRow.leadname,
						leadname: fRow.leadname,
						leadcontact: fRow.leadcontact,
						leadcount: 1,
						vote: ((fRow.votestatus&&(fRow.votestatus=="vote"))? 1:0),
						users: [fRow]
					}
				}
			}
			var fStatus = fRow.status.toUpperCase().trim();
			if(fStat.Status[fStatus]){
				fStat.Status[fStatus].Value = fStat.Status[fStatus].Value+1;
				if(fRow.votestatus&&(fRow.votestatus=="vote")){
					fStat.Status[fStatus].Vote = fStat.Status[fStatus].Vote+1;
				}
			}else{
				fStat.Status[fStatus] = {
					Key: fRow.status,
					Title: fRow.status,
					Value: 1,
					Vote: ((fRow.votestatus&&(fRow.votestatus=="vote"))? 1:0),
					Color: undefined
				}
				if(fCodeItem=fCodebook[fStatus]){
					fStat.Status[fStatus].Title = fCodeItem.title;
					fStat.Status[fStatus].Color = fCodeItem.color;
				}
			}
		}
		var fChart = {
			labels: [],
			datasets: [{
				data: [],
				backgroundColor: [],
				label: 'Test'
			}]
		};
		try{
			fStat.StatusSort = this.Utils.getArray(fStat.Status, 'Value', 1);
			if ((fStat.Status)&&(fData = this.Utils.getArray(fStat.Status, 'Value', 1))) {
				for (r = 0; r < fData.length; r++) {
					var fTitle = fData[r].Title;
					var fValue = fData[r].Value;
					var fColor = fData[r].Color;
					fChart.datasets[0].data.push(fValue);
					fChart.datasets[0].backgroundColor.push(fColor);
					fChart.labels.push(fTitle);
				}
			};
		}catch(err){
			console.log('STAT Chart Error', err);
		}
		fStat.Chart = fChart;

		return fStat;
	},
	setFile: function(iFileData){
		var fFileContent = iFileData.members_file;
		fFileContent = fFileContent.replace('data:application/octet-stream;base64,', '');
		fFileContent = atob(fFileContent);
		var fPath = (this.Utils.Config.Storage.default.Path) ? this.Utils.Config.Storage.default.Path : 'data/';
		var self = this;

		csv({
			noheader:false,
			output: "csv",
			encoding: 'utf-8'
		})
		.fromString(fFileContent)
		.then((csvRows)=>{ 
			/**/ self.Utils.echoTest('Stat.Pull.Result', csvRows, 8); /**/ 
			var fUsers = {};
			for(var i=0; i<csvRows.length; i++){
				//console.log(csvRows[i]);
				/*var fUser = {
					guid: self.Utils.getGuid(),
					rev: 1,
					date: new Date(),
					ident: self.Utils.getValue(csvRows[i], 0), 
					mb: self.Utils.getValue(csvRows[i], 3),
					firstname: self.Utils.getValue(csvRows[i], 6),
					familyname: self.Utils.getValue(csvRows[i], 5),
					emails:self.Utils.getValue(csvRows[i], undefined),
					contacts: self.Utils.getValue(csvRows[i], undefined),
					cid: self.Utils.getValue(csvRows[i], 7),
					gid: self.Utils.getValue(csvRows[i], undefined),
					birthdate: self.Utils.getValue(csvRows[i], 10),
					birthplace: self.Utils.getValue(csvRows[i], 11),
					birthstate: self.Utils.getValue(csvRows[i], 12),
					address: self.Utils.getValue(csvRows[i], 8),
					city: self.Utils.getValue(csvRows[i], 2),
					state: self.Utils.getValue(csvRows[i], undefined, "MNE"),
					sex: self.Utils.getValue(csvRows[i], undefined),
					religion: self.Utils.getValue(csvRows[i], undefined),
					education: self.Utils.getValue(csvRows[i], undefined),
					status: self.Utils.getValue(csvRows[i], 16),
					votepuid: self.Utils.getValue(csvRows[i], 3),
					voteplace: self.Utils.getValue(csvRows[i], 4),
					description: self.Utils.getValue(csvRows[i], 19)
				};*/
				var fUser = {
					guid: self.Utils.getGuid(),
					rev: 1,
					date: new Date(),
					ident: self.Utils.getValue(csvRows[i], 0), 
					mb: self.Utils.getValue(csvRows[i], 1),
					firstname: self.Utils.getValue(csvRows[i], 2),
					familyname: self.Utils.getValue(csvRows[i], 3),
					emails:self.Utils.getValue(csvRows[i], 4),
					contacts: self.Utils.getValue(csvRows[i], 5),
					cid: self.Utils.getValue(csvRows[i], 6),
					gid: self.Utils.getValue(csvRows[i], 7),
					birthdate: self.Utils.getValue(csvRows[i], 8),
					birthplace: self.Utils.getValue(csvRows[i], 9),
					birthstate: self.Utils.getValue(csvRows[i], 10),
					address: self.Utils.getValue(csvRows[i], 11),
					address: self.Utils.getValue(csvRows[i], 12),
					city: self.Utils.getValue(csvRows[i], 13),
					state: self.Utils.getValue(csvRows[i], 14),
					sex: self.Utils.getValue(csvRows[i], 15),
					religion: self.Utils.getValue(csvRows[i], 16),
					education: self.Utils.getValue(csvRows[i], 17),
					status: self.Utils.getValue(csvRows[i], 18),
					votepuid: self.Utils.getValue(csvRows[i], 19),
					voteplace: self.Utils.getValue(csvRows[i], 20),
					description: self.Utils.getValue(csvRows[i], 21)
				};
				var fMemberFile = self.Utils.getPath(fPath+"election/", iFileData.guid, fUser.guid);
				this.Utils.setFileJson(fMemberFile, fUser);
				fUsers[fUser.guid] = fUser;
			}
			var fMembersFile = self.Utils.getPath(fPath+"election/", iFileData.guid, 'members');
			this.Utils.setFileJson(fMembersFile, fUsers);
		});
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fPath = this.Utils.getStorage(this.TableName, iData);
		var fCallbackExec = true;
		/**/ this.Utils.echoTest('Sync Election', iData, 5); /**/
		var fPath = this.Utils.getStorage('', iData);
		switch (iData.Name) {
			case "Election-Init":
				if(this.UseCache){
					fResult = this.Utils.getData(this.TableName, {});
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
				break;
			case "Election-Users":
				//var fMembersPath = this.Utils.getPath(fPath+this.TableName, iData.Row.pguid);
				//var fMembersFile = this.Utils.getPath(fPath+this.TableName, iData.Row.pguid, 'members');
				var fMembersFile = this.Utils.getPath(fPath+this.TableName, '/'+iData.Row.pguid+'-'+'member');
				//console.log('Elections Particpient File', fMembersFile);
				switch(iData.Action) {
					case "action":
						var fUsers = {};
						if((this.UseCache)&&(iData.Row.pguid)&&(this.Cache.Users&&this.Cache.Users[iData.Row.pguid])){
							fUsers = this.Cache.Users[iData.Row.pguid];
						}else{
							fUsers = this.Utils.getFileJson(fMembersFile, {});
						}
						//this.Utils.setData('election/'+iData.Row.pguid, undefined, iData.Row, iData.Action);
						//fUsers = this.Utils.getObject(fUsers , 'guid');
						if(fUsers[iData.Row.guid]){
							fUsers[iData.Row.guid].votestatus =  (iData.Row.votestatus=="clear")? "":iData.Row.votestatus;
							this.Utils.setFileJson(fMembersFile, fUsers);
						}

						var fcount_secure = 0;
						var fmax_secure = 0;
						for(var fKey in fUsers){
							fRow = fUsers[fKey];
							fRow.status = fRow.status.toUpperCase();
							if(fRow.status=="S"){
								fmax_secure++;
								if(fRow.votestatus=="vote"){
									fcount_secure++;
								}
							}
						}
						var fElections = this.Utils.getData(this.TableName, {}, undefined, fPath);
						var fElection = fElections[iData.Row.pguid];
						fElection.went_max_secure=fmax_secure;
						fElection.went_count_secure=fcount_secure;
						fElections = this.Utils.setData(this.TableName, fElections, fElection, 'save', fPath);
						var fData = { Row: iData.Row, Table: fElections, Session: iData.session };
						this.Utils.Event('Election', fData, iData.session, 'x');

						if(this.UseCache&&iData.Row.pguid){
							this.Cache.Users[iData.Row.pguid] = fUsers;
						}
						fResult = {
							Users: fUsers,
							Elections: fElections
						}
					break;
					case "detail":
					//fResult = this.Utils.getData('member', this.Cache.Table, iData.Row);
					break;
					case "insert": case "update": case "save": case "delete":
						var fUsers = {};
						if((this.UseCache)&&(iData.Row.pguid)&&(this.Cache.Users&&this.Cache.Users[iData.Row.pguid])){
							fUsers = this.Cache.Users[iData.Row.pguid];
						}else{
							fUsers = this.Utils.getFileJson(fMembersFile, {});
						}
						//console.log('setData', 'election/'+iData.Row.pguid);
						//this.Utils.setData('election/'+iData.Row.pguid, undefined, iData.Row, iData.Action);
						fUsers = this.Utils.getObject(fUsers , 'guid');
						if(iData.Action=="delete"){
							delete(fUsers[iData.Row.guid]);
						}else{
							fUsers[iData.Row.guid] = iData.Row;
						}
						this.Utils.setFileJson(fMembersFile, fUsers);

						if(this.UseCache&&iData.Row.pguid){
							this.Cache.Users[iData.Row.pguid] = fUsers;
						}
						fResult = this.Utils.getClone(fUsers);
					break;
					default:
						fResult =  this.Utils.getFileJson(fMembersFile, {});
						if(this.UseCache&&iData.Row.pguid){
							this.Cache.Users[iData.Row.pguid] = this.Utils.getClone(fResult);
						}
					break;
				}
				break
			case "Election-Stats":
				var fItemsFile = this.Utils.getPath(fPath+this.TableName, '/'+iData.Row.guid+'-'+'member');
				fUsers = this.Utils.getFileJson(fItemsFile, {});

				var fStat = {
					pguid: iData.Row.guid,
          Status: {},
          Leads: {}
        };
        var fCodebook = {
					default: { guid: "", title: "(prazno)", color: "#414a45" },
					S: { guid: "S", title: "siguran", color: "#8bc7a3" },
					R: { guid: "R", title: "razgovor", color: "#4287f5" },
					P: { guid: "P", title: "potencijal", color: "#0c3475" },
					I: { guid: "I", title: "inostranstvo", color: "#d0d3d9" },
					O: { guid: "O", title: "opozicija", color: "#c78b91" },
					N: { guid: "N", title: "nepoznati", color: "#414a45" },
					A: { guid: "A", title: "apstinent", color: "#c1c482" },
					U: { guid: "U", title: "umrli", color: "#414a45" }
				};
        for(var fKey in fUsers){
          var fRow = fUsers[fKey];
					fRow.status = fRow.status.toUpperCase();
					fRow.guid = this.Utils.getBase64(fRow.leadname);
          if(fRow.leadname&&fRow.status.toUpperCase()=="S"){
            if(fStat.Leads[fRow.guid]){
							fStat.Leads[fRow.guid].leadcount = fStat.Leads[fRow.guid].leadcount+1;
							fStat.Leads[fRow.guid].title = fRow.leadname+' ('+fStat.Leads[fRow.guid].leadcount+')';
            }else{
              fStat.Leads[fRow.guid] = {
								guid: fRow.guid,
								title: fRow.leadname,
                leadname: fRow.leadname,
                leadcontact: fRow.leadcontact,
                leadcount: 1
              }
            }
          }
          if(fStat.Status[fRow.status]){
            fStat.Status[fRow.status].Value = fStat.Status[fRow.status].Value+1;
            if(fRow.votestatus&&(fRow.votestatus=="vote")){
              fStat.Status[fRow.status].Vote = fStat.Status[fRow.status].Vote+1;
            }
          }else{
            fStat.Status[fRow.status] = {
              Key: fRow.status,
              Title: fRow.status,
              Value: 1,
              Vote: ((fRow.votestatus&&(fRow.votestatus=="vote"))? 1:0),
              Color: undefined
            }
            if(fCodeItem=fCodebook[fRow.status]){
              fStat.Status[fRow.status].Title = fCodeItem.title;
              fStat.Status[fRow.status].Color = fCodeItem.color;
            }
          }
				}
        var fChart = {
          labels: [],
          datasets: [{
            data: [],
            backgroundColor: [],
            label: 'Test'
          }]
        };
        try{
          fStat.StatusSort = this.Utils.getArray(fStat.Status, 'Value', 1);
          if ((fStat.Status)&&(fData = this.Utils.getArray(fStat.Status, 'Value', 1))) {
            for (r = 0; r < fData.length; r++) {
              var fTitle = fData[r].Title;
              var fValue = fData[r].Value;
              var fColor = fData[r].Color;
              fChart.datasets[0].data.push(fValue);
              fChart.datasets[0].backgroundColor.push(fColor);
              fChart.labels.push(fTitle);
            }
					};
        }catch(err){
					console.log('STAT Chart Error', err);
				}
				fStat.Chart = fChart;
				//fTable = this.Utils.setData(this.TableName, fTable, undefined, undefined, fPath);
				//console.log('STAT End');
				fResult = this.Utils.getClone(fStat);
				break;
			case "Election-Info":
				var fStat = {
					guid: iData.Row.guid,
					Status: {},
					Leads: {}
				};
				if(iData.Row.Items&&(fCount=iData.Row.Items.length)){
					for(iis=0; iis<fCount; iis++){
						var fItemsFile = this.Utils.getPath(fPath+this.TableName, '/'+iData.Row.Items[iis].guid+'-'+'member');
						fUsers = this.Utils.getFileJson(fItemsFile, {});
						fStat = this.getStats(fUsers, fStat);
					}
				}else{
					var fItemsFile = this.Utils.getPath(fPath+this.TableName, '/'+iData.Row.guid+'-'+'member');
					fUsers = this.Utils.getFileJson(fItemsFile, {});
					fStat = this.getStats(fUsers, fStat);
				}

				fResult = this.Utils.getClone(fStat);
				break
			case "Election-Log":
				var fLogFile = this.Utils.getPath(fPath+this.TableName, '/'+iData.Row.euid+'-'+iData.Row.puid+'-'+'log');
				var fTable = this.Utils.getFileJson(fLogFile, []);
				//console.log('Log', fLogFile, iData, fTable);
				switch(iData.Action||"") {
					case "insert":
						fTable.unshift(iData.Row);
						this.Utils.setFileJson(fLogFile, fTable);
						this.Utils.Event('Election.Log', iData.Row, iData.session, 'x');
					break;
				}

				fResult = this.Utils.getClone(fTable);
				break;
			case "Election-Reset":
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);

				var fTableCount = Object.keys(fTable).length;
				var fTablePos = 0;
				var fTablePrc = 0;

				for(var fKey in fTable){

					var fMembersFile = this.Utils.getPath(fPath+this.TableName, '/'+fTable[fKey].guid+'-'+'member');
					var fUsers = this.Utils.getFileJson(fMembersFile, {});
					var fUserSecure = 0;
					var fUserApsent = 0;
					var fUserCount = 0;
					for(var fUserUID in fUsers){
						fUserCount++;
						if((fUsers[fUserUID].status)&&(fUsers[fUserUID].status.toUpperCase() =="S" )) { fUserSecure++ };
						if((fUsers[fUserUID].status)&&(fUsers[fUserUID].status.toUpperCase() =="A" )) { fUserApsent++ };

						fUsers[fUserUID].notes = [];
						fUsers[fUserUID].votestatus = "";
					}
					this.Utils.setFileJson(fMembersFile, fUsers);
					//var fRow = fTable[fKey];
					fTable[fKey].went_count_abstinent = 0;
					fTable[fKey].went_max_abstinent = fUserApsent;
					fTable[fKey].went_count_secure = 0;
					fTable[fKey].went_max_secure = fUserSecure;
					fTable[fKey].went_count = 0;
					fTable[fKey].went_max = fUserCount;
					if(fTable[fKey].members_fileareset){
						delete(fTable[fKey].members_fileareset);
					}
					this.Utils.setData(this.TableName, fTable, fTable[fKey], 'save', fPath);
					fTablePos++;
				}

				fTable = this.Utils.setData(this.TableName, fTable, undefined, undefined, fPath);
				fResult = this.Utils.getClone(fTable);
				if(this.UseCache){
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
				var fData = { Row: undefined, Table: fTable, Session: iData.Session };
				this.Utils.Event('Election', fData, iData.session, 'x');
			break;
			case "Election-List":
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);
				switch(iData.Action||"") {
					case "minus":
						var fRow = this.Utils.getData(this.TableName, this.Cache.Table, iData.Row, fPath);
						fRow.went_count = fRow.went_count-1;
						if(fRow.went_count<0){
							fRow.went_count = 0;
						}
						fTable = this.Utils.setData(this.TableName, fTable, fRow, 'save', fPath);
						fResult = this.Utils.getClone(fTable);
						var fData = { Row: fRow, Table: fTable, Session: iData.session };
						this.Utils.Event('Election', fData,iData.Trace.session, 'x');
						var fLog = {
							guid: this.Utils.getGuid(),
							euid: fRow.guid,
							action: 'minus',
							title: fRow.title+"&nbsp;&nbsp;<span class='label label-danger'>-1</span>"+" stanje <span class='badge'>"+fRow.went_count+"</span>", 
							notify: fRow.title+" -1"+" stanje "+fRow.went_count+"", 
							data: { Election: fRow },
							UserName: iData.Trace.session.UserName,
							UserUID: iData.Trace.session.UserUID,
							session: iData.Trace.session,
							date: new Date()
						};
						this.Utils.Event('Election.Log', fLog, iData.Trace.session, 'x');
					break;
					case "plus":
						var fRow = this.Utils.getData(this.TableName, this.Cache.Table, iData.Row, fPath);
						fRow.went_count = fRow.went_count+1;
						fTable = this.Utils.setData(this.TableName, fTable, fRow, 'save', fPath);
						fResult = this.Utils.getClone(fTable);
						var fData = { Row: fRow, Table: fTable, Session: iData.session };
						this.Utils.Event('Election', fData, iData.Trace.session, 'x');
						var fLog = {
							guid: this.Utils.getGuid(),
							euid: fRow.guid,
							action: 'plus',
							title: fRow.title+"&nbsp;&nbsp;<span class='label label-success'>+1</span>"+" stanje <span class='badge'>"+fRow.went_count+"</span>",  
							notify: fRow.title+" +1"+" stanje "+fRow.went_count+"", 
							data: { Election: fRow },
							UserName: iData.Trace.session.UserName,
							UserUID: iData.Trace.session.UserUID,
							session: iData.Trace.session,
							date: new Date()
						};
						this.Utils.Event('Election.Log', fLog, iData.Trace.session, 'x');
					break;
					case "detail":
						fResult = this.Utils.getData(this.TableName, this.Cache.Table, iData.Row, fPath);
					break;
					case "insert": case "update": case "save": case "delete":
						if(iData.Row.members_file){
							this.setFile(iData.Row);
							delete(iData.Row.members_file);
						}
						if(iData.Row.members_fileareset){
							delete(iData.Row.members_fileareset);
						}

						fTable = this.Utils.setData(this.TableName, fTable, iData.Row, iData.Action, fPath);
						fResult = this.Utils.getClone(fTable);
						var fData = { Row: iData.Row, Table: fTable, Session: iData.session };
						this.Utils.Event('Election', fData, iData.session, 'x');
						//CronStats = setTimeout(this.setStats(fTable), 1000);
					break;
					default:
						fTable = this.Utils.getData(this.TableName, fTable, undefined, fPath);
						fResult = this.Utils.getClone(fTable);
					break;
				}
				if(this.UseCache){
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
			break;
			case "Election-Search":
				var self = this;
				fCallbackExec = false;
				fPath = fPath+this.TableName+"/";
				var exec = require('child_process').exec;
				var execute = function(command, callback){
						exec(command, function(error, stdout, stderr){ callback(stdout); });
				};
				var fCmd = 'grep -ri -l "'+iData.Row.Find+'" '+fPath;
				/**/ this.Utils.echoTest('Searching Member', fCmd, 5); /**/
				execute(fCmd, function(oData){
					var fResult = [];
					var array = oData.toString().split("\n");
					for(i in array) {
						if((fFile = array[i])&&(fFile)){
							var fRows = self.Utils.getFileJson(fFile, []);
							for(j in fRows) {
								if(fRows[j]&&fRows[j].firstname&&JSON.stringify(fRows[j]).toString().toLowerCase().indexOf(iData.Row.Find.trim().toLowerCase())!=-1){
									fResult.push(fRows[j]);
								}
							}
						}
					}

					if(iCallback&&fResult) {
						iCallback(undefined, iData, fResult);
					}
				});
				//console.log('election', this.Cache.Table, iData.Row, iData.Action);
			break;
			case "Election-Rpt1":
				fResult = [];
				var fElection = this.Utils.getData(this.TableName, {});
				for(fElId in fElection) {
					var fItemsFile = this.Utils.getPath(fPath+this.TableName, '/'+fElection[fElId].guid+'-'+'member');
					var fItems = this.Utils.getFileJson(fItemsFile, {});
					console.log(fElId, fItemsFile);
					for(fId in fItems) {
						var fItem = fItems[fId];

						if(fItem.status.toUpperCase()=='S'){
							/*var fLogsFile = this.Utils.getPath(fPath+this.TableName, '/'+fElection[fElId].guid+'-'+fItems[fId].guid+'-log');
							var fLogs = this.Utils.getFileJson(fLogsFile, []);
							fItem.Logs = fLogs;*/
							if(fItem.session){
								delete(fItem.session);
							}
							fResult.push(fItem);
						}
					}
				}
				this.Utils.setFileJson('data/test/election/rpt1.db', fResult);
			break;
		}

		if (iCallback&&fCallbackExec&&fResult) {
			iCallback(undefined, iData, fResult);
		}
	}
}