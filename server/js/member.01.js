const csv=require('csvtojson');
var atob = require('atob');

module.exports = {
	Engine: null,
	UseCache: false,
	Type: "member",
	Path: "",
	Cache: {
		Index: [],
		Table: {},
		Users: {}
	},
	Members: {},
	setUtils: function (iEngine) {
		if (!this.Engine) {
			this.Engine = iEngine;
		}
		this.UseCache = this.Engine.getConfig('Service.Member.Cache', this.UseCache);
	},
	setFile: function(iFileData){
		var fFileContent = iFileData.members_file;
		fFileContent = fFileContent.replace('data:application/octet-stream;base64,', '');
		fFileContent = atob(fFileContent);
		var fPath = (this.Engine.Config.Storage.default.Path) ? this.Engine.Config.Storage.default.Path : 'data/';
		var self = this;

		csv({
			noheader:false,
			output: "csv",
			encoding: 'utf-8'
		})
		.fromString(fFileContent)
		.then((csvRows)=>{ 
			/**/ self.Engine.echoTest('Stat.Pull.Result', csvRows, 8); /**/ 
			var fUsers = {};
			for(var i=0; i<csvRows.length; i++){
				//console.log(csvRows[i]);
				/*var fUser = {
					guid: self.Engine.getGuid(),
					rev: 1,
					date: new Date(),
					ident: self.Engine.getValue(csvRows[i], 0), 
					mb: self.Engine.getValue(csvRows[i], 3),
					firstname: self.Engine.getValue(csvRows[i], 6),
					familyname: self.Engine.getValue(csvRows[i], 5),
					emails:self.Engine.getValue(csvRows[i], undefined),
					contacts: self.Engine.getValue(csvRows[i], undefined),
					cid: self.Engine.getValue(csvRows[i], 7),
					gid: self.Engine.getValue(csvRows[i], undefined),
					birthdate: self.Engine.getValue(csvRows[i], 10),
					birthplace: self.Engine.getValue(csvRows[i], 11),
					birthstate: self.Engine.getValue(csvRows[i], 12),
					address: self.Engine.getValue(csvRows[i], 8),
					city: self.Engine.getValue(csvRows[i], 2),
					state: self.Engine.getValue(csvRows[i], undefined, "MNE"),
					sex: self.Engine.getValue(csvRows[i], undefined),
					religion: self.Engine.getValue(csvRows[i], undefined),
					education: self.Engine.getValue(csvRows[i], undefined),
					status: self.Engine.getValue(csvRows[i], 16),
					votepuid: self.Engine.getValue(csvRows[i], 3),
					voteplace: self.Engine.getValue(csvRows[i], 4),
					description: self.Engine.getValue(csvRows[i], 19)
				};*/
				var fUser = {
					guid: self.Engine.getGuid(),
					rev: 1,
					date: new Date(),
					ident: self.Engine.getValue(csvRows[i], 0), 
					mb: self.Engine.getValue(csvRows[i], 1),
					firstname: self.Engine.getValue(csvRows[i], 2),
					familyname: self.Engine.getValue(csvRows[i], 3),
					emails:self.Engine.getValue(csvRows[i], 4),
					contacts: self.Engine.getValue(csvRows[i], 5),
					cid: self.Engine.getValue(csvRows[i], 6),
					gid: self.Engine.getValue(csvRows[i], 7),
					birthdate: self.Engine.getValue(csvRows[i], 8),
					birthplace: self.Engine.getValue(csvRows[i], 9),
					birthstate: self.Engine.getValue(csvRows[i], 10),
					address: self.Engine.getValue(csvRows[i], 11),
					address: self.Engine.getValue(csvRows[i], 12),
					city: self.Engine.getValue(csvRows[i], 13),
					state: self.Engine.getValue(csvRows[i], 14),
					sex: self.Engine.getValue(csvRows[i], 15),
					religion: self.Engine.getValue(csvRows[i], 16),
					education: self.Engine.getValue(csvRows[i], 17),
					status: self.Engine.getValue(csvRows[i], 18),
					votepuid: self.Engine.getValue(csvRows[i], 19),
					voteplace: self.Engine.getValue(csvRows[i], 20),
					description: self.Engine.getValue(csvRows[i], 21)
				};
				var fMemberFile = self.Engine.getPath(fPath+"member/", iFileData.guid, fUser.guid);
				this.Engine.setFileJson(fMemberFile, fUser);
				fUsers[fUser.guid] = fUser;
			}
			var fMembersFile = self.Engine.getPath(fPath+"member/", iFileData.guid, 'members');
			this.Engine.setFileJson(fMembersFile, fUsers);
		});
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fTable = {};
		var fCallbackExec = true;
		/**/ this.Engine.echoTest('Member.Sync', iData, 5); /**/
		//var fPath = this.Engine.getStorage('', iData);
		switch (iData.Name) {
			case "Member-Init":
				this.Path = this.Engine.getStorage(this.Type, iData);
				if(this.UseCache){
					fResult = this.Engine.getData(this.Type, {}, undefined, this.Path);
					this.Engine.setCache(this.Path, fResult, this.Type);
				}
				break;
			case "Member-Users":
				var fMembersPath = this.Engine.getPath(this.Path+this.Type, iData.Row.pguid);
				var fMembersFile = this.Engine.getPath(this.Path+this.Type, iData.Row.pguid, 'members');
				console.log('fMembersFile', fMembersFile);
				switch(iData.Action) {
					case "detail":
					//fResult = this.Engine.getData('member', this.Cache.Table, iData.Row);
					break;
					case "insert": 
					case "update": 
					case "save": 
					case "delete":
						var fUsers = {};
						if((this.UseCache)&&(iData.Row.pguid)&&(this.Cache.Users&&this.Cache.Users[iData.Row.pguid])){
							fUsers = this.Cache.Users[iData.Row.pguid];
						}else{
							fUsers = this.Engine.getFileJson(fMembersFile, {});
						}
						console.log('setData', 'member/'+iData.Row.pguid);
						iData.Row.puid = iData.Row.pguid.toString();
						this.Engine.setData('member', undefined, iData.Row, iData.Action);
						fUsers = this.Engine.getObject(fUsers , 'guid');
						if(iData.Action=="delete"){
							delete(fUsers[iData.Row.guid]);
						}else{
							fUsers[iData.Row.guid] = iData.Row;
						}
						this.Engine.setFileJson(fMembersFile, fUsers);

						if(this.UseCache&&iData.Row.pguid){
							this.Cache.Users[iData.Row.pguid] = fUsers;
						}
						fResult = this.Engine.getClone(fUsers);
					break;
					default:
						fResult =  this.Engine.getFileJson(fMembersFile, {});
						if(this.UseCache&&iData.Row.pguid){

							this.Cache.Users[iData.Row.pguid] = this.Engine.getClone(fResult);
						}
					break;
				}
				break
			case "Member-List":
				fTable = this.Engine.getData(this.Type, {}, undefined, this.Path);
				//console.log('fTable', fTable,  this.Engine.getCache(fPath, undefined, this.Type));

				switch(iData.Action||"") {
					case "detail":
						fResult = this.Engine.getData(this.Type, fTable, iData.Row, fPath);
					break;
					case "insert": case "update": case "save": case "delete":
						if(iData.Row.members_file){
							this.setFile(iData.Row);
							delete(iData.Row.members_file);
						}

						fTable = this.Engine.setData(this.Type, fTable, iData.Row, iData.Action, fPath);
						fResult = this.Engine.setFilter(fTable, this.Type, iData.Trace.session);
					break;
					default:
						fResult = this.Engine.setFilter(fTable, this.Type, iData.Trace.session);
					break;
				}
				if(this.UseCache){
					this.Engine.setCache(this.Path, fTable, this.Type);
				}
			break;
			case "Member-Search":
				var self = this;
				fCallbackExec = false;
				fPath = fPath+this.Type+"/";
				var exec = require('child_process').exec;
				var execute = function(command, callback){
						exec(command, function(error, stdout, stderr){ callback(stdout); });
				};
				var fCmd = 'grep -ri -l "'+iData.Row.Find+'" '+fPath;
				/**/ this.Engine.echoTest('Searching Member', fCmd, 5); /**/
				execute(fCmd, function(oData){
					var fResult = [];
					var array = oData.toString().split("\n");
					for(i in array) {
						if((fFile = array[i])&&(fFile)){
							var fRows = self.Engine.getFileJson(fFile, []);
							for(j in fRows) {
								if(fRows[j]&&fRows[j].firstname&&JSON.stringify(fRows[j]).toString().toLowerCase().indexOf(iData.Row.Find.trim().toLowerCase())!=-1){
									fResult.push(fRows[j]);
								}
							}
						}
					}
					console.log(fResult);
					if(iCallback&&fResult) {
						iCallback(undefined, iData, fResult);
					}
				});
				//console.log('member', this.Cache.Table, iData.Row, iData.Action);
			break;
		}

		if (iCallback&&fCallbackExec&&fResult) {
			iCallback(undefined, iData, fResult);
		}
	}
}