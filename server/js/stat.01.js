const request = require('request');
const csv=require('csvtojson');

module.exports = {
	Utils: null,
	UseCache: false,
	TableName: "stat",
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Service.Stat.Cache', this.UseCache);
	},
	formatColor: function(iValue, iColor){
    if(iColor){
      return iColor;
    }
    if( iValue){
      var hash = 0;

      for (var i = 0; i < iValue.length; i++) {
        hash = iValue.charCodeAt(i) + ((hash << 5) - hash);
      }

      return (hash & 0x00FFFFFF).toString(16).toUpperCase();
    }

    return "#00000";
	},
	Sync: function (iData, iCallback) {
		var fResult;
		var fPath = this.Utils.getStorage(this.TableName, iData);
		var fCallbackExec = true;
		/**/ this.Utils.echoTest('Stat.Sync', iData, 1); /**/
		switch (iData.Name) {
			case "Stat-Init":
				if(this.UseCache){
					fResult = this.Utils.getData(this.TableName, {});
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
			break;
			case "Stat-List":
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);

				switch((iData.Action||"")){
					case "detail":
						fResult = this.Utils.getData(this.TableName, fTable, iData.Row, fPath);
					break;
					case "delete":
					case "save":
						fTable= this.Utils.setData(this.TableName, fTable, iData.Row, iData.Action, fPath);
						this.Utils.setEvent({ Name:  iData.Name });
						fResult = this.Utils.getClone(fTable);
					break;
					default:
						fResult = this.Utils.getClone(fTable);
					break;
				}

				if(this.UseCache){
					this.Utils.setCache(fPath, fResult, this.TableName);
				}
			break;
			case "Stat-Index":
				var fTable = {};
				if(this.UseCache){
					fTable = this.Utils.getCache(fPath, this.TableName);
				}else{
					fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);
				}
				var fTable = this.Utils.getData(this.TableName, {}, undefined, fPath);

				fResult = this.Utils.getClone(fTable);
			break;
			case "Stat-Pull":
				if(!iData.Row.feed_url){
					break;
				}
				fFeedUrl = iData.Row.feed_url;
				fCallbackExec = false;
				var self = this;
				request(fFeedUrl, { }, (err, res, body) => {
					if (err) { return console.log(err); }
					csv({
						noheader:true,
						output: "csv"
					})
					.fromString(body)
					.then((csvRow)=>{ 
						/**/ self.Utils.echoTest('Stat.Pull.Result', csvRow, 8); /**/ 
						fResult = {
							Status: true,
							Date: new Date(),
							Data: csvRow
						};
						iCallback(undefined, iData, fResult);
					});
				});
			break;
			case "Stat-Data":
				fFeedUrl = iData.Row.feed_url;
				fCallbackExec = false;
				var self = this;
				request(fFeedUrl, { }, (err, res, body) => {
					if (err) { 
						// error response
						return console.log(err); 
					}

					csv({
						noheader:true,
						output: "csv"
					})
					.fromString(body)
					.then((csvRow)=>{ 
						/**/ self.Utils.echoTest('Stat.Data.Result', csvRow, 8); /**/ 

						var fData = csvRow;

						if(fData&&fData[0]){
							var fIndexCaption = 0;
							var fIndexValue = 1;
							var fIndexColor;

							if( fStat = this.Cache.Table[iData.Row.guid]){
								for(c=0; c<fData[0].length; c++){
									if(fData[0][c]==fStat.feed_col1){
										fIndexCaption = c;
									}
									if(fData[0][c]==fStat.feed_col2){
										fIndexValue = c;
									}
									if(fData[0][c]=='Color'){
										fIndexColor = c;
									}
								}
							}
							var fStatData = [];
							for(r=1; r<fData.length; r++){
								var fRowValue = parseFloat(fData[r][fIndexValue]);
								var fRowCaption = fData[r][fIndexCaption];
								var fRowColor = "#"+self.formatColor(fRowCaption);
								if(fIndexColor&&fData[r][fIndexColor]){
									fRowColor =  fData[r][fIndexColor];
								}
								fStatData.push({ Title: fRowCaption, Value: fRowValue, Color: fRowColor });
							}
						};
						fResult = {
							Pull: {
								Status: true,
								Date: new Date(),
								Data: this.Utils.getClone(fStatData),
							}
						};
						iCallback(undefined, iData, fResult);

						if( fStat = this.Cache.Table[iData.Row.guid]){
							fStat.Pull = this.Utils.getClone(fResult.Pull);
							this.Cache.Table = this.Utils.setData(this.TableName, this.Cache.Table, fStat, 'save');
						}
					});
				});
			break;
		}
		if (iCallback&&fCallbackExec) {
			iCallback(undefined, iData, fResult);
		}
	}
}