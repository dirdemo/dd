module.exports = {
	Utils: null,
	UseCache: false,
	Cache: {
		Index: [],
		Table: {}
	},
	Votes: {},
	setUtils: function (iUtils) {
		if (!this.Utils) {
			this.Utils = iUtils;
		}
		this.UseCache = this.Utils.getConfig('Service.Vote.Cache', this.UseCache);
		if (this.Utils.Config.Service.Vote) {

		} else {
			console.log('Error no vote config in', this.Utils.ConfigFile);
		}
	},
	VoteUser: function(iVote, iUser, iOption){
		var fVote = iVote;
		var fUser = iUser;
		var fVoteFile = this.Utils.getPath(this.Utils.Config.Service.Vote.Path, fVote.guid);
		var fUserVote = this.Utils.getPath(this.Utils.Config.Service.Vote.Path, fVote.guid, fUser.UID);
		var fResult = { State: 'error', Code: "0" };
		var fStatFile =  this.Utils.getPath(this.Utils.Config.Service.Vote.Path, fVote.guid + "-stat");


		if(typeof(this.Votes[fVote.guid])=="undefined"){
			if((fStatInfo = this.Utils.getFileJson(fStatFile, undefined))!=undefined){
				this.Votes[fVote.guid] = fStatInfo;
			}else{
				this.Votes[fVote.guid] = {
					exit: 0, count: 0, options: {}, created: new Date(), rev: 1, date: undefined, guid: fVote.guid
				};

				for(var fIndex in iVote.options){
					this.Votes[fVote.guid].options[iVote.options[fIndex].Title] = { Votes: 0, Points: 0.00 };
				}
			}
		}

		if(this.Utils.existFile(fUserVote)){
			fResult = { State: 'exists', Code: "100", Info: this.Utils.getFileJson(fUserVote, {}) };
		}else{
			fResult = { State: 'canvote', Code: "200" };
	
			if(iOption){
				this.Votes[fVote.guid].count = this.Votes[fVote.guid].count + 1;
				if(!this.Votes[fVote.guid].options[iOption]){
					this.Votes[fVote.guid].options[iOption] = { Votes: 0, Points: 0.00 };
				}
				this.Votes[fVote.guid].options[iOption].Votes = this.Votes[fVote.guid].options[iOption].Votes + 1;
				this.Votes[fVote.guid].options[iOption].Points = this.Votes[fVote.guid].options[iOption].Points + fUser.Points;
				this.Votes[fVote.guid].last = new Date();
				this.Utils.setFileJson(fStatFile, this.Votes[fVote.guid]);
				this.Utils.setFileJson(fUserVote, { User: fUser, Vote: fVote, Option: iOption, created: new Date() });

				fResult = { State: 'voted', Code: "300" };
				this.Utils.Event('Vote.Stat', 	this.Votes[fVote.guid], { UserId: "Event" });
			}
		}

		return fResult;
	},
	Sync: function (iData, iCallback) {
		var fResult;
		/**/ this.Utils.echoTest('Vote.Sync', iData, 5); /**/
		switch (iData.Name) {
			case "Vote-Init":
				this.Votes = {};
				this.Cache.Index = [];
				this.Cache.Table = this.Utils.getData('vote', {});
				fResult = this.Cache.Index;
				break;
			case "Vote-Auth-List":
					var fAuths ={};
					if(fAuth = this.Utils.Config.Auth){
						for(var fKey in fAuth){
							if(fAuth[fKey]&&fAuth[fKey].Status&&fAuth[fKey]&&fAuth[fKey].Status==true){
								fAuths[fKey] = {
									Icon:  fAuth[fKey].Icon,
									Type: fAuth[fKey].Type,
									Title: fAuth[fKey].Title,
									Description: fAuth[fKey].Description,
									Points: fAuth[fKey].Points
								}
							}
						}
					}
					fResult = fAuths;
				break;
				case "Vote-Stats":
					var fStat = {};
					if(fStatTmp = this.Votes[iData.Row.guid]){
						fStat = fStatTmp;
						delete(fStatTmp);
					}else{
						var fStatFile =  this.Utils.getPath(this.Utils.Config.Service.Vote.Path, iData.Row.guid + "-stat");
						if(fStatTmp = this.Utils.getFileJson(fStatFile, undefined)){
							fStat = fStatTmp;
						}else{
							if(fStatTmp = this.Cache.Table[iData.Row.guid]){
								fStat = fStatTmp;
							}else{
								var fStatFile =  this.Utils.Config.Service.Vote.Path + iData.Row.guid + ".db";
								if(fStatTmp = this.Utils.getFileJson(fStatFile, undefined)){
									fStat = fStatTmp;
								}else{
									this.Votes[iData.Row.guid] = {
										exit: 0, count: 0, options: {}, created: new Date(), rev: 1, date: undefined, guid: fVote.guid
									};
					
									for(var fIndex in fStatTmp.options){
										this.Votes[fiData.Row.guid].options[fStatTmp.options[fIndex].Title] = { Votes: 0, Points: 0.00 };
									}
								}
							}
						}
					}

					fResult = fStat;
				break;
				case "Vote-Check":
				var fVoteUser = this.VoteUser(iData.Row.vote, iData.Row.user);

				fResult = fVoteUser;
				break;
			case "Vote-Done":
				var fVoteUser = this.VoteUser(iData.Row.vote, iData.Row.user, iData.Row.option);
				fResult = fVoteUser;
				break;
			case "Vote-Index":
				fResult = this.Utils.getClone(this.Cache.Table);
				break;
			case "Vote-Sources":
				fResult = this.Utils.getSources();
				break;
			case "Vote-List":
					switch(iData.Action) {
						case "detail":
						fResult = this.Utils.getData('vote', this.Cache.Table, iData.Row);
						break;
						case "insert":
						case "update":
						case "save":
						case "delete":
						this.Cache.Table = this.Utils.setData('vote', this.Cache.Table, iData.Row, iData.Action);
						fResult = this.Utils.getClone(this.Cache.Table);
						break;
						default:
						fResult = this.Utils.getClone(this.Cache.Table);
						break;
					}
				break;
		}
		if (iCallback) {
			iCallback(undefined, iData, fResult);
		}
	}
}