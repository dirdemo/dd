(function(document, window, $) {
  'use strict';
  $(document).ready(function() {
    var Site;
    if(!Site){
      Site = window.Site
    }
    Site.run();
  });
})(document, window, jQuery);
// window.onbeforeunload = function() {
//   if(($("#DiscShow").data('bs.modal') || {}).isShown)     // Bootstrap <= 3
//   {
//    console.log('TREEEEEEEEEEEEEEEEEEEEEEEE');
//   }
//   return "Are you sure you want to navigate away?";
// }
function loadBG(iElement, iImage){
  fImage = "bg-01.jpg";
  if(iImage){
    fImage = iImage;
  }else{
    var fNmb = Math.floor(Math.random() * 20) + 1;
    if(fNmb<10) fNmb = "0"+fNmb;
    fImage = "bg-"+fNmb+".jpg";
  }

  var fImage ="url('/res/img/bg/"+fImage+"')";
  $(iElement).css("background-image", fImage);
  $(iElement).css("background-repeat", "no-repeat");
  $(iElement).css("background-position", "0 0");
  $(iElement).css("background-size", "cover");
}

var GlobalGoogleProfile= {};
function onSignInGoogle(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

  var fData = { id:profile.getId(), name:profile.getName(), img:profile.getImageUrl(), email:profile.getEmail() };
  $('#GoogleData').html(JSON.stringify(fData));
  var profile = googleUser.getBasicProfile();
  GlobalGoogleProfile = {
    id: profile.getId(),
    name: profile.getName(),
    firstname: profile.getGivenName(),
    familyname: profile.getFamilyName(),
    email: profile.getEmail(),
    image: profile.getImageUrl()
  };
  $('#inputName').val(profile.getName());
  $('#inputEmail').val(profile.getEmail());

  $('#GoogleSignin').hide();
  $('#GoogleUser').show();
  $('#GoogleId').html(profile.getId());
  $('#GoogleName').html(profile.getName());
  $('#GooglePicture').html('<img class="img-responsive img-circle" src="'+profile.getImageUrl()+'">');
  $('#GoogleEmail').html(profile.getEmail());
}

 /*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

(function(window, document, $) {
  'use strict';

  var $doc = $(document);

  // Site
  // ====
  $.site = $.site || {};

  $.extend($.site, {
    _queue: {
      prepare: [],
      run: [],
      complete: []
    },

    run: function() {
      var self = this;

      this.dequeue('prepare', function() {
        self.trigger('before.run', self);
      });

      this.dequeue('run', function() {
        self.dequeue('complete', function() {
          self.trigger('after.run', self);
        });
      });
    },

    dequeue: function(name, done) {
      var self = this,
        queue = this.getQueue(name),
        fn = queue.shift(),
        next = function() {
          self.dequeue(name, done);
        };

      if (fn) {
        fn.call(this, next);
      } else if ($.isFunction(done)) {
        done.call(this);
      }
    },

    getQueue: function(name) {
      if (!$.isArray(this._queue[name])) {
        this._queue[name] = [];
      }

      return this._queue[name];
    },

    extend: function(obj) {
      $.each(this._queue, function(name, queue) {
        if ($.isFunction(obj[name])) {
          queue.push(obj[name]);

          delete obj[name];
        }
      });

      $.extend(this, obj);

      return this;
    },

    trigger: function(name, data, $el) {
      if (typeof name === 'undefined') return;
      if (typeof $el === 'undefined') $el = $doc;

      $el.trigger(name + '.site', data);
    },

    throttle: function(func, wait) {
      var _now = Date.now || function() {
        return new Date().getTime();
      };
      var context, args, result;
      var timeout = null;
      var previous = 0;

      var later = function() {
        previous = _now();
        timeout = null;
        result = func.apply(context, args);
        context = args = null;
      };

      return function() {
        var now = _now();
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0) {
          clearTimeout(timeout);
          timeout = null;
          previous = now;
          result = func.apply(context, args);
          context = args = null;
        } else if (!timeout) {
          timeout = setTimeout(later, remaining);
        }
        return result;
      };
    },

    resize: function() {
      if (document.createEvent) {
        var ev = document.createEvent('Event');
        ev.initEvent('resize', true, true);
        window.dispatchEvent(ev);
      } else {
        element = document.documentElement;
        var event = document.createEventObject();
        element.fireEvent("onresize", event);
      }
    }
  });

  // Configs
  // =======
  $.configs = $.configs || {};

  $.extend($.configs, {
    data: {},
    get: function(name) {
      var callback = function(data, name) {
        return data[name];
      }

      var data = this.data;

      for (var i = 0; i < arguments.length; i++) {
        name = arguments[i];

        data = callback(data, name);
      }

      return data;
    },

    set: function(name, value) {
      this.data[name] = value;
    },

    extend: function(name, options) {
      var value = this.get(name);
      return $.extend(true, value, options);
    }
  });

  // Colors
  // ======
  $.colors = function(name, level) {
    if (name === 'primary') {
      name = $.configs.get('site', 'primaryColor');
      if (!name) {
        name = 'red';
      }
    }

    if (typeof $.configs.colors === 'undefined') {
      return null;
    }

    if (typeof $.configs.colors[name] !== 'undefined') {
      if (level && typeof $.configs.colors[name][level] !== 'undefined') {
        return $.configs.colors[name][level];
      }

      if (typeof level === 'undefined') {
        return $.configs.colors[name];
      }
    }

    return null;
  };

  // Components
  // ==========
  $.components = $.components || {};

  $.extend($.components, {
    _components: {},

    register: function(name, obj) {
      this._components[name] = obj;
    },

    init: function(name, context, args) {
      var self = this;

      if (typeof name === 'undefined') {
        $.each(this._components, function(name) {
          self.init(name);
        });
      } else {
        context = context || document;
        args = args || [];

        var obj = this.get(name);

        if (obj) {
          switch (obj.mode) {
            case 'default':
              return this._initDefault(name, context);
            case 'init':
              return this._initComponent(name, obj, context, args);
            case 'api':
              return this._initApi(name, obj, args);
            default:
              this._initApi(name, obj, context, args);
              this._initComponent(name, obj, context, args);
              return;
          }
        }
      }
    },

    /* init alternative, but only or init mode */
    call: function(name, context) {
      var args = Array.prototype.slice.call(arguments, 2);
      var obj = this.get(name);

      context = context || document;

      return this._initComponent(name, obj, context, args);
    },

    _initDefault: function(name, context) {
      if (!$.fn[name]) return;

      var defaults = this.getDefaults(name);

      $('[data-plugin=' + name + ']', context).each(function() {
        var $this = $(this),
          options = $.extend(true, {}, defaults, $this.data());

        $this[name](options);
      });
    },


    _initComponent: function(name, obj, context, args) {
      if ($.isFunction(obj.init)) {
        obj.init.apply(obj, [context].concat(args));
      }
    },

    _initApi: function(name, obj, args) {
      if (typeof obj.apiCalled === 'undefined' && $.isFunction(obj.api)) {
        obj.api.apply(obj, args);

        obj.apiCalled = true;
      }
    },


    getDefaults: function(name) {
      var component = this.get(name);

      if (component && typeof component.defaults !== "undefined") {
        return component.defaults;
      } else {
        return {};
      }
    },

    get: function(name, property) {
      if (typeof this._components[name] !== "undefined") {
        if (typeof property !== "undefined") {
          return this._components[name][property];
        } else {
          return this._components[name];
        }
      } else {
        if(typeof(component)!=="undefined"){
          console.warn('component:' + component + ' script is not loaded.');
        }else{
          console.warn('component:unknown script is not loaded.');
        }

        return undefined;
      }
    }
  });

})(window, document, jQuery);
/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';

  var $body = $(document.body);

  // configs setup
  // =============
  $.configs.set('site', {
    fontFamily: "Noto Sans, sans-serif",
    primaryColor: "indigo",
    assets: "../assets"
  });

  window.Site = $.site.extend({
    run: function(next) {
      // polyfill
      this.polyfillIEWidth();

      // Menubar setup
      // =============
      if (typeof $.site.menu !== 'undefined') {
        $.site.menu.init();
      }

      if (typeof $.site.menubar !== 'undefined') {
        $(".site-menubar").on('changing.site.menubar', function() {
          $('[data-toggle="menubar"]').each(function() {
            var $this = $(this);
            var $hamburger = $(this).find('.hamburger');

            function toggle($el) {
              $el.toggleClass('hided', !$.site.menubar.opened);
              $el.toggleClass('unfolded', !$.site.menubar.folded);
            }
            if ($hamburger.length > 0) {
              toggle($hamburger);
            } else {
              toggle($this);
            }
          });

          $.site.menu.refresh();
        });

        $(document).on('click', '[data-toggle="collapse"]', function(e) {
          var $trigger = $(e.target);
          if (!$trigger.is('[data-toggle="collapse"]')) {
            $trigger = $trigger.parents('[data-toggle="collapse"]');
          }
          var href;
          var target = $trigger.attr('data-target') || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');
          var $target = $(target);
          if ($target.hasClass('navbar-search-overlap')) {
            $target.find('input').focus();

            e.preventDefault();
          } else if ($target.attr('id') === 'site-navbar-collapse') {
            var isOpen = !$trigger.hasClass('collapsed');
            $body.addClass('site-navbar-collapsing');

            $body.toggleClass('site-navbar-collapse-show', isOpen);

            setTimeout(function() {
              $body.removeClass('site-navbar-collapsing');
            }, 350);

            if (isOpen) {
              $.site.menubar.scrollable.update();
            }
          }
        });

        $(document).on('click', '[data-toggle="menubar"]', function() {
          $.site.menubar.toggle();

          return false;
        });

        $.site.menubar.init();

        Breakpoints.on('change', function() {
          $.site.menubar.change();
        });
      }

      // Gridmenu setup
      // ==============
      if (typeof $.site.gridmenu !== 'undefined') {
        $.site.gridmenu.init();
      }

      // Sidebar setup
      // =============
      if (typeof $.site.sidebar !== 'undefined') {
        $.site.sidebar.init();
      }

      // Tooltip setup
      // =============
      $(document).tooltip({
        selector: '[data-tooltip=true]',
        container: 'body'
      });

      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();

      // Fullscreen
      // ==========
      if (typeof screenfull !== 'undefined') {
        $(document).on('click', '[data-toggle="fullscreen"]', function() {
          if (screenfull.enabled) {
            screenfull.toggle();
          }

          return false;
        });

        if (screenfull.enabled) {
          document.addEventListener(screenfull.raw.fullscreenchange, function() {
            $('[data-toggle="fullscreen"]').toggleClass('active', screenfull.isFullscreen);
          });
        }
      }

      // Dropdown menu setup
      // ===================
      $body.on('click', '.dropdown-menu-media', function(e) {
        e.stopPropagation();
      });


      // Page Animate setup
      // ==================
      if (typeof $.animsition !== 'undefined') {
        this.loadAnimate(function() {
          $('.animsition').css({
            "animation-duration": '0s'
          });
          next();
        });
      } else {
        next();
      }

      // Mega navbar setup
      // =================
      $(document).on('click', '.navbar-mega .dropdown-menu', function(e) {
        e.stopPropagation();
      });

      $(document).on('show.bs.dropdown', function(e) {
        var $target = $(e.target);
        var $trigger = e.relatedTarget ? $(e.relatedTarget) : $target.children('[data-toggle="dropdown"]');

        var animation = $trigger.data('animation');
        if (animation) {
          var $menu = $target.children('.dropdown-menu');
          $menu.addClass('animation-' + animation);

          $menu.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $menu.removeClass('animation-' + animation);
          });
        }
      });

      $(document).on('shown.bs.dropdown', function(e) {
        var $target = $(e.target);
        var $menu = $target.find('.dropdown-menu-media > .list-group');

        if ($menu.length > 0) {
          var api = $menu.data('asScrollable');
          if (api) {
            api.update();
          } else {
            var defaults = $.components.getDefaults("scrollable");
            $menu.asScrollable(defaults);
            try{
              $menu.asScrollable(defaults);
            }catch(err){
              console.log('Error', err);
            }
          }
        }
      });

      // Page Aside Scrollable
      // =====================

      var pageAsideScroll = $('[data-plugin="pageAsideScroll"]');

      if (pageAsideScroll.length > 0) {
        pageAsideScroll.asScrollable({
          namespace: "scrollable",
          contentSelector: "> [data-role='content']",
          containerSelector: "> [data-role='container']"
        });

        var pageAside = $(".page-aside");
        var scrollable = pageAsideScroll.data('asScrollable');

        if (scrollable) {
          if ($body.is('.page-aside-fixed') || $body.is('.page-aside-scroll')) {
            $(".page-aside").on("transitionend", function() {
              scrollable.update();
            });
          }

          Breakpoints.on('change', function() {
            var current = Breakpoints.current().name;

            if (!$body.is('.page-aside-fixed') && !$body.is('.page-aside-scroll')) {
              if (current === 'xs') {
                scrollable.enable();
                pageAside.on("transitionend", function() {
                  scrollable.update();
                });
              } else {
                pageAside.off("transitionend");
                scrollable.disable();
              }
            }
          });

          $(document).on('click.pageAsideScroll', '.page-aside-switch', function() {
            var isOpen = pageAside.hasClass('open');

            if (isOpen) {
              pageAside.removeClass('open');
            } else {
              scrollable.update();
              pageAside.addClass('open');
            }
          });

          $(document).on('click.pageAsideScroll', '[data-toggle="collapse"]', function(e) {
            var $trigger = $(e.target);
            if (!$trigger.is('[data-toggle="collapse"]')) {
              $trigger = $trigger.parents('[data-toggle="collapse"]');
            }
            var href;
            var target = $trigger.attr('data-target') || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');
            var $target = $(target);

            if ($target.attr('id') === 'site-navbar-collapse') {
              scrollable.update();
            }
          });
        }
      }

      // Page Actions Waves
      // =========================
      if (typeof Waves !== 'undefined') {
        Waves.init();
        Waves.attach('.site-menu-item > a', ['waves-classic']);
        Waves.attach(".site-navbar .navbar-toolbar [data-toggle='menubar']", ["waves-light", "waves-round"]);
        Waves.attach(".page-header-actions .btn:not(.btn-inverse)", ["waves-light", "waves-round"]);
        Waves.attach(".page-header-actions .btn-inverse", ["waves-classic", "waves-round"]);
        Waves.attach('.page > div:not(.page-header) .btn:not(.ladda-button):not(.btn-round):not(.btn-pure):not(.btn-floating):not(.btn-flat)', ['waves-light']);
        Waves.attach('.page > div:not(.page-header) .btn-pure:not(.ladda-button):not(.btn-round):not(.btn-floating):not(.btn-flat):not(.icon)', ['waves-classic']);
      }

      // Init Loaded Components
      // ======================
      $.components.init();

      this.startTour();
    },

    polyfillIEWidth: function() {
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
          document.createTextNode(
            '@-ms-viewport{width:auto!important}'
          )
        );
        document.querySelector('head').appendChild(msViewportStyle);
      }
    },

    loadAnimate: function(callback) {
      return $.components.call("animsition", document, callback);
    },

    startTour: function(flag) {
      if (typeof this.tour === 'undefined') {
        if (typeof introJs === 'undefined') {
          return;
        }

        var tourOptions = $.configs.get('tour'),
          self = this;
        flag = $('body').css('overflow');
        this.tour = introJs();

        this.tour.onbeforechange(function() {
          $('body').css('overflow', 'hidden');
        });

        this.tour.oncomplete(function() {
          $('body').css('overflow', flag);
        });

        this.tour.onexit(function() {
          $('body').css('overflow', flag);
        });

        this.tour.setOptions(tourOptions);
        $('.site-tour-trigger').on('click', function() {
          self.tour.start();
        });
      }
      // if (window.localStorage && window.localStorage.getItem('startTour') && (flag !== true)) {
      //   return;
      // } else {
      //   this.tour.start();
      //   window.localStorage.setItem('startTour', true);
      // }
    }
  });

})(window, document, jQuery);
