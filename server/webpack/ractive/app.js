io = require('socket.io-client');
jquery = require('jquery');
toastr = require('toastr');
bootstrap = require('bootstrap');
dropify = require('dropify');
chart = require('chart.js');
Ractive = require('ractive');
selectize = require('selectize');
Format = require('./format.js');
Language = require('./language.js');
Client = require('./client.js');
NoSleep = require('nosleep.js');
Global = Client.getGlobal();
SiteLng = Language.getDefault();

// Config
Ractive.DEBUG = ((Global) && (Global.Behavior) && (Global.Behavior.echoTest)) ? Global.Behavior.echoTest : false;
Ractive.adaptors.Ractive = require('ractive-ractive');
Ractive.defaults.adapt = ['Ractive'];
Ractive.defaults.isolated = false;
Ractive.defaults.lazy = false;
// Functions
Ractive.defaults.data.format = Format.format;
Ractive.defaults.data.session = Global.Session;
// Procedures
Ractive.defaults.sync = Client.Sync;
Ractive.defaults.isEcho = Client.isEcho;
Ractive.defaults.setDisplay = Client.setDisplay;
// Displays
Ractive.components['Element-Input'] = require('ractive-component!./components/Input.html');
Ractive.components['Element-Chart'] = require('ractive-component!./components/Chart.html');
Ractive.components['Element-Card'] = require('ractive-component!./components/Card.html');
Ractive.components['Element-Header'] = require('ractive-component!./components/Header.html');
Ractive.components['Element-Auth'] = require('ractive-component!./components/Auth.html');
Ractive.components['Element-Discusion'] = require('ractive-component!./components/Discusion.html');
Ractive.components['Element-Share'] = require('ractive-component!./components/Share.html');
Ractive.components['Element-Parlament'] = require('ractive-component!./components/Parlament.html');
Ractive.components['Element-Cmd'] = require('ractive-component!./components/Cmd.html');
Ractive.components['Element-Chat'] = require('ractive-component!./components/Chat.html');
Ractive.components['Element-Info'] = require('ractive-component!./components/Info.html');
Ractive.components['Element-Vote'] = require('ractive-component!./components/Vote.html');
Ractive.components['Element-Paging'] = require('ractive-component!./components/Paging.html');
Ractive.components['Element-Vote-Chart'] = require('ractive-component!./components/Vote-Chart.html');
Ractive.components['Element-Login'] = require('ractive-component!./components/Login.html');
Ractive.components['Element-Crumbs'] = require('ractive-component!./components/Crumbs.html');
Ractive.components['Element-Dialog'] = require('ractive-component!./components/Dialog.html');
Ractive.components['Element-List'] = require('ractive-component!./components/List.html');

Ractive.components['404'] = require('ractive-component!./pages/Page-404.html');
Ractive.components['Session-Menu'] = require('ractive-component!./pages/Session-Menu.html');
Ractive.components['Session-Home'] = require('ractive-component!./pages/Session-Home.html');
Ractive.components['Sources-Home'] = require('ractive-component!./pages/Sources-Home.html');
Ractive.components['Config-Home'] = require('ractive-component!./pages/Config-Home.html');

Ractive.components['Disc-Home'] = require('ractive-component!./pages/Disc-Home.html');
Ractive.components['Disc-Widget'] = require('ractive-component!./pages/Disc-Widget.html');

Ractive.components['News-Home'] = require('ractive-component!./pages/News-Home2.html');
Ractive.components['News-Widget'] = require('ractive-component!./pages/News-Widget.html');

Ractive.components['Live-Home'] = require('ractive-component!./pages/Live-Home.html');

Ractive.components['Vote-Home'] = require('ractive-component!./pages/Vote-Home.html');
Ractive.components['Vote-Widget'] = require('ractive-component!./pages/Vote-Widget.html');

Ractive.components['Stat-Home'] = require('ractive-component!./pages/Stat-Home.html');
Ractive.components['Stat-Widget'] = require('ractive-component!./pages/Stat-Widget.html');

Ractive.components['Tender-Home'] = require('ractive-component!./pages/Tender-Home.html');
Ractive.components['Tender-Widget'] = require('ractive-component!./pages/Tender-Widget.html');

Ractive.components['Circle-Home'] = require('ractive-component!./pages/Circle-Home.html');
Ractive.components['Member-Home'] = require('ractive-component!./pages/Member-Home.html');

Ractive.components['History-Home'] = require('ractive-component!./pages/History-Home.html');

Ractive.components['Election-Home'] = require('ractive-component!./pages/Election-Home.html');
Ractive.components['Election-Widget'] = require('ractive-component!./pages/Election-Widget.html');

Ractive.components['Air-Home'] = require('ractive-component!./pages/Air-Home.html');


Ractive.defaults.sync.UrlChange();
$(function () {
  $(".dd-frame").each(function () {
    var fFrame = $(this).data("frame");
    var fId = $(this).attr('id');
    if((Global) && (Global.Behavior) && (Global.Behavior.echoTest)){
      console.log('Frame', fFrame, fId);
    }
    $(this).html("Found this one (" + fId + ")");
    Ractive.defaults.setDisplay({ Frame: fFrame, Id: fId }, undefined, function () {
      //Ractive.defaults.sync.Url(window.location.hash);
    });
  });
});