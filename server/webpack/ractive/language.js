var SiteLng = {};

DefLng = {
  Date: {
    Month: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
    Day: ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "&#268;etvrtak", "Petak", "Subota"]
  },
  Dialog: {
    Edit: {
      Text: "izmeni",
      Class: "btn btn-sm btn-outline-primary",
    },
    Delete: {
      Text: "<span><i class='fa fa-trash'></i>&nbsp;Izbri&#353;i</span>",
      Class: "btn btn-sm btn-danger dd-action"
    },
    DeleteShow: {
      Text: "<span><i class='fa fa-trash'></i>&nbsp;Izbri&#353;i</span>",
      Class: "pull-left btn btn-sm btn-outline-danger dd-action"
    },
    Went: {
      Text: "izašlo",
      Class: "btn btn-sm btn-primary btn-animate btn-animate-side",
    },
    Save: {
      Text: "<span><i class='fa fa-check'></i>&nbsp;Snimi</span>",
      Class: "btn btn-sm btn-success dd-action"
    },
    Restore: {
      Text: "<span><i class='fa fa-undo'></i>&nbsp;vrati</span>",
      Class: "btn btn-sm btn-success"
    },
    Cancel: {
      Text: "<i class='fa fa-times'></i>&nbsp;poništi",
      Class: "btn btn-sm btn-default"
    },
    CancelSB: {
      Text: "<i class='fa fa-times'></i>&nbsp;zatvori",
      Class: "btn btn-xs btn-block btn-default"
    },
    Refresh: {
      Text: "osvezi",
      Class: "btn btn-sm btn-primary dd-action",
    },
    Add: "<i class='fa fa-plus-circle'></i>&nbsp;Add",
    Back: "<span><i class='icon md-arrow-left' aria-hidden='true'></i>&nbsp;Back</span>",
    NewRow: "<span class='badge badge-primary'>novo</span>"
  },
  Wait: {
    Text: "Loading...",
    Small: "<span><i class='fa fa-spinner fa-spin'></i>&nbsp;Loading...</span>",
    Big: "<div class='row vertical-align text-center white'><div class='vertical-align-middle'><br><div class='loader-wrapper loader-lg active'><div class='loader-layer loader-blue-only'><div class='loader-circle-left'><div class='circle'></div></div><div class='loader-circle-gap'></div><div class='loader-circle-right'><div class='circle'></div></div></div></div><br><p>Loading...</p></div></div>",
  },
  Page: {
    404: {
      Title: "Page not found !",
      Code: "CODE 404",
      Advise: "YOU SEEM TO BE TRYING TO FIND HIS WAY HOME"
    },
    MainBS: "col-xs-12 col-sm-9",
    RightBS: "col-xs-12 col-sm-3"
  },
  Button: {
    Auth: {
      Text: "login",
      Class: "btn btn-sm btn-block btn-link btn-default"
    },
    AuthLink: {
      Text: "<span><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp;Prijavite se da bi menjali</span>",
      Class: "btn btn-sm btn-info btn-animate btn-animate-side"
    },
    View: {
      Text: "vidi",
      Class: "btn btn-sm btn-block btn-primary btn-animate btn-animate-side"
    },
    Save: {
      Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Save</span>",
      Class: "btn btn-success btn-animate btn-animate-side"
    },
    Publish: {
      Text: "<span><i class='icon md-mail-send' aria-hidden='true'></i>&nbsp;Publish</span>",
      Class: "btn btn-primary btn-animate btn-animate-side"
    },
    Add: {
      Text: "<span><i class='icon md-plus' aria-hidden='true'></i>&nbsp;Add</span>",
      Class: "btn btn-primary btn-animate btn-animate-side"
    },
    Delete: {
      Text: "<span><i class='icon md-delete' aria-hidden='true'></i>&nbsp;Delete</span>",
      Class: "btn btn-danger btn-animate btn-animate-side"
    },
    Revert: {
      Text: "<span><i class='icon md-delete' aria-hidden='true'></i>&nbsp;Revert</span>"
      , Class: "btn btn-warning btn-animate btn-animate-side"
    },
    Back: {
      Text: "<span><i class='icon md-arrow-left' aria-hidden='true'></i>&nbsp;Back</span>",
      Class: "btn btn-warning btn-animate btn-animate-side"
    },
    Cancel: {
      Text: "<i class='fa fa-times'></i>&nbsp;Cancel",
      Class: "btn btn-default btn-animate btn-animate-side"
    },
    Edit: {
      Text: "<span><i class='fa fa-edit' aria-hidden='true'></i>&nbsp;izmeni</span>",
      Class: "btn btn-primary btn-animate btn-animate-side"
    },
    Send: {
      Text: "<span><i class='icon md-mail-send' aria-hidden='true'></i>&nbsp;Send</span>",
      Class: "btn btn-animate btn-animate-side"
    },
    Refresh: {
      Text: "<i class='fa fa-refresh'></i>&nbsp;",
      Class: "btn btn-primary btn-sm btn-icon btn-round"
    },
    Logout: {
      Text: "<i class='fa fa-sign-out'></i>&nbsp;Logout",
      Class: "btn btn-warning btn-animate btn-animate-side"
    },
    Monitor: {
      Text: "<span><i class='icon fa fa-refresh'></i>&nbsp;Live</span>",
      Class: "btn btn-primary btn-animate btn-animate-side"
    },
    AddUser: {
      Text: "Add user",
      Class: "btn btn-primary btn-animate btn-animate-side"
    },
    Upload: {
      Text: "<i class='icon md-settings' aria-hidden='true'></i>",
      Class: "btn btn-sm btn-icon btn-light btn-round"
    },
    Config: {
      Text: "<i class='icon md-settings' aria-hidden='true'></i>",
      Class: "btn btn-sm btn-icon btn-light btn-round"
    },
    Close: {
      Text: "<i class='fa fa-times'></i>",
      Class: "pull-right btn btn-sm btn-icon btn-warning btn-round"
    },
    Print: {
      Text: "<i class='fa fa-print'></i>&nbsp;Štampa",
      Class: "btn btn-xs btn-primary"
    }
  },
  Site: {
    Title: "Direktna Demokratija",
    ProfilePicture: "/res/img/logo.png",
    Caption: "",
    Description: "2020",
    Year: "2020",
    Social: {
      Twitter: {
        Link: "https://twitter.com/",
        Html: "<i class='icon fa-twitter'></i>"
      },
      Facebook: {
        Link: "https://facebook.com/",
        Html: "<i class='fa fa-x2 fa-facebook'></i>"
      },
      LinkedIn: {
        Link: "https://linkedin.com/",
        Html: "<i class='icon fa-twitter'></i>"
      }
    }
  },
  News: {
    Sections: {
      "news": "Vesti",
      "choise": "Izbor",
      "social": "Dru&#353;tvene Mre&#382;e"
    }
  },
  Live: {
    Title: "Live",
    Color: "green",
    Srv: {},
    title: {
      Text: "Naziv",
      NewValue: "",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    moderators: {
      Type: "emails",
      Text: "Moderatori",
      Placeholder: "unesite moderatore..."
    },
    link: {
      Text: "Link",
      NewValue: "",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    links: {
      Text: "Linkovi",
      NewValue: [],
      Type: "selectize",
      Placeholder: "unesite opis..."
    },
    Dialog: {
      Title: "Live",
      Class: "fade modal-primary in",
    }
  },
  Vote: {
    title: {
      Text: "Naziv",
      NewValue: "",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    vote_count: {
      Text: "Rundi",
      NewValue: 1,
      Type: "number",
      Placeholder: "unesite opis..."
    },
    vote_secret: {
      Text: "Tajno glasanje",
      NewValue: 1,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    vote_live: {
      Text: "rezultati vidljivi",
      NewValue: 1,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    vote_confirm: {
      Text: "Potvrdi izbor",
      NewValue: 1,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    vote_sell: {
      Text: "Prodaj glas",
      NewValue: 0,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    vote_give: {
      Text: "Pokloni glas",
      NewValue: 0,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    points_auth: {
      Text: "Autentifikacija",
      NewValue: true,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    points_social: {
      Text: "Društvo",
      NewValue: false,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    points_education: {
      Text: "Obrazovanje",
      NewValue: false,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    points_activity: {
      Text: "Aktivnost",
      NewValue: false,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    date_fromtime: {
      Text: "Vreme od",
      Type: "time",
      Placeholder: "unesite opis..."
    },
    date_fromdate: {
      Text: "Datum od",
      Type: "date",
      Placeholder: "unesite opis..."
    },
    date_totime: {
      Text: "Vreme do",
      Type: "time",
      Placeholder: "unesite opis..."
    },
    date_todate: {
      Text: "Datum do",
      Type: "date",
      Placeholder: "unesite opis..."
    },
    share_facebook: {
      Text: "Facebook",
      NewValue: false,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    share_twitter: {
      Text: "Twitter",
      NewValue: false,
      Type: "checkbox",
      Placeholder: "unesite opis..."
    },
    State: {
      "exists": {
        "Msg": "Već ste glasali",
        "Btn": "Prijavi neregularnost"
      },
      "canvote": {},
      "voted": {
        "Msg": "Hvala na glasanju",
        "Btn": "zatvori"
      }
    },
    Dialog: {
      Title: "Glasanje",
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-primary btn-animate btn-animate-side",
      }
    }
  },
  VoteReport: {
    message: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis problema...",
      Rows: 5
    },
    Dialog: {
      Title: "Prijava problema",
      Report: {
        Text: "izmeni",
        Class: "btn btn-sm btn-primary",
      }
    }
  },
  Stat: {
    title: {
      Text: "Naziv",
      NewValue: "",
      Type: "text",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    description: {
      Text: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    data_meta: {
      Text: "",
      Placeholder: "unesite meta podatke...",
      Rows: 5
    },
    feed_url: {
      Text: "Izvor",
      Placeholder: "Unesite izvor...",
    },
    feed_kind: {
      Text: "Tip grafikona",
      Placeholder: "Unesite izvor...",
    },
    feed_col1: {
      Text: "Kolona naziv",
      Placeholder: "Unesite izvor...",
    },
    feed_col2: {
      Text: "Vrednost",
      Placeholder: "Unesite izvor...",
    },
    Dialog: {
      Title: "Grafikon",
      Pull: {
        Text: "U&#269;itaj",
        Class: "btn btn-xs btn-primary btn-animate btn-animate-side",
        Placeholder: "unesite opis...",
      },
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-primary btn-animate btn-animate-side",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-primary btn-animate btn-animate-side",
      },
      Refresh: {
        Text: "osve&#382;i",
        Class: "btn btn-sm btn-primary",
      },
      Delete: {
        Text: "<span><i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Izbrisi</span>"
        , Class: "pull-left btn btn-sm btn-danger btn-animate btn-animate-side"
      },
      Save: {
        Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Snimi</span>"
        , Class: "btn btn-sm btn-success btn-animate btn-animate-side"
      },
      Cancel: {
        Text: "<i class='fa fa-times'></i>&nbsp;poništi"
        , Class: "btn btn-sm btn-default btn-animate btn-animate-side"
      },
      Refresh: {
        Text: "osve&#382;i",
        Class: "btn btn-sm btn-primary",
      }
    },
    Button: {
      Pull: {
        Text: "U&#269;itaj",
        Class: "btn btn-xs btn-primary btn-animate btn-animate-side",
        Placeholder: "unesite opis...",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-block btn-primary btn-animate btn-animate-side",
      },
    },
    Types: {
      "detect": {
        Id: "detect",
        Title: "(prepoznaj)",
        Icon: "",
        Description: ""
      },
      "csv": {
        Id: "csv",
        Title: "CSV",
        Icon: "",
        Description: ""
      },
      "json": {
        Id: "json",
        Title: "JSon",
        Icon: "",
        Description: ""
      }
    }
  },
  Source: {
    Title: "Izvori",
    title: {
      Text: "Naziv",
      Class: "font-weight-500"
    },
    description: {
      Type: "area",
      Text: "",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    moderators: {
      Type: "emails",
      Text: "Moderatori",
      Placeholder: "unesite moderatore..."
    },
    source_type: {
      Type: "text",
      Text: "Tip"
    },
    source_icon: {
      Type: "text",
      Text: "Ikona"
    },
    source_link: {
      Type: "text",
      Text: "Link"
    },
    source_section: {
      Type: "text",
      Text: "Sekcija"
    },
    rss_url: {
      Type: "text",
      Text: "RSS"
    },
    rss_item: {
      Type: "text",
      Text: " "
    },
    twitter_url: {
      Type: "text",
      Text: "Twitter"
    },
    twitter_item: {
      Type: "text",
      Text: " "
    },
    source_admin: {
      Type: "text",
      Text: "Amins"
    },
    youtube_url: {
      Type: "text",
      Text: "YT Link"
    },
    SourcesTypes: {
      "news": "Vesti",
      "party": "Stranka",
      "politician": "Politi&#269;ar",
      "people": "Javna li&#269;nost",
      "goverment": "Dr&#382;ava",
      "other": "Drugo"
    },
    Types: {
      "news": {
        Id: "news",
        Title: "Vesti",
        Icon: "",
        Description: ""
      },
      "party": {
        Id: "party",
        Title: "Stranka",
        Icon: "",
        Description: ""
      },
      "politician": {
        Id: "politician",
        Title: "Politi&#269;ar",
        Icon: "",
        Description: ""
      },
      "people": {
        Id: "people",
        Title: "Javna li&#269;nost",
        Icon: "",
        Description: ""
      },
      "goverment": {
        Id: "goverment",
        Title: "Dr&#382;ava",
        Icon: "",
        Description: ""
      },
      "other": {
        Id: "other",
        Title: "Drugo",
        Icon: "",
        Description: ""
      }
    }
  },
  History: {
    title: {
      Text: "Naziv",
      Class: "font-weight-500"
    },
    description: {
      Type: "area",
      Text: "",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    kzm: {
      Type: "text",
      Text: "",
      Placeholder: "unesite kzm link...",
      Rows: 4
    },
    tag: {
      Type: "selectize",
      Text: "Oznake",
      Placeholder: "unesite tag..."
    },
    links: {
      Type: "area",
      Text: "Reference",
      Placeholder: "unesite link...",
      Rows: 5
    },
    date_day: {
      Type: "number",
      Text: "Dan",
      Placeholder: "unesite dan...",
    },
    date_month: {
      Type: "number",
      Text: "Mesec",
      Placeholder: "unesite mesec...",
    },
    date_year: {
      Type: "number",
      Text: "Godina",
      Placeholder: "Unesite godinu...",
    },
    Dialog: {
      Title: "Podatak",
      Class: "fade modal-primary in",
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-primary btn-animate btn-animate-side",
      },
      Delete: {
        Text: "<span><i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Izbrisi</span>"
        , Class: "pull-left btn btn-sm btn-danger btn-animate btn-animate-side"
      },
      Save: {
        Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Snimi</span>"
        , Class: "btn btn-sm btn-success btn-animate btn-animate-side"
      },
      Cancel: {
        Text: "<i class='fa fa-times'></i>&nbsp;poništi"
        , Class: "btn btn-sm btn-default btn-animate btn-animate-side"
      },
      Refresh: {
        Text: "osvezi",
        Class: "btn btn-sm btn-primary",
      }
    },
    Button: {
      Pull: {
        Text: "Ucitaj",
        Class: "btn btn-xs btn-primary btn-animate btn-animate-side",
        Placeholder: "unesite opis...",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-block btn-primary btn-animate btn-animate-side",
      },
      Edit: {
        Text: "<i class='fa fa-edit'></i>&nbsp;promeni",
        Class: "btn btn-xs btn-link btn-primary white",
      }
    },
    Types: {
      "default": {
        Id: "detect",
        Title: "(nedefinisano)",
        Icon: "",
        Description: ""
      },
      "one": {
        Id: "one",
        Title: "Dogadjaj",
        Icon: "",
        Description: ""
      },
      "period": {
        Id: "period",
        Title: "Period",
        Icon: "",
        Description: ""
      }
    }
  },
  Disc: {
    title: {
      Text: "Naziv",
      NewValue: "",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    description: {
      Type: "area",
      Text: "",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    kind: {
      Text: "Vrsta",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(pending)" },
        chat: { guid: "chat", title: "Razgovor" },
        parlament: { guid: "parlament", title: "Parlament" }
      }
    },
    visibility: {
      Text: "Vidljivost",
      Type: "select",
      NewValue: "",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    anonimus: {
      Text: "Dozvoli anonimne komentare",
      Type: "checkbox"
    },
    emails: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    moderators: {
      Type: "emails",
      Text: "Moderatori",
      Placeholder: "unesite moderatore..."
    },
    parcipients: {
      Type: "emails",
      Text: "Učesnici",
      Placeholder: "unesite učesnike..."
    },
    tag: {
      Type: "selectize",
      Text: "Oznake",
      Placeholder: "unesite tag..."
    },
    state: {
      Type: "area",
      Text: "Reference",
      Placeholder: "unesite link...",
      Rows: 5
    },
    parlament_talk: {
      Type: "number",
      Text: "Rec",
      Placeholder: "Trajanje govora..."
    },
    parlament_reply: {
      Type: "number",
      Text: "Replika",
      Placeholder: "Trajanje replike..."
    },
    msg_count: {
      Type: "number",
      Text: "Poruka",
      Placeholder: "Broj poruka..."
    },
    msg_counter: {
      Type: "number",
      Text: "Ukupno poruka",
      Placeholder: "Ukupan broj poruka..."
    },
    Dialog: {
      Title: "Diskusija",
      Class: "fade modal-primary in",
    },
    Button: {
      Show: {
        Text: "Diskusija",
        Class: "btn btn-sm btn-dark",
        Placeholder: "unesite opis...",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-block btn-primary btn-animate btn-animate-side",
      },
      Edit: {
        Text: "<i class='fa fa-edit'></i>&nbsp;promeni",
        Class: "btn btn-xs btn-link btn-primary white",
      },
      Send: {
        Text: "<i class='fa fa-send'></i>&nbsp;send",
        Class: "btn btn-sm btn-block text-uppercase",
      },
      SendSM: {
        Text: "<i class='fa fa-send'></i>",
        Class: "btn ",
      }
    },
    Types: {
      "default": {
        Id: "detect",
        Title: "(nedefinisano)",
        Icon: "",
        Description: ""
      },
      "one": {
        Id: "one",
        Title: "Dogadjaj",
        Icon: "",
        Description: ""
      },
      "period": {
        Id: "period",
        Title: "Period",
        Icon: "",
        Description: ""
      }
    },
    Msg: {
      Registration: {
        Msg: "Tražite pristup grupi.",
        Btn: "Traži"
      },
      ChatEmpty: "Nema diskusuje, započnite je..."
    }
  },
  Member: {
    title: {
      Text: "Naziv",
      NewValue: "",
      Type: "text",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    moderators: {
      Text: "Moderatori",
      NewValue: [],
      Type: "emails"
    },
    members_file: {
      id: "files",
      Text: "Dodaj članove",
      Type: "file",
      Placeholder: "Izaberite csv..."
    },
    members_fileareset: {
      Text: "Izbriši prethodne članove",
      Type: "checkbox"
    },
    member_count: {
      Text: "stavki",
      NewValue: 0,
      Type: "number",
      Placeholder: "unesite opis..."
    },
    star_points: {
      Text: "Vrednost zvezde",
      NewValue: 1,
      Type: "number",
      Placeholder: "unesite opis..."
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    Msg: {
      Search: {
        Wait: "Tražim <b>{0}</b>...",
        Found: "Prona&#273;eno {0} za {1}",
        Typing: "&#268;ekam potvrdu za <b>{0}</b>"
      },
      Acc: {
        Msg: "<i class='fa fa-hand-paper-o'></i>&nbsp;Nemate pristup",
        Btn: "Trazi"
      },
      Login: {
        Msg: "Prijavite se na sistem",
        Btn: "Prijava"
      },
      TitleUser: "&#268;lan"
    },
    Filter: {
      Title: "Filter"
    },
    Dialog: {
      Title: "Grupa",
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-outline-primary",
      },
      Voted: {
        Text: "glasao",
        Class: "btn btn-sm btn-outline-default",
      },
      Abstainer: {
        Text: "apstinent",
        Class: "btn btn-sm btn-outline-default",
      },
      VoteClear: {
        Text: "<i class='fa fa-trash' aria-hidden='true'></i>",
        Class: "btn btn-sm btn-outline-danger",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-primary",
      },
      Delete: {
        Text: "<span><i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Izbrisi</span>"
        , Class: "pull-left btn btn-sm btn-danger btn-animate btn-animate-side"
      },
      Save: {
        Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Snimi</span>"
        , Class: "btn btn-sm btn-success btn-animate btn-animate-side"
      },
      Cancel: {
        Text: "<i class='fa fa-times'></i>&nbsp;poništi"
        , Class: "btn btn-sm btn-default btn-animate btn-animate-side"
      },
      Find: {
        Text: "<i class='fa fa-search'></i>&nbsp;traži",
        Class: "btn btn-primary",
      }
    }
  },
  Circle: {
    Title: "Krugovi",
    title: {
      Text: "Naziv",
      NewValue: "",
      Type: "text",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    moderators: {
      Text: "Moderatori",
      NewValue: [],
      Type: "emails"
    },
    member_count: {
      Text: "stavki",
      NewValue: 0,
      Type: "number",
      Placeholder: "unesite opis..."
    },
    star_points: {
      Text: "Vrednost zvezde",
      NewValue: 1,
      Type: "number",
      Placeholder: "unesite opis..."
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    Msg: {
      Search: {
        Wait: "Tražim <b>{0}</b>...",
        Found: "Prona&#273;eno {0} za {1}",
        Typing: "&#268;ekam potvrdu za <b>{0}</b>"
      },
      Acc: {
        Msg: "<i class='fa fa-hand-paper-o'></i>&nbsp;Nemate pristup",
        Btn: "Trazi"
      },
      Login: {
        Msg: "Prijavite se na sistem",
        Btn: "Prijava"
      },
      TitleUser: "&#268;lan"
    },
    Filter: {
      Title: "Filter"
    },
    Dialog: {
      Title: "Grupa",
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-outline-primary",
      },
      Voted: {
        Text: "glasao",
        Class: "btn btn-sm btn-outline-default",
      },
      Abstainer: {
        Text: "apstinent",
        Class: "btn btn-sm btn-outline-default",
      },
      VoteClear: {
        Text: "<i class='fa fa-trash' aria-hidden='true'></i>",
        Class: "btn btn-sm btn-outline-danger",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-primary",
      },
      Delete: {
        Text: "<span><i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Izbrisi</span>"
        , Class: "pull-left btn btn-sm btn-danger btn-animate btn-animate-side"
      },
      Save: {
        Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Snimi</span>"
        , Class: "btn btn-sm btn-success btn-animate btn-animate-side"
      },
      Cancel: {
        Text: "<i class='fa fa-times'></i>&nbsp;poništi"
        , Class: "btn btn-sm btn-default btn-animate btn-animate-side"
      },
      Find: {
        Text: "<i class='fa fa-search'></i>&nbsp;traži",
        Class: "btn btn-primary",
      }
    }
  },
  Election: {
    title: {
      Text: "Naziv",
      NewValue: "",
      Type: "text",
      RegEx: "^.{2,}$",
      Class: "font-weight-500"
    },
    committee: {
      Text: "Odbor",
      NewValue: "",
      Type: "text",
      RegEx: "",
      Class: ""
    },
    municipal: {
      Text: "Opština",
      NewValue: "",
      Type: "text",
      RegEx: "",
      Class: ""
    },
    city: {
      Text: "Grad",
      NewValue: "",
      Type: "text",
      RegEx: "",
      Class: ""
    },
    address: {
      Text: "Ulica",
      NewValue: "",
      Type: "text",
      RegEx: "",
      Class: ""
    },
    moderators: {
      Text: "Moderatori",
      NewValue: [],
      Type: "emails"
    },
    members_file: {
      id: "files",
      Text: "Dodaj članove",
      Type: "file",
      Placeholder: "Izaberite csv..."
    },
    members_fileareset: {
      Text: "Izbriši prethodne članove",
      Type: "checkbox"
    },
    went_count: {
      Text: "Izašlo",
      NewValue: 0,
      Type: "number"
    },
    went_max: {
      Text: "Birača",
      NewValue: 0,
      Type: "number"
    },
    went_count_secure: {
      Text: "Sigurno",
      NewValue: 0,
      Type: "number"
    },
    went_max_secure: {
      Text: "S.Izašlo",
      NewValue: 0,
      Type: "number"
    },
    went_count_abstinent: {
      Text: "odsutno",
      NewValue: 0,
      Type: "number"
    },
    went_max_abstinent: {
      Text: "O.Ukupno",
      NewValue: 0,
      Type: "number"
    },
    item_count: {
      Text: "Stavki",
      NewValue: 0,
      Type: "number"
    },
    geo_lat: {
      Text: "Lat",
      NewValue: 0,
      Type: "number"
    },
    geo_lng: {
      Text: "Long",
      NewValue: 0,
      Type: "number"
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "...",
      Rows: 2
    },
    visibility: {
      Text: "Vidljivost",
      NewValue: "",
      Type: "select",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(skriveno)" },
        open: { guid: "open", title: "Otvoreno" },
        registration: { guid: "registration", title: "Registracija" },
        close: { guid: "close", title: "Po pozivu" }
      }
    },
    Msg: {
      Search: {
        Wait: "Tražim <b>{0}</b>...",
        Found: "Prona&#273;eno {0} za {1}",
        Typing: "&#268;ekam potvrdu za <b>{0}</b>"
      },
      Acc: {
        Msg: "<i class='fa fa-hand-paper-o'></i>&nbsp;Nemate pristup",
        Btn: "Trazi"
      },
      Login: {
        Msg: "Prijavite se na sistem",
        Btn: "Prijava"
      }
    },
    Filter: {
      Title: "Filter"
    },
    Dialog: {
      Title: "Biračko mesto",
      Edit: {
        Text: "izmeni",
        Class: "btn btn-sm btn-outline-default",
      },
      Voted: {
        Text: "glasao",
        Class: "btn btn-sm btn-outline-default"
      },
      Abstainer: {
        Text: "apstinent",
        Class: "btn btn-sm btn-outline-default",
      },
      VoteClear: {
        Text: "<i class='fa fa-trash' aria-hidden='true'></i>",
        Class: "btn btn-sm btn-outline-danger",
      },
      View: {
        Text: "vidi",
        Class: "btn btn-sm btn-primary",
      },
      Delete: {
        Text: "<span><i class='fa fa-trash' aria-hidden='true'></i>&nbsp;Izbrisi</span>"
        , Class: "pull-left btn btn-sm btn-danger btn-animate btn-animate-side"
      },
      Save: {
        Text: "<span><i class='fa fa-check' aria-hidden='true'></i>&nbsp;Snimi</span>"
        , Class: "btn btn-sm btn-success btn-animate btn-animate-side"
      },
      Cancel: {
        Text: "<i class='fa fa-times'></i>&nbsp;poništi"
        , Class: "btn btn-sm btn-default btn-animate btn-animate-side"
      },
      Find: {
        Text: "<i class='fa fa-search'></i>&nbsp;traži",
        Class: "btn btn-primary",
      },
      Reset: {
        Text: "<i class='fa fa-trash'></i>&nbsp;resetuj statistiku",
        Title: "Upozorenje",
        Class: "btn btn-lg btn-danger text-uppercase dd-action",
        Success: "Resetovano",
        Message: "Ovom akcijom resetuje glasanje na pocetak!"
      },
      Settings: {
        Text: "<i class='fa fa-trash'></i>&nbsp;resetuj statistiku",
        Title: "Upozorenje",
        Class: "btn btn-lg btn-danger text-uppercase dd-action",
        Success: "Resetovano",
        Message: "Ovom akcijom resetuje glasanje na pocetak!"
      }
    }
  },
  Participant: {
    ident: {
      Text: "Šifra",
      Placeholder: "unesite šifru...",
      NewValue: "",
      Type: "text"
    },
    mb: {
      Text: "Matični broj",
      Placeholder: "unesite MB...",
      NewValue: "",
      Type: "text"
    },
    firstname: {
      Text: "Ime",
      Placeholder: "unesite vaše ime...",
      NewValue: "",
      Type: "text",
      RegEx: "^(?!\s*$).+",
      Class: "font-weight-500"
    },
    familyname: {
      Type: "text",
      Text: "Prezime",
      Placeholder: "unesite vaše prezime..."
    },
    emails: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    cid: {
      Type: "text",
      Text: "Jbg",
      Placeholder: "unesite jbg..."
    },
    gid: {
      Type: "text",
      Text: "Lk",
      Placeholder: "unesite lk..."
    },
    contacts: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    birthdate: {
      Type: "date",
      Text: "Datum rođenja",
      Placeholder: "unesite datum..."
    },
    birthplace: {
      Type: "text",
      Text: "Mesto rođenja",
      Placeholder: "unesite mesto..."
    },
    birthstate: {
      Type: "text",
      Text: "Država rođenja",
      Placeholder: "unesite državu..."
    },
    address: {
      Type: "text",
      Text: "Ulica",
      Placeholder: "unesite ulicu..."
    },
    contact: {
      Type: "text",
      Text: "Kontakt",
      Placeholder: "unesite telefon..."
    },
    telephone: {
      Type: "text",
      Text: "Telefon",
      Placeholder: "unesite telefon..."
    },
    city: {
      Type: "text",
      Text: "Grad",
      Placeholder: "unesite grad..."
    },
    state: {
      Type: "text",
      Text: "Država",
      Placeholder: "unesite državu..."
    },
    sex: {
      Type: "select",
      Text: "Pol",
      Placeholder: "unesite pol...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        M: { guid: "M", title: "Muško" },
        Z: { guid: "Z", title: "Žensko" }
      }
    },
    religion: {
      Type: "select",
      Text: "Religija",
      Placeholder: "unesite religiju...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        Pravoslavac: { guid: "Pravoslavac", title: "Pravoslavac" },
        Katolik: { guid: "Katolik", title: "Katolik" },
        Musliman: { guid: "Musliman", title: "Musliman" }
      }
    },
    education: {
      Type: "select",
      Text: "Obrazovanje",
      Placeholder: "unesite obrazovanje...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        "100": {
          "guid": 100,
          "points": 1,
          "title": "Osnovno obrazovanje i vaspitanje"
        },
        "110": {
          "guid": 110,
          "points": 1,
          "title": "Osnovno obrazovanje odraslih"
        },
        "120": {
          "guid": 120,
          "points": 1,
          "title": "Osnovno muzičko/baletsko obrazovanje"
        },
        "200": {
          "guid": 200,
          "points": 2,
          "title": "Stručno osposobljavanje (1 godina)"
        },
        "210": {
          "guid": 210,
          "points": 2,
          "title": "Obrazovanje za rad (2 godine)"
        },
        "220": {
          "guid": 220,
          "points": 2,
          "title": "Obučavanje (120–360 sati obuke)"
        },
        "300": {
          "guid": 300,
          "points": 3,
          "title": "Srednje stručno obrazovanje u trajanju od tri godine"
        },
        "310": {
          "guid": 310,
          "points": 3,
          "title": "Neformalno obrazovanje odraslih (min 960 sati obuke)"
        },
        "400": {
          "guid": 400,
          "points": 4,
          "title": "Srednje obrazovanje u trajanju od četiri godine (stručno, umetničko, gimnazijsko)"
        },
        "500": {
          "guid": 500,
          "points": 5,
          "title": "Majstorsko i specijalističko obrazovanje"
        },
        "610": {
          "guid": 610,
          "points": 6.1,
          "title": "Osnovne akademske studije(OAS, 180 ESPB)"
        },
        "611": {
          "guid": 611,
          "points": 6.1,
          "title": "Osnovne strukovne studije (OSS, 180 ESPB)"
        },
        "620": {
          "guid": 620,
          "points": 6.2,
          "title": "Osnovne akademske studije(OAS, 240)"
        },
        "621": {
          "guid": 621,
          "points": 6.2,
          "title": "Specijalističke strukovne studije I stepena(SSS, 180+60 ESPB, stečene u skladu sa Zakonom o visokom obrazovanju koji je na snazi od 07.10.2017.)"
        },
        "710": {
          "guid": 710,
          "points": 7.1,
          "title": "Integrisane akademske studije (IAS, maks. 360 ESPB)"
        },
        "711": {
          "guid": 711,
          "points": 7.1,
          "title": "Master akademske studije (MAS,180+120 ili 240+60 ESPB)"
        },
        "712": {
          "guid": 712,
          "points": 7.1,
          "title": "Master strukovne studije (MSS, 120 ESPB)"
        },
        "721": {
          "guid": 721,
          "points": 7.2,
          "title": "Specijalističke akademske studije (SAS, 60 ESPB)"
        },
        "800": {
          "guid": 800,
          "points": 8,
          "title": "Doktorske studije (DS, 180 ESPB)"
        }
      }
    },
    status: {
      Type: "select",
      Text: "Status",
      Placeholder: "unesite pol...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)", color: "#414a45" },
        S: { guid: "S", title: "siguran", color: "#8bc7a3" },
        R: { guid: "R", title: "razgovor", color: "#4287f5" },
        P: { guid: "P", title: "potencijal", color: "#0c3475" },
        I: { guid: "I", title: "inostranstvo", color: "#d0d3d9" },
        O: { guid: "O", title: "opozicija", color: "#c78b91" },
        N: { guid: "N", title: "nepoznati", color: "#414a45" },
        A: { guid: "A", title: "apstinent", color: "#c1c482" },
        U: { guid: "U", title: "umrli", color: "#414a45" }
      }
    },
    votepuid: {
      Type: "text",
      Text: "Glasacko mesto",
      Placeholder: "unesite glasačno mesto..."
    },
    voteplace: {
      Type: "text",
      Text: "Glasačko mesto",
      Placeholder: "unesite glasačno mesto..."
    },
    votemember: {
      Type: "text",
      Text: "Mesni odbor",
      Placeholder: "unesite mesni odbor..."
    },
    votesecure: {
      Type: "text",
      Text: "Nosi",
      Placeholder: "unesite koliko nosi..."
    },
    votestatus: {
      Type: "text",
      Text: "Stanje",
      Placeholder: "unesite status glasanja",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "default", title: "(prazno)", color: "#414a45" },
        vote: { guid: "vote", title: "Glasao", color: "#8bc7a3" },
        apstinent: { guid: "abstainer", title: "Apstinent", color: "#4287f5" }
      }
    },
    leadname: {
      Type: "text",
      Text: "Vodi",
      Placeholder: "unesite ko vodi..."
    },
    leadcontact: {
      Type: "text",
      Text: "Vodi kontakt",
      Placeholder: "unesite telefon..."
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "...",
      Rows: 2
    }
  },
  User: {
    ident: {
      Text: "Šifra",
      Placeholder: "unesite šifru...",
      NewValue: "",
      Type: "text"
    },
    mb: {
      Text: "Matični broj",
      Placeholder: "unesite MB...",
      NewValue: "",
      Type: "text"
    },
    firstname: {
      Text: "Ime",
      Placeholder: "unesite vaše ime...",
      NewValue: "",
      Type: "text",
      RegEx: "^(?!\s*$).+",
      Class: "font-weight-500"
    },
    familyname: {
      Type: "text",
      Text: "Prezime",
      Placeholder: "unesite vaše prezime..."
    },
    emails: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    cid: {
      Type: "text",
      Text: "Jbg",
      Placeholder: "unesite jbg..."
    },
    gid: {
      Type: "text",
      Text: "Lk",
      Placeholder: "unesite lk..."
    },
    contacts: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    birthdate: {
      Type: "date",
      Text: "Datum rođenja",
      Placeholder: "unesite datum..."
    },
    birthplace: {
      Type: "text",
      Text: "Mesto rođenja",
      Placeholder: "unesite mesto..."
    },
    birthstate: {
      Type: "text",
      Text: "Država rođenja",
      Placeholder: "unesite državu..."
    },
    address: {
      Type: "text",
      Text: "Ulica",
      Placeholder: "unesite ulicu..."
    },
    contact: {
      Type: "text",
      Text: "Kontakt",
      Placeholder: "unesite telefon..."
    },
    telephone: {
      Type: "text",
      Text: "Telefon",
      Placeholder: "unesite telefon..."
    },
    city: {
      Type: "text",
      Text: "Grad",
      Placeholder: "unesite grad..."
    },
    state: {
      Type: "text",
      Text: "Država",
      Placeholder: "unesite državu..."
    },
    sex: {
      Type: "select",
      Text: "Pol",
      Placeholder: "unesite pol...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        M: { guid: "M", title: "Muško" },
        Z: { guid: "Z", title: "Žensko" }
      }
    },
    religion: {
      Type: "select",
      Text: "Religija",
      Placeholder: "unesite religiju...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        Pravoslavac: { guid: "Pravoslavac", title: "Pravoslavac" },
        Katolik: { guid: "Katolik", title: "Katolik" },
        Musliman: { guid: "Musliman", title: "Musliman" }
      }
    },
    education: {
      Type: "select",
      Text: "Obrazovanje",
      Placeholder: "unesite obrazovanje...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        "100": {
          "guid": 100,
          "points": 1,
          "title": "Osnovno obrazovanje i vaspitanje"
        },
        "110": {
          "guid": 110,
          "points": 1,
          "title": "Osnovno obrazovanje odraslih"
        },
        "120": {
          "guid": 120,
          "points": 1,
          "title": "Osnovno muzičko/baletsko obrazovanje"
        },
        "200": {
          "guid": 200,
          "points": 2,
          "title": "Stručno osposobljavanje (1 godina)"
        },
        "210": {
          "guid": 210,
          "points": 2,
          "title": "Obrazovanje za rad (2 godine)"
        },
        "220": {
          "guid": 220,
          "points": 2,
          "title": "Obučavanje (120–360 sati obuke)"
        },
        "300": {
          "guid": 300,
          "points": 3,
          "title": "Srednje stručno obrazovanje u trajanju od tri godine"
        },
        "310": {
          "guid": 310,
          "points": 3,
          "title": "Neformalno obrazovanje odraslih (min 960 sati obuke)"
        },
        "400": {
          "guid": 400,
          "points": 4,
          "title": "Srednje obrazovanje u trajanju od četiri godine (stručno, umetničko, gimnazijsko)"
        },
        "500": {
          "guid": 500,
          "points": 5,
          "title": "Majstorsko i specijalističko obrazovanje"
        },
        "610": {
          "guid": 610,
          "points": 6.1,
          "title": "Osnovne akademske studije(OAS, 180 ESPB)"
        },
        "611": {
          "guid": 611,
          "points": 6.1,
          "title": "Osnovne strukovne studije (OSS, 180 ESPB)"
        },
        "620": {
          "guid": 620,
          "points": 6.2,
          "title": "Osnovne akademske studije(OAS, 240)"
        },
        "621": {
          "guid": 621,
          "points": 6.2,
          "title": "Specijalističke strukovne studije I stepena(SSS, 180+60 ESPB, stečene u skladu sa Zakonom o visokom obrazovanju koji je na snazi od 07.10.2017.)"
        },
        "710": {
          "guid": 710,
          "points": 7.1,
          "title": "Integrisane akademske studije (IAS, maks. 360 ESPB)"
        },
        "711": {
          "guid": 711,
          "points": 7.1,
          "title": "Master akademske studije (MAS,180+120 ili 240+60 ESPB)"
        },
        "712": {
          "guid": 712,
          "points": 7.1,
          "title": "Master strukovne studije (MSS, 120 ESPB)"
        },
        "721": {
          "guid": 721,
          "points": 7.2,
          "title": "Specijalističke akademske studije (SAS, 60 ESPB)"
        },
        "800": {
          "guid": 800,
          "points": 8,
          "title": "Doktorske studije (DS, 180 ESPB)"
        }
      }
    },
    status: {
      Type: "select",
      Text: "Status",
      Placeholder: "unesite pol...",
      RowsValue: "guid",
      RowsCation: "title",
      Rows: {
        default: { guid: "", title: "(prazno)" },
        M: { guid: "M", title: "Član" },
        A: { guid: "A", title: "Spisak" },
        P: { guid: "P", title: "Čeka" },
        B: { guid: "B", title: "Odbijen" },
        C: { guid: "C", title: "Pozvan" }
      }
    },
    votepuid: {
      Type: "text",
      Text: "Glasacko mesto",
      Placeholder: "unesite glasačno mesto..."
    },
    voteplace: {
      Type: "text",
      Text: "Glasačko mesto",
      Placeholder: "unesite glasačno mesto..."
    },
    votemember: {
      Type: "text",
      Text: "Mesni odbor",
      Placeholder: "unesite mesni odbor..."
    },
    votesecure: {
      Type: "text",
      Text: "Sigurno",
      Placeholder: "unesite sigurno..."
    },
    votelead: {
      Type: "text",
      Text: "Vodi",
      Placeholder: "unesite ko vodi..."
    },
    description: {
      Text: "",
      NewValue: "",
      Type: "area",
      Placeholder: "unesite opis...",
      Rows: 2
    }
  },
  Auth: {
    firstname: {
      Type: "text",
      Text: "Ime",
      Placeholder: "unesite vaše ime..."
    },
    familyname: {
      Type: "text",
      Text: "Prezime",
      Placeholder: "unesite vaše prezime..."
    },
    emails: {
      Type: "text",
      Text: "Emailovi",
      Placeholder: "unesite emailove..."
    },
    birthdate: {
      Type: "date",
      Text: "Datum rođenja",
      Placeholder: "unesite datum..."
    },
    birthplace: {
      Type: "text",
      Text: "Mesto rođenja",
      Placeholder: "unesite mesto..."
    },
    birthstate: {
      Type: "text",
      Text: "Država rođenja",
      Placeholder: "unesite državu..."
    },
    state: {
      Type: "text",
      Text: "Država",
      Placeholder: "unesite državu..."
    },
    sex: {
      Type: "text",
      Text: "Pol",
      Placeholder: "unesite pol..."
    },
    image: {
      Type: "text",
      Text: "Fotografija",
      Placeholder: "unesite fotografiju..."
    },
    Types: {
      Email: {

      }
    },
    JMBGReg: {
      "1": "stranci u BiH",
      "2": "stranci u Crnoj Gori",
      "3": "stranci u Hrvatskoj",
      "4": "stranci u Makedoniji",
      "5": "stranci u Sloveniji",
      "6": "Nedefinisano mesto rodjenja",
      "7": "stranci u Srbiji (bez pokrajina)",
      "8": "stranci u Vojvodini",
      "9": "stranci u Kosovu",
      "10": "Banja Luka",
      "11": "Bihac",
      "12": "Doboj",
      "13": "Goražde",
      "14": "Livno",
      "15": "Mostar",
      "16": "Prijedor",
      "17": "Sarajevo",
      "18": "Tuzla",
      "19": "Zenica",
      "20": "Crna Gora",
      "21": "Podgorica",
      "22": "Crna Gora",
      "23": "Crna Gora",
      "24": "Crna Gora",
      "25": "Crna Gora",
      "26": "Nikšic",
      "27": "Crna Gora",
      "28": "Crna Gora",
      "29": "Pljevlja",
      "30": "Osijek, Slavonija region",
      "31": "Bjelovar, Virovitica, Koprivnica, Pakrac, Podravina region",
      "32": "Varaždin, Medimurje region",
      "33": "Zagreb",
      "34": "Karlovac",
      "35": "Gospic, Lika region",
      "36": "Rijeka, Pula, Istra and Primorje",
      "37": "Sisak, Banovina region",
      "38": "Split, Zadar, Dubrovnik, Dalmacija region",
      "39": "Hrvatska",
      "40": "",
      "41": "Bitolj",
      "42": "Kumanovo",
      "43": "Ohrid",
      "44": "Prilep",
      "45": "Skoplje",
      "46": "Strumica",
      "47": "Tetovo",
      "48": "Veles",
      "49": "Štip",
      "50": "Slovenija",
      "70": "Srbija",
      "71": "Beograd",
      "72": "Šumadija i Pomoravlje",
      "73": "Niš",
      "74": "Južna Morava",
      "75": "Zajecar",
      "76": "Podunavlje",
      "77": "Podrinje i Kolubara",
      "78": "Kraljevo",
      "79": "Užice",
      "80": "Novi Sad",
      "81": "Sombor",
      "82": "Subotica",
      "83": "Vojvodina",
      "84": "Vojvodina",
      "85": "Zrenjanin",
      "86": "Pancevo",
      "87": "Kikinda",
      "88": "Ruma",
      "89": "Sremska Mitrovica",
      "90": "Kosovo i Metohija",
      "91": "Priština",
      "92": "Kosovska Mitrovica",
      "93": "Pec",
      "94": "Ðakovica",
      "95": "Prizren",
      "96": "Kosovsko Pomoravski okrug",
      "97": "Kosovo i Metohija",
      "98": "Kosovo i Metohija",
      "99": "Kosovo i Metohija"
    },
    Dialog: {
      AuthInTitle: "Prijava",
      AuthInDesc: "Unesite email da nastavite sa procesom prijave."
    },
    Button: {
      Next: {
        Text: "<span><i class='fa fa-play'></i>&nbsp;Proveri</span>",
        Class: "btn btn-success btn-block",
        ClassEmpty: "btn btn-outline-success btn-block"
      },
      Check: {
        Text: "<span><i class='fa fa-check'></i>&nbsp;Proveri</span>",
        Class: "btn btn-success btn-block",
      },
      Edit: {
        Text: "<i class='fa fa-edit'></i>&nbsp;promeni",
        Class: "btn btn-xs btn-link btn-primary white",
      },
      Send: {
        Text: "<i class='fa fa-send'></i>&nbsp;send",
        Class: "btn btn-sm btn-block text-uppercase",
      },
      SendSM: {
        Text: "<i class='fa fa-send'></i>",
        Class: "btn ",
      }
    }
  },
  Config: {
    Title: "Config",
    Color: "#00bcd4",
    LicCon: "<i class='fa fa-certificate'></i>&nbsp;Licence/Config",
    AdmAut: "<i class='fa fa-sign-in'></i>&nbsp;Admins/Auths",
    SerUse: "<i class='fa fa-server'></i>&nbsp;Server/Usage",
    Live: "<i class='fa fa-globe fa-spin'></i>&nbsp;Live/Usage",
    MemUse: "<i class='fa fa-microchip'></i>&nbsp;Memorija <strong>{0}</strong> MB koristi  <strong>{1}</strong> MB Redis {2}",
    UpTime: "<i class='fa fa-chevron-circle-up'></i>&nbsp;Radi <strong>{0}</strong> sekundi od {1}",
    Description: "Public platform for direct democracy",
    Server: {
      DebugLevel: {
        Text: "Debug level",
        NewValue: "5",
        Type: "number",
        Placeholder: "1..10"
      },
      Admins: {
        Type: "emails",
        Text: "Admins",
        Placeholder: "Admins emails..."
      }
    },
    Licence: {
      Origin: {
        Type: "select",
        Text: "Type",
        Placeholder: "Mandatory...",
        RowsValue: "guid",
        RowsCation: "title",
        Rows: {
          default: { guid: "", title: "(self)" },
          remote: { guid: "remote", title: "Remote" }
        }
      },
      RemoteCheck: true,
      RemoteCheckUrl: {
        Type: "text",
        Text: "URL",
        Placeholder: "Remote check URL..."
      }
    }
  }
};

SiteLng = DefLng;

module.exports = {
  getDefault: function (iValue) {
    return DefLng;
  },
  getLng: function (iValue) {
    return SiteLng;
  }
}