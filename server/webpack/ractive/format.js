var  formatStatus = function(iValue, iStates) {

  var fStates  = iStates.split(',');
  var fFormat  = "default";
  var fCaption = iValue;

  if(fStates[iValue]){
    fFormat = "default";
    var fState  = fStates[iValue].split('=');
    fCaption=fState[0];
    if(fState[1]){
      fFormat = fState[1];
    }
  }

  return "<span class='label label-"+fFormat+"'>"+fCaption+"</span>";
}
var  formatLoop = function(iValue, iCodebook, iFormat, iKey, iText) {
  var fResult = "";
  var fCodebook  = getArray(iCodebook);
  var fValue  = getArray(iValue);
  var fFormat = (iFormat)? iFormat : "{0}";
  
  for(var c=0; c<fValue.length; c++){
    var fV1 = fValue[c];

    for(i=0; i<fCodebook.length; i++){
      if(fV1==fCodebook[i][iKey]){
        fCaption = setReplace(fFormat, '{1}', fCodebook[i][iKey]);
        fCaption = setReplace(fFormat, '{0}', fCodebook[i][iText]);
        fResult += fCaption;
      }
    }
  }

  return fResult;
}
var formatStr = function(iValue){
  if(typeof(iValue)==="string"){
    return iValue;
  }

  return '<div class="animationLoading"><div class="animationItem"></div></div>';
}
var getArray = function(iValue){
  if(Array.isArray(iValue)){
    return iValue
  }
  if(typeof(iValue)=='string'){
    return iValue.split(',');
  }
  if(typeof(iValue)=='object'){
    var fResult = [];
    for(var key in iValue){
      fResult.push(iValue[key]);
    }
    return fResult;
  }

  return [];
}
var  setReplace = function(iValue, iFrom, iTo) {
  return iValue.replace(iFrom, iTo);
}
var  formatLabelOld = function(iValue, iFormat, iCodebook, iText) {
  var fResult = "";
  var fLabes  = (Array.isArray(iValue))? iValue : iValue.split(',');
  var fFormat = (iFormat)? iFormat : 'default';

  for(i=0; i<fLabes.length; i++){
    var fCaption = fLabes[i];
    if((iCodebook)&&(iText)){
      fCaption = iCodebook[fCaption][iText];
    }
    
    fResult = fResult + "<span class='label label-"+fFormat+"'>"+fCaption+"</span> ";
  }

  return fResult;
}
var  formatLabel = function(iValue, iCodebook, iDefault) {
  var fResult = ((iDefault)? iDefault:iValue);

  if(iCodebook){
    if(iCodebook.Rows&&iCodebook.Rows[iValue]){
      fResult = iCodebook.Rows[iValue].title;
    }else{
      if(iCodebook[iValue]){
        fResult = iCodebook[iValue];
      }
    }
  }

  return fResult;
}
var  formatHtml = function(iValue, iDefault) {
  return iValue.replace("\n", "<br>");
}  
var  formatText = function(iValue, iCodebook, iText, iDefault, iFormat) {
  var fResult = ((iDefault)? iDefault:"");
  var fLabes  = (Array.isArray(iValue))? iValue : iValue.split(',');
  var fFormat = (iFormat)? iFormat : 'default';

  for(i=0; i<fLabes.length; i++){
      var fCaption = fLabes[i];
      if((iCodebook)&&(iText)&&(iCodebook[iText])){
        fCaption = iCodebook[iText];
      }

      fResult =  "<span class='label label-"+fFormat+"'>"+fCaption+"</span> ";
  }

  return fResult;	
}
var formatNumber = function(iNumber, iDecimal) {
  if ((typeof iNumber==='undefined')||(iNumber==""))
      return "0.00";
  if(!iDecimal){
    iDecimal = 0;
  }
  return parseFloat(iNumber).toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
var formatMoney = function(iNumber) {
  if ((typeof iNumber==='undefined')||(iNumber==""))
      return "0.00";

  return parseFloat(iNumber).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
var formatBase64 = function(iValue, iDefault) {
  if ((typeof iValue==='undefined')||(iValue.trim()==""))
  {
      var iValue = "";

      if ((typeof iDefault!='undefined'))
          iValue = iDefault;
  }

  iValue = btoa(iValue).replace('==', '').replace('=', '').trim();

  return iValue;
}
var formatSrc = function(iValue, iDefault) {
  if ((typeof iValue==='undefined')||(!iValue)||((iValue)&&(iValue.trim()==""))) {
      if ((typeof iDefault!='undefined')&&(iDefault)){
        return iDefault;
      }

      return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX/TQBcNTh/AAAAAXRSTlMAQObYZgAAAApJREFUeJxjYgAAAAYAAzY3fKgAAAAASUVORK5CYII=';
  }

  if((!iValue.indexOf('http')==0)&&(!iValue.indexOf('//')==0)){
    iValue = "//direktnademokratija.com/data/img/"+iValue;
  }

  return iValue;
}
var formatSrcSource = function(iSources, iSourceId) {
  if(iSources&&iSourceId&&iSources[iSourceId]&&iSources[iSourceId].source_icon){
    var fLink = iSources[iSourceId].source_icon;
    if(fLink.trim().toLowerCase().indexOf('http')==0){
      iValue = fLink;
    }else{
      iValue = "//direktnademokratija.com/data/img/"+iSources[iSourceId].source_icon;
    }
  }else{
    return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX/TQBcNTh/AAAAAXRSTlMAQObYZgAAAApJREFUeJxjYgAAAAYAAzY3fKgAAAAASUVORK5CYII=';
  }

  return iValue;
}
var formatSrcUser = function(iValue, iName) {
  if ((typeof iValue==='undefined')||(!iValue)||((iValue)&&(iValue.trim()=="")))
  {
      var iValue = "/res/img/user_default.jpg";// /res/img/default.png";

      if ((typeof iDefault!='undefined'))
          iValue = iDefault;

      return iValue;
  }

  return iValue;
}
var formatTime = function(iValue) {
  if ((typeof iValue==='undefined')||(iValue==""))
      return "0:00";

  var fMin 	= parseFloat(iValue%60).toFixed(0);
  var fHour = parseFloat(iValue/60).toFixed(0);
  fTime = fHour+':'+((fMin<10)? '0':'') + fMin;

  return fTime;	
}
var formatDT = function(iValue) {
  if ((typeof iValue==='undefined')||(iValue==""))
      return "0:00";

  return formatDateTime(iValue, "");

  var fMin 	= parseFloat(iValue.substring(14, 16)).toFixed(0);
  var fHour = parseFloat(iValue.substring(12, 14)).toFixed(0);
  var fDate = '';
  fTime = fHour+':'+((fMin<10)? '0':'') + fMin;

  if( (iValue)&&(iValue.indexOf("-")>0) ) 
      fDate = iValue.substring(8, 10)+"."+iValue.substring(5, 7)+"."+iValue.substring(0, 4);

  return fTime+' '+fDate;
}
var formatAgo = function(iValue, iDefault, iTick) {
  if((typeof iValue==="undefined")||(iValue=="")||(!iValue)){
    if(iDefault){
      return iDefault;
    }else{
      return "0:00";
    }
  }
  if(iValue=="0000-00-00 00:00:00"){
    return "odavno"; //"beginning of time";
  }

  var fDate = iValue;
  if(Number.isInteger(iValue)){
    fDate = new Date().setTime(iValue);
  }else{
    if(typeof(iValue)=="string"){
      //fDate = new Date(iValue.replace('T', ' ').substring(0, 18));
      fDate = new Date(iValue);
    }
  }

  var today = new Date();
  var diffMs = (today - fDate); 
  var diffDays = Math.floor(diffMs / 86400000); // days
  var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
  var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

  if((diffDays==0)&&(diffHrs==0)&&(diffMins<2)) return 'upravo sada';
  if((diffDays==0)&&(diffHrs==0)&&(diffMins<=59)) return 'pre ' + diffMins + ' minuta';
  if((diffDays==0)&&(diffHrs<=4)&&(diffHrs<=24)) return 'pre ' + diffHrs + ' sata';
  if((diffDays==0)&&(diffHrs>=5)&&(diffHrs<=24)) return 'pre ' + diffHrs + ' sati';

  if(diffDays==1){
    var fMin 	= fDate.getMinutes();
    var fHour =  fDate.getHours();
    fTime = ((fHour<10)? '0':'') + fHour+':'+((fMin<10)? '0':'') + fMin;

    return fTime + ' ju&#269;e';
  }

  return  formatDateTime(iValue, "");
}
var formatDateTime = function(iValue, iDefault) {
  if ((typeof iValue==='undefined')||(iValue=="")){
    return ((iDefault)? iDefault:"");
  }
  var fDate = iValue;
  if(Number.isInteger(iValue)){
    fDate = new Date().setTime(iValue);
  }else{
    if(typeof(iValue)=="string"){
      fDate = new Date(iValue.replace('T', ' ').substring(0, 18));
    }
  }

  function checkZero(data){
    if(data.length == 1){
      data = "0" + data;
    }
    return data;
  }

  var day = fDate.getDate() + "";
  var month = (fDate.getMonth() + 1) + "";
  var year = fDate.getFullYear() + "";
  var hour = fDate.getHours() + "";
  var minutes = fDate.getMinutes() + "";

  fDate = checkZero(hour)+":"+checkZero(minutes)+" "+checkZero(day)+"."+checkZero(month)+"."+checkZero(year)+".";

  return fDate.toString();
}
var formatDate = function(iValue) {
  if ((typeof iValue==='undefined')||(iValue=="")){
    return "";
  }

  if(Number.isInteger(iValue)){
    iValue = new Date(1000*iValue);
  }
  if(iValue.length==10){
    iValue = new Date(iValue);
  }
  var monthNames = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul","Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
  if(SiteLng&&SiteLng.Date&&SiteLng.Date.Month){
    monthNames = SiteLng.Date.Month;
  }
  var day = iValue.getDate();
  var monthIndex = iValue.getMonth();
  var year = iValue.getFullYear();

  return day + '. ' + monthNames[monthIndex] + ' ' + year+'.';

  new Locale.Builder().setLanguage("sr").setScript("Latn").build();
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  return iValue.toLocaleDateString("sr-RS", options); 
}
var formatDateLong = function(iValue) {
  if ((typeof iValue==='undefined')||(iValue=="")){
    return "";
  }

  if(Number.isInteger(iValue)){
    iValue = new Date(1000*iValue);
  }

  if(iValue.length==10){
    iValue = new Date(iValue);
  }

  var monthNames = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul","Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
  if(SiteLng&&SiteLng.Date&&SiteLng.Date.Month){
    monthNames = SiteLng.Date.Month;
  }
  var daysNames = ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "Cetvrtak", "Petak", "Subota"];
  if(SiteLng&&SiteLng.Date&&SiteLng.Date.Day){
    daysNames = SiteLng.Date.Day;
  }

  var day = iValue.getDate();
  var monthIndex = iValue.getMonth();
  var year = iValue.getFullYear();

  return daysNames[iValue.getDay()] + ', ' + day + '. ' + monthNames[monthIndex] + ' ' + year+'.';
  /*
  new Locale.Builder().setLanguage("sr").setScript("Latn").build();
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  return iValue.toLocaleDateString("sr-RS", options); 
  */
}
var formatColor = function(iValue, iColor){
    if(iColor){
      return iColor;
    }
    if(iValue){
      if(iValue.length<5){
        iValue = iValue.toString().padEnd(5,'A');
      } 

      var hash = 0;

      for (var i = 0; i < iValue.length; i++) {
        hash = (iValue.charCodeAt(i)?iValue.charCodeAt(i):'a') + ((hash << 5) - hash);
      }
      var fColor = (hash & 0x00FFFFFF).toString(16).toUpperCase().padEnd(6, 'F');;

      return (fColor)? fColor:"#00000";
    }

    return "#00000";
}
var formatBackMedia = function(iValue){
  if(!iValue){
    return;
  }
  var fValue = iValue;
  var fHtml = '';
  var fYTback = ' <div class="hidden-xs video-background"><iframe id="bckvideo" type="text/html" frameborder="0" height="100%" width="100%"   src="{{link}}"  allowfullscreen></iframe></div>';
  var fImg = '<style>body { background-image: url({{link}}); }</style>';
  
  if(Array.isArray( iValue)){
    var bckList = iValue;
    var rnd = Math.floor(Math.random() * bckList.length);
    fValue = iValue[rnd];
}

  if((fValue)&&(fValue!="")){
    if(fValue.indexOf('youtube') !== -1){
      fValue = formatYTBack(fValue);
      fHtml = fYTback.replace('{{link}}', fValue);
      //var myVideo =  iframe.getElementById('myVideo'); 
      //myVideo.mute();                
    }else{
      fHtml = fImg.replace('{{link}}', fValue);
    }
  }
  console.log('test', iValue, fHtml);
  return fHtml;
}
var formatYTBack = function(iValue){

  var fValue = iValue;
  var n = fValue.indexOf("?");
  var fVideoUrl = iValue;//(n)? fValue.substring(0, n):fValue;
  var fSplit = fValue.split('&');
  var fParams = "?rel=0&enablejsapi=1&autoplay=1&controls=0&showinfo=0&loop=1&iv_load_policy=3&mute=1";
  //rel=0&enablejsapi=1&autoplay=1&controls=0&showinfo=0&loop=1&iv_load_policy=3&mute=1&list=PL7jzVA9xYSRg8Xd2VXqV5QVVqfghuhuHI&index=5
  fVideoUrl = fVideoUrl+fParams;

  for(var i=1;i<fSplit.length; i++){
    var fKey = fSplit[i];
    var fKeyName = fKey.split('=')[0];
    if(fParams.indexOf(fKeyName)==-1){
      fVideoUrl = fVideoUrl+'&'+fKey;
    }
  }

  return fVideoUrl;
}
var formatTitle = function(iKind, iRow){
  if(iRow&&iRow.title){
    return iRow.title;
  }
  if(iKind){
    if(SiteLng&&SiteLng[iKind]&&SiteLng[iKind].Title){
      return SiteLng[iKind].Title;
    }
  }

  return (iKind)? iKind:'.';
}
var formatCaption = function(iRow){
  if(iRow.Caption){
    return iRow.Caption;
  }

  if(iRow.Properties){
    var fCaption = "";
    for(var key in iRow.Properties){
      if(iRow.Properties[key]){
        if(((fCaption+" "+iRow.Properties[key]).toString().length)<55){
          fCaption = fCaption+" "+iRow.Properties[key];
        }
      }
    }
    return fCaption;
  }
  return '';
}
var getCount = function (iValue){
  if(typeof(iValue)=="string"){
    if(iValue==""){
      return 0;
    }
    return iValue.split(',').length;
  }
  if(iValue){
    if(Array.isArray(iValue)) {
      return iValue.length;
    }else{
      var i=0;
      for(var key in iValue){
        i++;
      }
      return i;
    }
  }

  return 0;
}
var getBase64 = function (iValue){
  var fValue = JSON.stringify(iValue);
  fValue = encodeURIComponent(fValue);
  fValue = btoa(fValue).toString().replace('==', '').replace('=', '').trim();

  return fValue;
}
var getDateObj = function (iValue){
  var dateString  = iValue;
  var year        = dateString.substring(0,4);
  var month       = dateString.substring(4,6);
  var day         = dateString.substring(6,8);

  return new Date(year, month-1, day);
}
var getDateYMD = function(date) {
  var d = ((date)? new Date(date):new Date()),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}
var getDateYMDShort = function(date) {
  var d = ((date)? new Date(date):new Date());
  var month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('');
}
var isEmpty = function(iValue) {
  if(typeof iValue==="undefined"){
    return true;
  }

  if((iValue)&&(iValue=="")){
    return true;
  }

  return !(iValue);
}
var isNotEmpty = function(iValue) {
  if(typeof iValue==="undefined"){
    return false;
  }

  if((iValue)&&(iValue=="")){
    return false;
  }

  return ((iValue)&&(iValue!=""));
}
var formatOnline = function(iUserUID) {
  if((Global)&&(Global.Users)&&(Global.Users.Online)&&(typeof Global.Users.Online[iUserUID]!='undefined')){
    return 'online';
  }

  return 'off';
}
var formatHref = function(iPage) {
  var fHref="";

  return fHref;
}
var formatTextShort = function(iText, iLen) {
  if(iText){
    return iText.substring(0, iLen)+"...";
  }

  return iText;
}
var formatDateObj = function(iDate) {
  var fReturn = {
    Text: "",
    Day: "",
    Month: "",
    Year: ""
  };
  var monthNames = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul","Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
  if(SiteLng&&SiteLng.Date&&SiteLng.Date.Month){
    monthNames = SiteLng.Date.Month;
  }
  var fDate = new Date(iDate.StartYear, 0, 1, 0, 0, 0, 0);

  fDate.setDate(fDate.getDate() + parseInt(iDate.Current));
  fReturn.Day = fDate.getDate();
  fReturn.Month = fDate.getMonth()+1;
  fReturn.MonthIndex = fDate.getMonth();
  fReturn.MonthName = monthNames[fReturn.MonthIndex];
  fReturn.Year = fDate.getFullYear();
  
  fReturn.Text = fReturn.Day + '. ' + monthNames[fReturn.MonthIndex] + ' ' + fReturn.Year+'.'
  
  return fReturn;
}
var formatIsDetails = function(iRow){
    return (iRow&&iRow.guid);
}
var formatIsMine = function(iData, iUser){
  //return (iRow&&iRow.guid);
  return true;
}
var formatEmails= function(iValue) {
  var fRetrun = "";
  if(iValue){
    var fStates  = iValue.split(',');
    fStates.forEach(function(element){
      fRetrun += "<span class='label label-default'>"+element+"</span>&nbsp;&nbsp;";
    });
  }

  return fRetrun;
}
var formatSplitCount= function(iValue) {
  var fRetrun = 0;
  if(iValue){
    var fStates  = iValue.split(',');
    fStates.forEach(function(element){
      if(element){
        fRetrun++;
      }
    });
  }

  return fRetrun;
} 
var formatName = function(iValue){
  if((iValue)&&(fIntex=iValue.indexOf('@'))){
    return iValue.substring(0, fIntex);
  }
  return iValue;
}
var formatIsTrue = function(iValue){
  return (iValue&&iValue=="1");
}
var formatIsAdmin = function(iGroup){
  var fResult = false;
  var fStatus = ((iGroup)? iGroup:'U').toString().toUpperCase();

  switch(fStatus){
    case "A":
      break;
    case "U":
      if(Global&&Global.Session&&Global.Session.UserAuth){
        fResult = true;
      }
      break;
    default:
      break;
  }

  return fResult;
}
var formatHasMod = function(iItem, iSection){
  /*if(iSection){
    if(Global&&Global.Session&&Global.Session.UserAdmin&&Global.Session.UserAdmin[iSection.toLowerCase()]){
      return true;
    }
  }*/

  if(iItem&&iItem.moderators){
    if(Global&&Global.Session&&Global.Session.UserEmail&&-1!=iItem.moderators.indexOf(Global.Session.UserEmail)){
      return true;
    }
  }

  return false;
}
var formatHasAcc = function(iItem, iSection){
  /*if(iItem&&(!iItem.visibility||iItem.visibility=="open")){
    return true;
  }*/
  if(iItem&&iItem.visibility&&(iItem.visibility=="open")){
    return true;
  }

  if(iSection){
    if(Global&&Global.Session&&Global.Session.UserAdmin&&Global.Session.UserAdmin[iSection.toLowerCase()]){
      return true;
    }
  }

  if(iItem&&iItem.moderators){
    if(Global&&Global.Session&&Global.Session.UserEmail&&-1!=iItem.moderators.indexOf(Global.Session.UserEmail)){
      return true;
    }
  }

  if(iItem&&iItem.parcipients){
    if(Global&&Global.Session&&Global.Session.UserEmail&&-1!=iItem.parcipients.indexOf(Global.Session.UserEmail)){
      return true;
    }
  }

  return false;
}
var formatHasView = function(iItem, iSection){
  if(iItem&&iItem.visibility&&(iItem.visibility=="open"||iItem.visibility=="registration")){
    return true;
  }

  if(iSection){
    if(Global&&Global.Session&&Global.Session.UserAdmin&&Global.Session.UserAdmin[iSection.toLowerCase()]){
      return true;
    }
  }

  if(iItem&&iItem.moderators){
    if(Global&&Global.Session&&Global.Session.UserEmail&&-1!=iItem.moderators.indexOf(Global.Session.UserEmail)){
      return true;
    }
  }

  return false;
}
var formatUserStyle = function(iValue){
  var fColorName;
  if((iValue)&&(typeof(iValue)=="string")){
    fColorName = iValue;
  }
  if((iValue)&&(typeof(iValue)=="object")&&(iValue.UserUID)){
    fColorName = iValue.UserUID;
  }
  if(fColorName){
    var hash = 0;

    for (var i = 0; i < fColorName.length; i++) {
      hash = fColorName.charCodeAt(i) + ((hash << 5) - hash);
    }

    fColor = (hash & 0x00FFFFFF).toString(16).toUpperCase();

    return 'style="color: #'+fColor+';"';
  }
  // style="color: #{{format.Color(Session.UserUID)}};"
  return "";
}
var formatPrc  = function(iCount, iMax){
  if(iCount&&iMax){
    var fPrc = parseInt(((iCount*100)/iMax));
    return fPrc;
  }
  
  return 0;
}
var formatPhone = function(iValue){
  return (iValue)? iValue.split(' ').join('').trim():"";
}
var formatFormat  = function(iValue, iP0, iP1, iP2, iP3){
  var fValue = (iValue? iValue.toString():"");

  if(iP0) fValue = fValue.replace("{0}", iP0); else fValue = fValue.replace("{0}", "");
  if(iP1) fValue = fValue.replace("{1}", iP1); else fValue = fValue.replace("{1}", "");
  if(iP2) fValue = fValue.replace("{2}", iP2); else fValue = fValue.replace("{2}", "");
  if(iP3) fValue = fValue.replace("{3}", iP3); else fValue = fValue.replace("{3}", "");

  return fValue;
}
var isPage = function(iIndex, iPage){
  return (((iPage*50)<=iIndex)&&(iIndex<=((iPage+1)*50)));
}
module.exports.formatSrc = formatSrc;
module.exports.formatSrcUser = formatSrcUser;
module.exports.formatDT = formatDT;
module.exports.formatAgo = formatAgo;
module.exports.formatDate = formatDate;
module.exports.getBase64 = getBase64;
module.exports.getDateYMD = getDateYMD;
module.exports.getDateObj = getDateObj;
module.exports.getDateYMDShort = getDateYMDShort;
module.exports.getCount = getCount;
module.exports.formatStatus = formatStatus;
module.exports.formatColor = formatColor;
module.exports.formatMoney = formatMoney;
module.exports.formatLoop = formatLoop;
module.exports.formatLabel = formatLabel;
module.exports.formatText = formatText;
module.exports.formatTextShort = formatTextShort;
module.exports.formatYTBack = formatYTBack;
module.exports.formatHtml = formatHtml;
module.exports.formatCaption = formatCaption;
module.exports.isEmpty = isEmpty;
module.exports.isTrue = formatIsTrue;
module.exports.isNotEmpty = isNotEmpty;
module.exports.formatHref = formatHref;
module.exports.formatDateObj = formatDateObj;
module.exports.formatIsDetails = formatIsDetails;
module.exports.formatPrc = formatPrc;
module.exports.format= {
  Str: formatStr,
  Src: formatSrc,
  SrcUser: formatSrcUser,
  SrcSource: formatSrcSource,
  DT: formatDT,
  Ago: formatAgo,
  Date: formatDate,
  DateLong: formatDateLong,
  DateYMDShort: getDateYMDShort,
  Base64: getBase64,
  Status: formatStatus,
  Color: formatColor,
  Money: formatMoney,
  Loop: formatLoop,
  Label: formatLabel,
  Text: formatText,
  Number: formatNumber,
  TextShort: formatTextShort,
  YTBack: formatYTBack,
  Html: formatHtml,
  Title: formatTitle,
  Caption: formatCaption,
  isEmpty: isEmpty,
  isNotEmpty: isNotEmpty,
  IsNotEmpty: isNotEmpty,
  Count: getCount,
  Online: formatOnline,
  Href: formatHref,
  Array: getArray,
  BackMedia: formatBackMedia,
  DateObj: formatDateObj,
  isDetails: formatIsDetails,
  isMine: formatIsMine,
  Emails: formatEmails,
  SplitCount: formatSplitCount,
  Name: formatName,
  UserStyle: formatUserStyle,
  isTrue: formatIsTrue,
  isAdmin: formatIsAdmin,
  hasMod: formatHasMod,
  hasAcc: formatHasAcc,
  hasView: formatHasView,
  Prc: formatPrc,
  Phone: formatPhone,
  Format: formatFormat,
  isPage: isPage
}