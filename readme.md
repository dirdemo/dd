#### _*Javna platforma, za direktnu demokratiju, 2019.*_


# Introduction

This README would normally document whatever steps are necessary to get your application up and running.


# Install

## Prepare

##### Linux
```
cd ~
sudo su
apt update
apt install openssh-server -y
systemctl status ssh
ufw allow ssh
```

## Install

##### Linux
```
apt install git -y
ssh-keygen
cat ~/.ssh/id_rsa.pub

# https://bitbucket.org/dirdemo/dd/admin/access-keys/

git clone git@bitbucket.org:dirdemo/dd.git

cd dd/utils/
cp start-dd.sh ~/start-dd.sh
sh install-ubuntu.sh
```

#### Test database
```
wget -O history.db https://www.dropbox.com/s/fbemcv22589fd5h/history.db?dl=1
wget -O sources.db https://www.dropbox.com/s/005r78dsxp4jmt1/sources.db?dl=1
wget -O stat.db https://www.dropbox.com/s/kocoxrm291z6tef/stat.db?dl=1

wget -O date.zip https://www.dropbox.com/s/bmcr0wrhuuyfb69/date.zip?dl=1
wget -O link.zip https://www.dropbox.com/s/wb3rf4acrco2l5v/link.zip?dl=1
wget -O list.zip https://www.dropbox.com/s/qimgzxmd7q3q8ia/list.zip?dl=1
```

## Start

### Production
##### Linux
```
sh start-dd.sh
```

### Development
##### Server Linux&Windows
```
npm run dev
```
##### Client/Webpack Linux&Windows
```
npm run watch
```

# API

##  Back-end methods

Action

  * source: 
  * list:
  * pull:
  * auth:
  * vote:

## Front-end methods

  * getSource(): 
  * getList():
  * getPull():
  * getAuth():
  * setVote():

## Database

docker pull mongo
docker run --name some-mongo -d mongo


## Windows Host DNS
C:\Windows\System32\drivers\etc\hosts

127.0.0.1 test.dd.in.rs
127.0.0.1 party.dd.in.rs
127.0.0.1 demo.dd.in.rs
127.0.0.1 vb.dd.in.rs
