﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dd.Election
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private class Codebook
        {
            public String voteplace = String.Empty;
            public String lng = String.Empty;
            public String lat = String.Empty;
            public String email = String.Empty;

            public static Codebook FromCsv(string csvLine)
            {
                string[] values = csvLine.Split(',');
                Codebook c = new Codebook();

                c.voteplace = Convert.ToString(values[0]);
                c.lat = Convert.ToString(values[1]);
                c.lng = Convert.ToString(values[2]);
                c.email = Convert.ToString(values[3]);

                return c;
            }

            public static Codebook byId(List<Codebook> iCodebooks, string iKey)
            {
                foreach (Codebook fCodebook in iCodebooks)
                {
                    if (fCodebook.voteplace.StartsWith(iKey + " - "))
                    {
                        return fCodebook;
                    }
                }

                return null;
            }
        }

        private class Member
        {
            public String guid = String.Empty;
            public Int32 rev = 0;
            public String date = String.Empty;
            public String session = String.Empty;
            public String ident = String.Empty;
            public String mb = String.Empty;
            public String firstname = String.Empty;
            public String description = String.Empty;
            public String status = String.Empty;
            public String familyname = String.Empty;
            public String cid = String.Empty;
            public String gid = String.Empty;
            public String birthstate = String.Empty;
            public String birthplace = String.Empty;
            public String birthdate = String.Empty;
            public String state = String.Empty;
            public String city = String.Empty;
            public String address = String.Empty;
            public String telephone = String.Empty;
            public String votesecure = String.Empty;
            public String votepuid = String.Empty;
            public String voteplace = String.Empty;
            public String votemember = String.Empty;
            public String leadname = String.Empty;
            public String leadcontact = String.Empty;
            public String pguid = String.Empty;

            public void setValue(string iColName, int iIndex, string[] iValues, string[] iColumns)
            {
                try
                {
                    if (iIndex < iValues.Length)
                    {
                        String fValue = Convert.ToString(iValues[iIndex]).Trim().Replace("\"", "").Trim();
                        switch (iColName)
                        {
                            case "ident": this.ident = fValue; break;
                            case "mb": this.mb = fValue; break;
                            case "firstname": this.firstname = fValue; break;
                            case "familyname": this.familyname = fValue; break;
                            case "cid": this.cid = fValue; break;
                            case "gid": this.gid = fValue; break;
                            case "telephone": this.telephone = fValue; break;
                            case "birthdate": this.birthdate = fValue; break;
                            case "birthplace": this.birthplace = fValue; break;
                            case "birthstate": this.birthstate = fValue; break;
                            case "address": this.address = fValue; break;
                            case "city": this.city = fValue; break;
                            case "state": this.state = fValue; break;
                            case "votesecure": this.votesecure = fValue; break;
                            case "votepuid": this.votepuid = fValue; break;
                            case "voteplace": this.voteplace = fValue; break;
                            case "votemember": this.votemember = fValue; break;
                            case "leadname": this.leadname = fValue; break;
                            case "leadcontact": this.leadcontact = fValue; break;
                            case "pguid": this.pguid = fValue; break;
                            case "description": this.description = fValue; break;
                            case "status": this.status = fValue; break;
                        }
                    }
                }
                catch
                {

                }
            }

            public static String getValue(int iIndex, string[] iValues)
            {
                try
                {
                    if (iIndex < iValues.Length)
                    {
                        return Convert.ToString(iValues[iIndex]).Trim().Replace("\"", "").Trim();
                    }
                }
                catch
                {

                }

                return "";
            }

            public static String getValue(string iColName, string[] iValues, string[] iColumns)
            {
                try
                {
                    int iIndex = Array.IndexOf(iColumns, iColName);
                    if (iIndex < iValues.Length)
                    {
                        return Convert.ToString(iValues[iIndex]).Trim().Replace("\"", "").Trim();
                    }
                }
                catch
                {

                }

                return "";
            }

            public static Member FromCsv(string csvLine)
            {
                string[] values = csvLine.Split(',');
                Member m = new Member();
                /*
                m.guid = Convert.ToString(values[0]);
                m.rev = 1;
                m.date = "2022-10-12T00:13:38.963Z";
                m.session = "";
                m.ident = Convert.ToString(values[0]);
                m.mb = Convert.ToString(values[1]);
                m.firstname = Convert.ToString(values[2]);
                m.familyname = Convert.ToString(values[3]);
                m.cid = Convert.ToString(values[5]);
                m.gid = Convert.ToString(values[6]);
                m.telephone = Convert.ToString(values[7]);
                m.birthdate = Convert.ToString(values[8]);
                m.birthplace = Convert.ToString(values[9]);
                m.birthstate = Convert.ToString(values[10]);
                m.address = Convert.ToString(values[11]);
                m.city = Convert.ToString(values[13]);
                m.state = Convert.ToString(values[18]);
                m.votesecure = "1";
                m.votepuid = Convert.ToString(values[19]);
                m.voteplace = Convert.ToString(values[20]);
                m.votemember = "";
                m.leadname = "";
                m.leadcontact = "";
                m.pguid = Convert.ToString(values[19]);
                m.description = Convert.ToString(values[21]);
                m.status = "";
                */
                String votestr = getValue(2, values);
                int votei = votestr.IndexOf(")") + 1;
                String votepuid = "p" + votestr.Substring(0, votei).Replace("(", "").Replace(")", "").Replace(" ", "").Trim();
                String voteplace = votestr.Substring(votei).Trim();

                /*
                m.guid = "m" + getValue(0, values);
                m.ident = getValue(0, values);
                m.mb = getValue(1, values);
                m.firstname = getValue(2, values);
                m.familyname = getValue(3, values); //Convert.ToString(values[3]);
                m.cid = getValue(5, values);//Convert.ToString(values[5]);
                m.gid = getValue(6, values);//Convert.ToString(values[6]);
                m.telephone = getValue(7, values);//Convert.ToString(values[7]);
                m.birthdate = getValue(8, values);//Convert.ToString(values[8]);
                m.birthplace = getValue(9, values);//Convert.ToString(values[9]);
                m.birthstate = getValue(10, values);//Convert.ToString(values[10]);
                m.address = getValue(11, values);//Convert.ToString(values[11]);
                m.city = getValue(13, values);//Convert.ToString(values[13]);
                m.state = getValue(14, values);//Convert.ToString(values[18]);
                m.votesecure = "1";
                m.votepuid = votepuid;
                m.voteplace = voteplace;
                m.votemember = "";
                m.leadname = "";
                m.leadcontact = "";
                m.pguid = votepuid;
                m.description = getValue(21, values);//Convert.ToString(values[21]);
                m.status = getValue(18, values);
                */
                m.guid = "m" + getValue(0, values);
                m.ident = getValue(0, values);
                m.mb = getValue(1, values);
                m.firstname = getValue(4, values);
                m.familyname = getValue(3, values);
                m.cid = getValue(5, values);
                m.gid = getValue(6, values);
                m.telephone = getValue(7, values);
                m.birthdate = getValue(8, values);
                m.birthplace = getValue(9, values);
                m.birthstate = getValue(10, values);
                m.address = getValue(11, values);
                m.city = getValue(13, values);
                m.state = getValue(14, values);
                m.votesecure = "1";
                m.votepuid = votepuid;
                m.voteplace = voteplace;
                m.votemember = "";
                m.leadname = "";
                m.leadcontact = "";
                m.pguid = votepuid;
                m.description = getValue(21, values);
                m.status = getValue(18, values);

                return m;
            }

            public static Member FromCsv(string csvLine, string[] csvColl)
            {
                string[] values = csvLine.Split(',');
                Member m = new Member();
                String votestr = getValue("voteplace", values, csvColl);
                int votei = votestr.IndexOf(")") + 1;
                String votepid = votestr.Substring(0, votei).Replace("(", "").Replace(")", "").Replace(" ", "").Trim();
                String votepuid = "p" + votepid;
                String voteplace = votestr.Substring(votei).Trim();

                for (int i = 0; i < csvColl.Length; i++)
                {
                    m.setValue(csvColl[i], i, values, csvColl);
                }
                if (m.votepuid == "")
                {
                    m.votepuid = votepuid;
                    m.voteplace = votepid + " - " + voteplace;
                    m.pguid = votepuid;
                }
                m.guid = "m" + m.ident;
                /*
                m.guid = "m" + getValue("ident", values, csvColl);
                m.ident = getValue("ident", values, csvColl);
                m.mb = getValue("mb", values, csvColl);
                m.firstname = getValue("firstname", values, csvColl);
                m.familyname = getValue("familyname", values, csvColl);
                m.cid = getValue("cid", values, csvColl);
                m.gid = getValue("gid", values, csvColl);
                m.telephone = getValue("telephone", values, csvColl);
                m.birthdate = getValue("birthdate", values, csvColl);
                m.birthplace = getValue("birthplace", values, csvColl);
                m.birthstate = getValue("birthstate", values, csvColl);
                m.address = (getValue("address", values, csvColl) + " " + getValue("address1", values, csvColl) + " " + getValue("address2", values, csvColl)).Trim();
                m.city = getValue("city", values, csvColl);
                m.state = getValue("state", values, csvColl);
                m.votesecure = "1";
                m.votepuid = votepuid;
                m.voteplace = voteplace;
                m.votemember = getValue("votemember", values, csvColl);
                m.leadname = getValue("leadname", values, csvColl);
                m.leadcontact = getValue("leadcontact", values, csvColl);
                m.pguid = votepuid;
                m.description = getValue("description", values, csvColl);
                m.status = getValue("status", values, csvColl);*/

                return m;
            }

            public String toJson(String iTemplate)
            {
                String fStr = iTemplate;

                fStr = fStr.Replace("{guid}", this.guid);
                fStr = fStr.Replace("{rev}", this.rev.ToString());
                fStr = fStr.Replace("{date}", this.date);
                fStr = fStr.Replace("{session}", this.session);
                fStr = fStr.Replace("{ident}", this.ident);
                fStr = fStr.Replace("{mb}", this.mb);
                fStr = fStr.Replace("{firstname}", this.firstname);
                fStr = fStr.Replace("{description}", this.description);
                fStr = fStr.Replace("{status}", this.status);
                fStr = fStr.Replace("{familyname}", this.familyname);
                fStr = fStr.Replace("{cid}", this.cid);
                fStr = fStr.Replace("{gid}", this.gid);
                fStr = fStr.Replace("{birthstate}", this.birthstate);
                fStr = fStr.Replace("{birthplace}", this.birthplace);
                fStr = fStr.Replace("{birthdate}", this.birthdate);
                fStr = fStr.Replace("{state}", this.state);
                fStr = fStr.Replace("{city}", this.city);
                fStr = fStr.Replace("{address}", this.address);
                fStr = fStr.Replace("{telephone}", this.telephone);
                fStr = fStr.Replace("{votesecure}", this.votesecure);
                fStr = fStr.Replace("{votepuid}", this.votepuid);
                fStr = fStr.Replace("{voteplace}", this.voteplace);
                fStr = fStr.Replace("{votemember}", this.votemember);
                fStr = fStr.Replace("{leadname}", this.leadname);
                fStr = fStr.Replace("{leadcontact}", this.leadcontact);
                fStr = fStr.Replace("{pguid}", this.pguid);

                return fStr;
            }
        }

        private String FileFrom = String.Empty;
        private String DirTo = String.Empty;
        private int ItemsCount = 0;
        private Encoding FileEncoding = Encoding.GetEncoding("ISO-8859-1"); //Encoding.UTF8;

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (!this.bW.IsBusy)
            {
                this.btnLoad.Enabled = false;
                this.lblStat.Text = String.Format("Loading");
                this.FileFrom = this.fileFrom.Text;
                this.DirTo = this.dirTo.Text;
                this.ItemsCount = 0;
                this.bW.RunWorkerAsync();
            }
        }

        private void bW_DoWork(object sender, DoWorkEventArgs e)
        {
            String fFrom = this.FileFrom;
            String fTo = this.DirTo;
            String[] fColumns = File.ReadLines(fFrom).First().ToString().ToLower().Split(',');
            //List<Member> values = File.ReadAllLines(fFrom).Skip(1).Select(fLine => Member.FromCsv(fLine, fColumns)).ToList();
            List<Codebook> codebook = File.ReadAllLines("Codebook.csv").Select(fLine => Codebook.FromCsv(fLine)).ToList();

            List<Member> values = new List<Member>();
            using (StreamReader file = new StreamReader(fFrom))
            {
                int counter = 0;
                string fLine;

                while ((fLine = file.ReadLine()) != null)
                {
                    if (0 < counter)
                    {
                        values.Add(Member.FromCsv(fLine, fColumns));
                        if ((counter % 10) == 0)
                        {
                            this.bW.ReportProgress(counter);
                        }
                    }
                    counter++;
                }
                file.Close();
            }


            Directory.CreateDirectory(fTo);

            String fGroupId = String.Empty;
            String fGroupStr = String.Empty;
            String fGroupList = String.Empty;
            String fGroupTitle = String.Empty;

            int i = 0;
            int fGroup_went_max = 0;
            int fGroup_went_max_secure = 0;

            var Members = values.OrderBy(x => x.voteplace).ToList();
            this.ItemsCount = Members.Count;

            foreach (Member fMember in Members)
            {
                if (fGroupId != fMember.votepuid)
                {
                    if ((!String.IsNullOrEmpty(fGroupStr)) && (!String.IsNullOrEmpty(fGroupId)))
                    {
                        String fMemberFile = fTo + "\\" + fGroupId + "-member.db";
                        File.WriteAllText(fMemberFile, "{" + fGroupStr + "}", this.FileEncoding);

                        String fGroupFile = fTo + "\\" + fGroupId + ".db";
                        String fGroupCnt = Properties.Resources.GroupItem;

                        fGroupCnt = fGroupCnt.Replace("{guid}", fGroupId);
                        fGroupCnt = fGroupCnt.Replace("{title}", fGroupTitle);

                        fGroupCnt = fGroupCnt.Replace("{went_max}", fGroup_went_max.ToString());
                        fGroupCnt = fGroupCnt.Replace("{went_max_secure}", fGroup_went_max_secure.ToString());

                        Codebook fCode = Codebook.byId(codebook, fGroupId.Replace("p", ""));
                        if (fCode != null)
                        {
                            fGroupCnt = fGroupCnt.Replace("{moderator}", fCode.email);
                            fGroupCnt = fGroupCnt.Replace("{lat}", fCode.lat);
                            fGroupCnt = fGroupCnt.Replace("{lng}", fCode.lng);
                        }

                        if (fGroupId != "")
                        {
                            fGroupList += ((String.IsNullOrEmpty(fGroupList)) ? "" : ", ") + "\"" + fGroupId + "\": " + fGroupCnt;

                            File.WriteAllText(fGroupFile, fGroupCnt, this.FileEncoding);
                        }
                    }
                    fGroupStr = String.Empty;
                    fGroupId = fMember.pguid;
                    fGroupTitle = fMember.voteplace;
                    fGroup_went_max = 0;
                    fGroup_went_max_secure = 0;

                }
                fGroup_went_max++;
                if (fMember.status.ToUpper() == "S")
                {
                    fGroup_went_max_secure++;
                }
                String fItemStr = fMember.toJson(Properties.Resources.MemberJson);
                fGroupStr += ((String.IsNullOrEmpty(fGroupStr)) ? "" : ", ") + fItemStr;
                i++;
                if ((i % 10) == 0)
                {
                    this.bW.ReportProgress(i);
                }
            }

            String fMemberFileLast = fTo + "\\" + fGroupId + "-member.db";
            File.WriteAllText(fMemberFileLast, "{" + fGroupStr + "}", this.FileEncoding);

            String fGroupFileLast = fTo + "\\" + fGroupId + ".db";
            String fGroupCntLast = Properties.Resources.GroupItem;

            fGroupCntLast = fGroupCntLast.Replace("{guid}", fGroupId);
            fGroupCntLast = fGroupCntLast.Replace("{title}", fGroupTitle);

            fGroupCntLast = fGroupCntLast.Replace("{went_max}", fGroup_went_max.ToString());
            fGroupCntLast = fGroupCntLast.Replace("{went_max_secure}", fGroup_went_max_secure.ToString());

            Codebook fCodeLast = Codebook.byId(codebook, fGroupId.Replace("p", ""));
            if (fCodeLast != null)
            {
                fGroupCntLast = fGroupCntLast.Replace("{moderator}", fCodeLast.email);
                fGroupCntLast = fGroupCntLast.Replace("{lat}", fCodeLast.lat);
                fGroupCntLast = fGroupCntLast.Replace("{lng}", fCodeLast.lng);
            }

            if (fGroupId != "")
            {
                fGroupList += ((String.IsNullOrEmpty(fGroupList)) ? "" : ", ") + "\"" + fGroupId + "\": " + fGroupCntLast;

                File.WriteAllText(fGroupFileLast, fGroupCntLast, this.FileEncoding);
            }

            String fListFile = fTo + "\\election.db";
            File.WriteAllText(fListFile, "{" + fGroupList + "}", this.FileEncoding);
        }

        private void bW_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (0 < this.ItemsCount)
            {
                this.lblStat.Text = String.Format("{0}/{1}", e.ProgressPercentage, this.ItemsCount);
            }
            else
            {
                this.lblStat.Text = String.Format("{0}", e.ProgressPercentage);
            }
        }

        private void bW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.btnLoad.Enabled = true;
            this.lblStat.Text = String.Format("End");
        }

        private void bWExport_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        public DataTable jsonStringToTable(string jsonContent)
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);

            return dt;
        }

        public bool jsonToCsv(string jsonFile, string csvFile)
        {
            String jsonContent = File.ReadAllText(jsonFile);
            JObject json = JObject.Parse(jsonContent);


            foreach (JProperty property in json.Properties())
            {
                String csvItem = jsonFile;
                //string csvHead = "jsonFile";

                JObject fItem = JObject.Parse(property.Value.ToString());

                foreach (JProperty fProp in fItem.Properties())
                {
                    if (fProp.Name != "session")
                    {
                        csvItem += "," + fProp.Value;
                        //csvHead += "," + fProp.Name;
                    }
                }

                File.AppendAllText(csvFile, csvItem + Environment.NewLine);
            }

            return true;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            this.btnExport.Enabled = false;
            this.expStat.Text = String.Format("Loading");
            this.expStat.Refresh();
            int i = 0;
            string[] dirs = Directory.GetFiles(this.dirFrom.Text, "*.*");
            string fCsvFile = this.fileTo.Text;
            if (File.Exists(fCsvFile))
            {
                File.Delete(fCsvFile);
            }

            File.WriteAllText(fCsvFile, "jsonFile,guid,rev,date,ident,mb,firstname,description,status,familyname,cid,gid,birthstate,birthplace,birthdate,state,city,address,telephone,votesecure,voteplace,votemember,leadname,leadcontact,pguid,votestatus");
            File.AppendAllText(fCsvFile, Environment.NewLine);
            foreach (string dir in dirs)
            {
                Console.WriteLine(dir);

                jsonToCsv(dir, fCsvFile);
                this.expStat.Text = String.Format("{0}/{1}", (++i), dir.Length);
                this.expStat.Refresh();
            }
            this.expStat.Text = String.Format("End");
            this.expStat.Refresh();
            this.btnExport.Enabled = true;
        }
    }
}
