{
	"guid": "{guid}",
	"rev": 1,
	"date": "2022-10-12T00:29:58.475Z",
	"session": {
		"SessionUID": "c74113be-3112-4902-90d2-f53ea2d705ea",
		"CookieUID": "847070a2-4176-49af-934c-85ce60209662",
		"UserUID": "d4d4ba6d131c4274a56eb23053011ad1",
		"UserName": "master",
		"UserEmail": "master@dd.in.rs"
	},
	"title": "{title}",
	"committee": "Bar",
	"municipal": "Bar",
	"city": "Bar",
	"address": "",
	"moderators": ["{moderator}"],
	"went_count": 0,
	"went_max": {went_max},
	"went_count_secure": 0,
	"went_max_secure": {went_max_secure},
	"went_count_abstinent": 0,
	"went_max_abstinent": 0,
	"item_count": 1,
	"geo_lat": {lat},
	"geo_lng": {lng},
	"description": "",
	"visibility": "open"
}