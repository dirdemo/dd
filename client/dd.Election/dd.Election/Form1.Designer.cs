﻿namespace dd.Election
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.dirTo = new System.Windows.Forms.TextBox();
            this.fileFrom = new System.Windows.Forms.TextBox();
            this.lblStat = new System.Windows.Forms.Label();
            this.bW = new System.ComponentModel.BackgroundWorker();
            this.dirFrom = new System.Windows.Forms.TextBox();
            this.fileTo = new System.Windows.Forms.TextBox();
            this.expStat = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.bWExport = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(13, 73);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(186, 23);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Convert";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // dirTo
            // 
            this.dirTo.Location = new System.Drawing.Point(13, 47);
            this.dirTo.Name = "dirTo";
            this.dirTo.Size = new System.Drawing.Size(403, 20);
            this.dirTo.TabIndex = 1;
            this.dirTo.Text = "ext";
            // 
            // fileFrom
            // 
            this.fileFrom.Location = new System.Drawing.Point(13, 13);
            this.fileFrom.Name = "fileFrom";
            this.fileFrom.Size = new System.Drawing.Size(403, 20);
            this.fileFrom.TabIndex = 2;
            this.fileFrom.Text = "load.csv";
            // 
            // lblStat
            // 
            this.lblStat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStat.Location = new System.Drawing.Point(314, 70);
            this.lblStat.Name = "lblStat";
            this.lblStat.Size = new System.Drawing.Size(100, 23);
            this.lblStat.TabIndex = 3;
            this.lblStat.Text = "?/?";
            this.lblStat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bW
            // 
            this.bW.WorkerReportsProgress = true;
            this.bW.WorkerSupportsCancellation = true;
            this.bW.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bW_DoWork);
            this.bW.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bW_ProgressChanged);
            this.bW.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bW_RunWorkerCompleted);
            // 
            // dirFrom
            // 
            this.dirFrom.Location = new System.Drawing.Point(13, 129);
            this.dirFrom.Name = "dirFrom";
            this.dirFrom.Size = new System.Drawing.Size(403, 20);
            this.dirFrom.TabIndex = 4;
            this.dirFrom.Text = "imp";
            // 
            // fileTo
            // 
            this.fileTo.Location = new System.Drawing.Point(13, 156);
            this.fileTo.Name = "fileTo";
            this.fileTo.Size = new System.Drawing.Size(403, 20);
            this.fileTo.TabIndex = 5;
            this.fileTo.Text = "zaexcel.csv";
            // 
            // expStat
            // 
            this.expStat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.expStat.Location = new System.Drawing.Point(314, 179);
            this.expStat.Name = "expStat";
            this.expStat.Size = new System.Drawing.Size(100, 23);
            this.expStat.TabIndex = 7;
            this.expStat.Text = "?/?";
            this.expStat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(13, 182);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(186, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Convert";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // bWExport
            // 
            this.bWExport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bWExport_DoWork);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 233);
            this.Controls.Add(this.expStat);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.fileTo);
            this.Controls.Add(this.dirFrom);
            this.Controls.Add(this.lblStat);
            this.Controls.Add(this.fileFrom);
            this.Controls.Add(this.dirTo);
            this.Controls.Add(this.btnLoad);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox dirTo;
        private System.Windows.Forms.TextBox fileFrom;
        private System.Windows.Forms.Label lblStat;
        private System.ComponentModel.BackgroundWorker bW;
        private System.Windows.Forms.TextBox dirFrom;
        private System.Windows.Forms.TextBox fileTo;
        private System.Windows.Forms.Label expStat;
        private System.Windows.Forms.Button btnExport;
        private System.ComponentModel.BackgroundWorker bWExport;
    }
}

