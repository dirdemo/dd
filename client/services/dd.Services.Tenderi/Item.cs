﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dd.Services.Tenderi
{

    public class Item
    {
        public String KeyValue;
        public Dictionary<string, string> Items;

        public Item(String iKeyValue)
        {
            this.KeyValue = iKeyValue;
            this.Items = new Dictionary<string, string>();
        }

        public void Add(String iData)
        {
            this.Add(iData, "");
        }

        public void Add(String iData, String iText)
        {
            string fS = iData.ToString().Trim();
            if ((fS != "") && (!this.Items.ContainsKey(fS)))
            {
                String fData = iData;

                fData = fData.Replace("ctl00_cphMain_lbl", "");
                fData = fData.Replace("ctl00_cphMain_txt", "");                
                fData = fData.Replace("ctl00_cphMain_", "");
                fData = fData.Replace("ctl00_cphRight_lbl", "");
                fData = fData.Replace("Data", "");

                String fText = this.GetLat(iText);

                this.Items.Add(fData, fText);
            }
        }

        public string getJson()
        {
            string fJson = "{ \"key\":\"" + this.KeyValue + "\"";
            foreach (KeyValuePair<string, string> kvp in this.Items)
            {
                var fItems = this.GetLat(kvp.Value);

                using (StringReader reader = new StringReader(fItems))
                {
                    string line = string.Empty;
                    do
                    {
                        line = reader.ReadLine();
                        if (line != null)
                        {
                            string[] fItem = line.Split(':');
                            if (fItem.Length == 2)
                            {
                                fJson += ", \"" + fItem[0] + "\":\"" + fItem[1].ToString().Trim().Replace("\"", "") + "\"";
                            }
                        }

                    } while (line != null);
                }
            }
            fJson += "}";

            return fJson;
        }

        public string getKey()
        {
            string fKey = "";
            foreach (KeyValuePair<string, string> kvp in this.Items)
            {
                var fItems = kvp.Value;

                using (StringReader reader = new StringReader(fItems))
                {
                    string line = string.Empty;
                    do
                    {
                        line = reader.ReadLine();
                        if (line != null)
                        {
                            string[] fItem = line.Split(':');
                            if ((fItem.Length == 2) && (fItem[0].ToString().Trim() == "Седиште"))
                            {
                                fKey = this.GetLat(fItem[1].ToString().Trim()).Replace("\"", "").Replace(" · ", "-").Replace(" ", "-").Replace(@"\", "").Replace(@"/", "");

                                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                                {
                                    fKey = fKey.Replace(c, '_');
                                }
                            }
                        }

                    } while (line != null);
                }
            }

            return fKey;
        }
        public string getKeyId()
        {
            string fKey = "";

            return fKey;
        }
        public string getLink()
        {
            string fKey = "";

            return fKey;
        }
        
        public Dictionary<string, string> getItems(String iHtml)
        {
            Dictionary<string, string> fResult = new Dictionary<string, string>();

            using (StringReader reader = new StringReader(iHtml))
            {
                string line = string.Empty;
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                    {
                        string[] fItem = line.Split(':');
                        fResult.Add(fItem[0], fItem[1].ToString().Trim());
                    }

                } while (line != null);
            }

            return fResult;
        }

        public string GetLat(string str)
        {

            str = str.Replace("а", "a");
            str = str.Replace("А", "A");

            str = str.Replace("б", "b");
            str = str.Replace("Б", "B");

            str = str.Replace("в", "v");
            str = str.Replace("В", "V");

            str = str.Replace("г", "g");
            str = str.Replace("Г", "G");

            str = str.Replace("д", "d");
            str = str.Replace("Д", "D");

            str = str.Replace("ж", "ž");
            str = str.Replace("Ж", "Ž");

            str = str.Replace("з", "z");
            str = str.Replace("З", "Z");

            str = str.Replace("и", "i");
            str = str.Replace("И", "I");

            str = str.Replace("к", "k");
            str = str.Replace("К", "K");

            str = str.Replace("л", "l");
            str = str.Replace("Л", "L");

            str = str.Replace("м", "m");
            str = str.Replace("М", "M");

            str = str.Replace("н", "n");
            str = str.Replace("Н", "N");

            str = str.Replace("о", "o");
            str = str.Replace("О", "O");

            str = str.Replace("п", "p");
            str = str.Replace("П", "P");

            str = str.Replace("р", "r");
            str = str.Replace("Р", "R");

            str = str.Replace("с", "s");
            str = str.Replace("С", "S");

            str = str.Replace("ч", "č");
            str = str.Replace("Ч", "Č");

            str = str.Replace("ћ", "ć");
            str = str.Replace("Ћ", "Ć");

            str = str.Replace("ш", "š");
            str = str.Replace("Ш", "Š");

            str = str.Replace('т', 't');
            str = str.Replace("Т", "T");

            str = str.Replace('ц', 'c');
            str = str.Replace("Ц", "C");

            str = str.Replace('о', 'o');
            str = str.Replace("О", "O");

            str = str.Replace('е', 'e');
            str = str.Replace("Е", "E");

            str = str.Replace('ф', 'f');
            str = str.Replace("Ф", "F");

            str = str.Replace('У', 'U');
            str = str.Replace("у", "u");

            str = str.Replace('х', 'h');
            str = str.Replace("Х", "H");

            str = str.Replace("љ", "lj");
            str = str.Replace("Љ", "Lj");

            str = str.Replace("њ", "nj");
            str = str.Replace("Њ", "Nj");

            str = str.Replace("Ђ", "dj");
            str = str.Replace("Ђ", "Dj");

            str = str.Replace("џ", "dz");
            str = str.Replace("Џ", "Dz");
       
            return str;
        }

    }
}
