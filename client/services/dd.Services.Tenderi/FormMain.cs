﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Data.SqlClient;

namespace dd.Services.Tenderi
{
    public partial class FormMain : Form
    {
        private int SecondLeft = 0;

        public FormMain()
        {
            InitializeComponent();

            this.LogBox.Text += String.Format("Tenderi - {0} ({1})", Application.ProductName, Application.ProductVersion);
            this.LogBox.Text += Environment.NewLine + String.Format("URL: {0}", Properties.Settings.Default.ServiceURL);
            this.LogBox.Text += Environment.NewLine + String.Format("Proxy: {0}", Properties.Settings.Default.ProxyURL);
            this.LogBox.Text += Environment.NewLine + String.Format("Last id: {0}", Properties.Settings.Default.LastId);
            if (Properties.Settings.Default.Active)
            {
                this.ShowInTaskbar = false;
                this.WindowState = FormWindowState.Minimized;
                this.Hide();
                this.notifyIcon.Visible = true;
            }
        }
        public void grab(TextBox Log = null)
        {
            try
            {
                this.cmdOnce.Text = "stop";
                Application.DoEvents();
                Graber fGraber = new Graber(Properties.Settings.Default.ProxyURL);
                List<Item> fItems = fGraber.Start(Properties.Settings.Default.LastId);
                Properties.Settings.Default.LastId = fGraber.LastId;
                fGraber.Destroy();

                Log.Text += Environment.NewLine;
                Log.Text += Environment.NewLine + String.Format("New items: {0}", fItems.Count);

                if (0 < fItems.Count)
                {
                    var fJsonItems = Newtonsoft.Json.JsonConvert.SerializeObject(fItems);
                    this.setRequest("action=tenderadd", fJsonItems);

                    if (!String.IsNullOrEmpty(Properties.Settings.Default.ZIPStr))
                    {
                        ZipArchive fZipFile = ZipFile.Open(Properties.Settings.Default.ZIPStr, ZipArchiveMode.Update);

                        foreach (Item fItem in fItems)
                        {
                            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(fItem);
                            var fFileName = "t" + fItem.KeyValue + ".json";
                            System.IO.File.WriteAllText(fFileName, jsonString);
                            fZipFile.CreateEntryFromFile(fFileName, fFileName, CompressionLevel.Optimal);
                            System.IO.File.Delete(fFileName);
                        }

                        fZipFile.Dispose();
                    }

                    if (!String.IsNullOrEmpty(Properties.Settings.Default.DBStr))
                    {
                        using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DBStr))
                        {
                            connection.Open();

                            foreach (Item fItem in fItems)
                                try
                                {

                                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(fItem.Items);

                                    using (SqlCommand command = new SqlCommand(Properties.Settings.Default.DBIns, connection))
                                    {
                                        command.Parameters.Add(new SqlParameter("@Id", fItem.KeyValue));
                                        //command.Parameters.Add(new SqlParameter("@MB", ""));
                                        command.Parameters.Add(new SqlParameter("@data", jsonString));

                                        command.Parameters.Add(new SqlParameter("@IdDokumenta", fItem.Items["IdDokumenta"]));
                                        command.Parameters.Add(new SqlParameter("@DatumKreiranja", fItem.Items["DatumKreiranja"]));
                                        command.Parameters.Add(new SqlParameter("@DatumIzmene", fItem.Items["DatumIzmene"]));
                                        command.Parameters.Add(new SqlParameter("@Naziv", fItem.Items["Naziv"]));
                                        command.Parameters.Add(new SqlParameter("@MB", fItem.Items["MB"]));
                                        command.Parameters.Add(new SqlParameter("@PIB", fItem.Items["PIB"]));
                                        command.Parameters.Add(new SqlParameter("@Drzava", fItem.Items["Drzava"]));
                                        command.Parameters.Add(new SqlParameter("@Opstina", fItem.Items["Opstina"]));
                                        command.Parameters.Add(new SqlParameter("@Mesto", fItem.Items["Mesto"]));
                                        command.Parameters.Add(new SqlParameter("@Delatnost", fItem.Items["Delatnost"]));
                                        command.Parameters.Add(new SqlParameter("@OblikSvojine", fItem.Items["OblikSvojine"]));
                                        command.Parameters.Add(new SqlParameter("@OblikOrganizovanja", fItem.Items["OblikOrganizovanja"]));

                                        command.Parameters.Add(new SqlParameter("@Kategorija", fItem.Items["Kategorija"]));
                                        command.Parameters.Add(new SqlParameter("@KontaktOsoba", fItem.Items["KontaktOsoba"]));
                                        command.Parameters.Add(new SqlParameter("@KontaktTel", fItem.Items["KontaktTel"]));
                                        command.Parameters.Add(new SqlParameter("@PrethodnaObavestenjaVrednost", fItem.Items["PrethodnaObavestenjaVrednost"]));
                                        command.Parameters.Add(new SqlParameter("@ZakonVerzijaVrednost", fItem.Items["ZakonVerzijaVrednost"]));
                                        command.Parameters.Add(new SqlParameter("@NazivDokumenta", fItem.Items["NazivDokumenta"]));
                                        command.Parameters.Add(new SqlParameter("@CPV", fItem.Items["CPV"]));
                                        command.Parameters.Add(new SqlParameter("@PredmetVrednost", fItem.Items["PredmetVrednost"]));

                                        command.ExecuteNonQuery();
                                    }
                                }
                                catch (System.Exception ex)
                                {
                                    this.LogBox.Text += Environment.NewLine + ex.Message;
                                }

                            connection.Close();
                        }
                    }
                }

                Properties.Settings.Default.Save();
            }
            catch (System.Exception ex)
            {
                this.LogBox.Text += Environment.NewLine + ex.Message;
            }
            this.cmdOnce.Text = "Start";

            if (Properties.Settings.Default.ActiveClose && Properties.Settings.Default.Active)
            {
                Application.Exit();
            }
        }
        private String setRequest(String iParams, String iPost)
        {
            String fResult = "";

            try
            {
                using (WebClient fHttp = new WebClient())
                {
                    fHttp.Encoding = Encoding.UTF8;
                    String fCallURL = String.Format("{0}?{1}", Properties.Settings.Default.ServiceURL, iParams);
                    fHttp.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    fResult = fHttp.UploadString(fCallURL, iPost);
                }
            }
            catch (System.Exception ex)
            {
                //MessageBox.Show(ex.Message);
                this.LogBox.Text += ex.Message + Environment.NewLine;
            }
            finally
            {
                //this.HistoryAdd("setRequest", iParams, iPost, fResult);
            }

            return fResult;
        }
        private void cmdOnce_Click(object sender, EventArgs e)
        {
            grab(this.LogBox);
        }
        private void doResetLastId_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.LastId = "";
            Properties.Settings.Default.Save();
        }
        private void isActive_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Active = this.isActive.Checked;
            Properties.Settings.Default.Save();
            if (this.isActive.Checked)
            {
                this.SecondLeft = Properties.Settings.Default.ActiveSec;
                this.timerGrab.Interval = 1000;
                this.timerGrab.Enabled = true;
                this.cmdOnce.Text = String.Format("Start {0}", this.SecondLeft);
            }
            else
            {
                this.timerGrab.Enabled = false;
                this.cmdOnce.Text = String.Format("Start", this.SecondLeft);
            }
        }
        private void timerGrab_Tick(object sender, EventArgs e)
        {
            if (!this.isActive.Checked)
            {
                this.cmdOnce.Text = String.Format("Start", this.SecondLeft);
                this.timerGrab.Enabled = false;

                return;
            }
            this.cmdOnce.Text = String.Format("Start {0}", this.SecondLeft);
            if (this.SecondLeft < 1)
            {
                this.timerGrab.Enabled = false;
                this.notifyIcon_Show("Tenderi starting");
                this.grab(this.LogBox);
            }
            this.SecondLeft--;
        }
        private void FormMain_Shown(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.Active)
            {
                this.isActive.Checked = Properties.Settings.Default.Active;
                this.SecondLeft = Properties.Settings.Default.ActiveSec;
                this.timerGrab.Interval = 1000;
                this.timerGrab.Enabled = true;
            }
        }
        private void notifyIcon_Click(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.Show();
            this.notifyIcon.Visible = true;
        }
        private void notifyIcon_Show(String iMsg)
        {
            this.notifyIcon.ShowBalloonTip(1000, "Info", iMsg, ToolTipIcon.Info);
        }
    }
}
