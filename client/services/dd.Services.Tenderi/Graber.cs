﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace dd.Services.Tenderi
{
    public class Graber
    {
        public String LastId = "";
        public IWebDriver driver;

        public Graber(string iProxy)
        {
            ChromeOptions options = new ChromeOptions();

            if (iProxy != "")
            {
                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = iProxy;
                options.Proxy = proxy;
            }

            if (Properties.Settings.Default.Arguments != "")
            {
                foreach (String fParam in Properties.Settings.Default.Arguments.Split(','))
                {
                    options.AddArgument(fParam);
                }
            }

            this.driver = new ChromeDriver(options);
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed && element.Enabled;
        }

        public string getValue(String iId)
        {
            return this.getValue(iId, String.Empty);
        }

        public string getValue(String iId, String iDeafult)
        {
            string fResult = iDeafult;

            try
            {
                IWebElement fElement = this.driver.FindElements(By.Id(iId))[0];
                fResult = fElement.Text;
            }
            catch (Exception ex)
            {

            }

            return fResult;
        }

        public List<Item> Start(string iLastId = "")
        {
            string fValue = iLastId.ToString();
            List<Item> fResults = new List<Item>();
            int fPos = 2;
            int fPosMax = 17;
            this.driver.Navigate().GoToUrl(Properties.Settings.Default.URL);
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            this.driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            for (int i = fPos; i < fPosMax; i++)
            {
                String fButtonId = "ctl00_cphMain_grvPoslednjihSto_ctl" + i.ToString().PadLeft(2, '0') + "_btnPregled";

                IWebElement fButton = this.driver.FindElements(By.Id(fButtonId))[0];
                fButton.Click();
                this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                this.driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(15);
                Thread.Sleep(1000);

                string fId = this.getValue("ctl00_cphMain_IdDokumentaData");
                if (!String.IsNullOrEmpty(fId))
                {
                    if (i == fPos)
                    {
                        this.LastId = fId;
                    }

                    if (fId == iLastId)
                    {
                        i = fPosMax;
                        continue;
                    }

                    Item fItem = new Item(fId);

                    fItem.Add("ctl00_cphMain_IdDokumentaData", this.getValue("ctl00_cphMain_IdDokumentaData"));

                    fItem.Add("ctl00_cphMain_lblDatumKreiranja", this.getValue("ctl00_cphMain_lblDatumKreiranja"));
                    fItem.Add("ctl00_cphMain_lblDatumIzmene", this.getValue("ctl00_cphMain_lblDatumIzmene"));

                    fItem.Add("ctl00_cphRight_lblNazivData", this.getValue("ctl00_cphRight_lblNazivData"));
                    fItem.Add("ctl00_cphRight_lblMBData", this.getValue("ctl00_cphRight_lblMBData"));
                    fItem.Add("ctl00_cphRight_lblPIBData", this.getValue("ctl00_cphRight_lblPIBData"));

                    fItem.Add("ctl00_cphRight_lblDrzavaData", this.getValue("ctl00_cphRight_lblDrzavaData"));
                    fItem.Add("ctl00_cphRight_lblOpstinaData", this.getValue("ctl00_cphRight_lblOpstinaData"));
                    fItem.Add("ctl00_cphRight_lblMestoData", this.getValue("ctl00_cphRight_lblMestoData"));

                    fItem.Add("ctl00_cphRight_lblDelatnostData", this.getValue("ctl00_cphRight_lblDelatnostData"));
                    fItem.Add("ctl00_cphRight_lblOblikSvojineData", this.getValue("ctl00_cphRight_lblOblikSvojineData"));
                    fItem.Add("ctl00_cphRight_lblOblikOrganizovanjaData", this.getValue("ctl00_cphRight_lblOblikOrganizovanjaData"));

                    fItem.Add("ctl00_cphRight_lblKategorijaData", this.getValue("ctl00_cphRight_lblKategorijaData"));
                    fItem.Add("ctl00_cphRight_lblKontaktOsobaData", this.getValue("ctl00_cphRight_lblKontaktOsobaData"));
                    fItem.Add("ctl00_cphRight_lblKontaktTelData", this.getValue("ctl00_cphRight_lblKontaktTelData"));

                    fItem.Add("ctl00_cphMain_lblPrethodnaObavestenjaVrednost", this.getValue("ctl00_cphMain_lblPrethodnaObavestenjaVrednost"));
                    fItem.Add("ctl00_cphMain_lblZakonVerzijaVrednost", this.getValue("ctl00_cphMain_lblZakonVerzijaVrednost"));
                    fItem.Add("ctl00_cphMain_txtNazivDokumenta", this.getValue("ctl00_cphMain_txtNazivDokumenta"));

                    fItem.Add("ctl00_cphMain_txtCPV", this.getValue("ctl00_cphMain_txtCPV"));
                    fItem.Add("ctl00_cphMain_lblPredmetVrednost", this.getValue("ctl00_cphMain_lblPredmetVrednost"));



                    fResults.Add(fItem);
                }
                this.driver.Navigate().Back();
                /*if (fId == iLastId)
                {
                    i = fPosMax;
                    continue;
                }*/
            }

            return fResults;
        }

        System.Windows.Forms.Timer timerShutdown;

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            this.timerShutdown.Stop();
            this.Destroy();
        }

        public void Shutdown()
        {
            this.timerShutdown = new System.Windows.Forms.Timer();
            this.timerShutdown.Interval = 100;
            this.timerShutdown.Tick += new EventHandler(TimerEventProcessor);
            this.timerShutdown.Start();
        }

        public void Destroy(bool iDoClosing = false)
        {
            if (iDoClosing)
                foreach (String fHandle in this.driver.WindowHandles)
                {
                    this.driver.SwitchTo().Window(fHandle);
                    this.driver.Close();
                }

            this.driver.Quit();
            this.driver = null;

            try
            {
                string tempfolder = System.IO.Path.GetTempPath();
                string[] tempfiles = Directory.GetDirectories(tempfolder, "scoped_dir*", SearchOption.AllDirectories);
                foreach (string tempfile in tempfiles)
                {
                    try
                    {
                        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(tempfolder);
                        foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
                    }
                    catch (System.Exception ex)
                    {
                        //Console.WriteLine("error {0}", ex);
                    }
                }
            }
            catch
            {

            }
        }

        public void Dispose()
        {
            this.Destroy();
        }
    }
}
