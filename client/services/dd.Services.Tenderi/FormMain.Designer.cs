﻿namespace dd.Services.Tenderi
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.isActive = new System.Windows.Forms.CheckBox();
            this.cmdOnce = new System.Windows.Forms.Button();
            this.LogBox = new System.Windows.Forms.TextBox();
            this.ResetMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.doResetLastId = new System.Windows.Forms.ToolStripMenuItem();
            this.timerGrab = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.ResetMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // isActive
            // 
            this.isActive.AutoSize = true;
            this.isActive.Location = new System.Drawing.Point(94, 11);
            this.isActive.Name = "isActive";
            this.isActive.Size = new System.Drawing.Size(56, 17);
            this.isActive.TabIndex = 0;
            this.isActive.Text = "Active";
            this.isActive.UseVisualStyleBackColor = true;
            this.isActive.Click += new System.EventHandler(this.isActive_Click);
            // 
            // cmdOnce
            // 
            this.cmdOnce.Location = new System.Drawing.Point(13, 7);
            this.cmdOnce.Name = "cmdOnce";
            this.cmdOnce.Size = new System.Drawing.Size(75, 23);
            this.cmdOnce.TabIndex = 1;
            this.cmdOnce.Text = "Start";
            this.cmdOnce.UseVisualStyleBackColor = true;
            this.cmdOnce.Click += new System.EventHandler(this.cmdOnce_Click);
            // 
            // LogBox
            // 
            this.LogBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LogBox.ContextMenuStrip = this.ResetMain;
            this.LogBox.Location = new System.Drawing.Point(13, 36);
            this.LogBox.Multiline = true;
            this.LogBox.Name = "LogBox";
            this.LogBox.ReadOnly = true;
            this.LogBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogBox.Size = new System.Drawing.Size(684, 142);
            this.LogBox.TabIndex = 3;
            // 
            // ResetMain
            // 
            this.ResetMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doResetLastId});
            this.ResetMain.Name = "contextMenuStrip1";
            this.ResetMain.Size = new System.Drawing.Size(103, 26);
            this.ResetMain.Text = "Reset";
            // 
            // doResetLastId
            // 
            this.doResetLastId.Name = "doResetLastId";
            this.doResetLastId.Size = new System.Drawing.Size(102, 22);
            this.doResetLastId.Text = "Reset";
            this.doResetLastId.Click += new System.EventHandler(this.doResetLastId_Click);
            // 
            // timerGrab
            // 
            this.timerGrab.Interval = 1000;
            this.timerGrab.Tick += new System.EventHandler(this.timerGrab_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Visible = true;
            this.notifyIcon.Click += new System.EventHandler(this.notifyIcon_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 190);
            this.ContextMenuStrip = this.ResetMain;
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.cmdOnce);
            this.Controls.Add(this.isActive);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Service Tenderi";
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.ResetMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox isActive;
        private System.Windows.Forms.Button cmdOnce;
        private System.Windows.Forms.TextBox LogBox;
        private System.Windows.Forms.ContextMenuStrip ResetMain;
        private System.Windows.Forms.ToolStripMenuItem doResetLastId;
        private System.Windows.Forms.Timer timerGrab;
        private System.Windows.Forms.NotifyIcon notifyIcon;
    }
}

