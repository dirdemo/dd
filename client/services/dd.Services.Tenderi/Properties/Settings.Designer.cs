﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dd.Services.Tenderi.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string ProxyURL {
            get {
                return ((string)(this["ProxyURL"]));
            }
            set {
                this["ProxyURL"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://portal.ujn.gov.rs/Pretraga.aspx?tab=3")]
        public string URL {
            get {
                return ((string)(this["URL"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LastId {
            get {
                return ((string)(this["LastId"]));
            }
            set {
                this["LastId"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.1.4:1024/api")]
        public string ServiceURL {
            get {
                return ((string)(this["ServiceURL"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Active {
            get {
                return ((bool)(this["Active"]));
            }
            set {
                this["Active"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public int ActiveSec {
            get {
                return ((int)(this["ActiveSec"]));
            }
            set {
                this["ActiveSec"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ActiveClose {
            get {
                return ((bool)(this["ActiveClose"]));
            }
            set {
                this["ActiveClose"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Server=W7NET-PC\\MSSQL2014;Database=tmp;Trusted_Connection=True;")]
        public string DBStr {
            get {
                return ((string)(this["DBStr"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string ZIPStr {
            get {
                return ((string)(this["ZIPStr"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"IF(EXISTS(SELECT * FROM rs_tenderi WHERE Id=@Id))
	UPDATE rs_tenderi SET 
		updated=GETDATE(), 
		data=@data,
		IdDokumenta = @IdDokumenta,
		DatumKreiranja = @DatumKreiranja,
		DatumIzmene = @DatumIzmene,
		Naziv = @Naziv,
		MB = @MB,
		PIB = @PIB,
		Drzava = @Drzava,
		Opstina = @Opstina,
		Mesto = @Mesto,
		Delatnost = @Delatnost,
		OblikSvojine = @OblikSvojine,
		OblikOrganizovanja = @OblikOrganizovanja,
		Kategorija = @Kategorija,
		KontaktOsoba = @KontaktOsoba,
		KontaktTel = @KontaktTel,
		PrethodnaObavestenjaVrednost = @PrethodnaObavestenjaVrednost,
		ZakonVerzijaVrednost = @ZakonVerzijaVrednost,
		NazivDokumenta = @NazivDokumenta,
		CPV = @CPV,
		PredmetVrednost = @PredmetVrednost
	WHERE Id=@Id
ELSE
	INSERT INTO rs_tenderi(
		Id, MB, data, created, updated,
		IdDokumenta,DatumKreiranja,DatumIzmene,Naziv,PIB,Drzava,Opstina,Mesto,Delatnost,OblikSvojine,OblikOrganizovanja,Kategorija,KontaktOsoba,KontaktTel,PrethodnaObavestenjaVrednost,ZakonVerzijaVrednost,NazivDokumenta,CPV,PredmetVrednost
	)VALUES(
		@Id, NULL, @data, GETDATE(), GETDATE(),
		@IdDokumenta,@DatumKreiranja,@DatumIzmene,@Naziv,@PIB,@Drzava,@Opstina,@Mesto,@Delatnost,@OblikSvojine,@OblikOrganizovanja,@Kategorija,@KontaktOsoba,@KontaktTel,@PrethodnaObavestenjaVrednost,@ZakonVerzijaVrednost,@NazivDokumenta,@CPV,@PredmetVrednost
	)


")]
        public string DBIns {
            get {
                return ((string)(this["DBIns"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("headless,ignore-certificate-errors,--incognito")]
        public string Arguments {
            get {
                return ((string)(this["Arguments"]));
            }
        }
    }
}
