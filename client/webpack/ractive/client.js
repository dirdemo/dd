var mergeArray = function (iA, iB) {
  var fResult = iA;
  try {
    fResult = Object.assign(iA, iB);
  } catch (err) {
    for (var key in iB) {
      iA[key] = iB[key];
    }
  }

  return fResult;
}
var getMerge = function (iA1, iA2, iA3) {
  var fResult = {};

  if (typeof (iA1) == 'object') {
    fResult = mergeArray(fResult, iA1);
  }

  if (typeof (iA2) == 'object') {
    fResult = mergeArray(fResult, iA2);
  }

  if (typeof (iA3) == 'object') {
    fResult = mergeArray(fResult, iA3);
  }

  return fResult;
}
var isEcho = function (iLevel) {
  if ((Global) && (Global.Behavior) && (Global.Behavior.echoTest)) {
    if ((Global) && (Global.Behavior) && (Global.Behavior.echoLevel) && (iLevel) && (Global.Behavior.echoLevel <= iLevel)) {
      return false;
    }
    return true;
  }

  return false;
}
var setDisplay = function (iViewName, iBehavior, iCallback) {
  var fElement;
  var fDisplay;

  if ((typeof (iViewName) == "object") && (iViewName.Frame)) {
    fDisplay = Ractive.components[iViewName.Frame];
    fElement = { el: iViewName.Id };
  } else {
    fElement = { el: 'body' };
    if ((Global) && (Global.Config) && (Global.Config.DisplayId)) {
      fElement = { el: Global.Config.DisplayId };
    }
    fDisplay = Ractive.components[iViewName];
  }

  if (isEcho()) console.log('Display', iViewName, iBehavior, fElement);
  if ((typeof (fDisplay) == "undefined") || (!fDisplay)) {
    console.error('Error: Component not found!');
    return;
  }
  //Global.Display.Root = new fDisplay(fElement);
  Global.Display.Frames.push(new fDisplay(fElement));

  if (iCallback) iCallback();
  if (Global.Config.onFormLoad) Global.Config.onFormLoad();
}
var getClone = function (obj) {
  if (obj&&Array.isArray(obj)){
    return obj.slice();
  }
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}
var getGuid = function () {
  var d = new Date().getTime();
  if (window.performance && typeof window.performance.now === "function") {
    d += performance.now(); //use high-precision timer if available
  }
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}
var getShort = function (iLength) {
  if (!iLength) iLength = 4;
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < iLength; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
var getSession = function () {
  var fResult = { Status: false, Id: 0, UID: '', User: null, Session: null };

  var fResult = { Status: false, Id: 0, UID: '', User: null, Session: null };
  /* if (sessionStorage.getItem('Session')) {
    fSession = JSON.parse(sessionStorage.getItem('Session'));
    fResult.Status = true;
    fResult.UID = fSession.SessionUID;
    fResult.Session = fSession;
  } */

  if (sessionStorage.getItem('User')) {
    fResult.User = JSON.parse(sessionStorage.getItem('User'));
  }

  return fResult;
}
var getSessionShort= function () {
  var fResult = {
    SessionUID: Global.Session.SessionUID,
    UserEmail: Global.Session.UserEmail,
    UserName: Global.Session.UserName,
    UserUID: Global.Session.UserUID,
    UserGeo: Global.Session.UserGeo
  };

  return fResult;
}
var getArray = function (iValue) {
  var fResult = [];

  if (Array.isArray(iValue)) {
    return iValue
  }
  if (typeof (iValue) == 'string') {
    return iValue;
  }
  if (typeof (iValue) == 'object') {
    var fResult = [];
    for (var key in iValue) {
      fResult.push(iValue[key]);
    }
    return fResult;
  }

  return [];
}
var getObject = function (iValue, iKey) {

  if (Array.isArray(iValue)) {
    var fResult = {};

    for (i = 0; i < iValue.length; i++) {
      var fKey = iValue[i][iKey];
      fResult[fKey] = iValue[i];
    }

    return fResult;
  }

  return iValue;
}
var getSortBy = function (iData, field, reverse, primer) {

  var fData = [];
  var fKeepProp = false;
  if (Array.isArray(iData)) {
    fData = iData;
  } else {
    for (var key in iData) {
      fData.push(iData[key]);
      fKeepProp = true;
    }
  }

  reverse = !reverse ? 1 : -1;
  var key = primer ?
    function (x) { return primer(x[field]) } :
    function (x) { return x[field] };

  fData.sort(function (a, b) {
    var a = key(a);//.ChangeDate;
    var b = key(b);//.ChangeDate;
    return reverse * ((a > b) - (b > a));
  });

  if (fKeepProp) {
    var fTemp = {};
    for (i = 0; i < fData.length; i++) {
      //fTemp[fData[i].PageUID] = fData[i]; 
    }

    //fData = fTemp;
  }

  return fData;
}
var setUrlChange = function() {
  if ("onhashchange" in window) {
    window.onhashchange = setUrl
  } else {
    window.setInterval(setUrl, 500);
  };
}
var setUrl = function (value, iCallback) {
  if (Global.Config.onFormLoad) {
    Global.Config.onFormLoad();
  }
  //if (isEcho(9)) console.log('setUrl', value);

  if (!value || (value&&typeof(value)==='object')) {
    value = window.location.hash;
  }
  var fUrl = getUrlData(value);
  //console.log('URL', fUrl);

  if((fUrl.Page)&&(Display = Ractive.components[fUrl.Page.toLowerCase()])){
      Global.Display.Current = new Display({ el: 'List' });
  }

  if(Global.Display.Frames){
    /*Global.Display.Frames.forEach(fItem => {
      if(fItem) fItem.fire('doUrl', fUrl);
    });*/
    Global.Display.Frames.forEach(function(fItem){
      if(fItem) fItem.fire('doUrl', fUrl);
    });
  }

  //if (Display = Ractive.components['Dashboard']) {
  //  Global.Display.Current = new Display({ el: 'List' });
  //}

  if (iCallback) iCallback();
  if (typeof (runDesign) === "function") runDesign();
}
var getUrlData = function (value) {
  var fUrlData = {
    page: "",
    guid: "",
    data: {},
    hash: ((value)?value:window.location.hash),
    command: "",
    params: {}
  };
  fUrlData.hash = fUrlData.hash.replace('#', '');

  if (fUrlData.hash) {
    var fSpl = fUrlData.hash.split('/');

    fUrlData.guid = fSpl[0];
    fUrlData.command = fSpl[1];
    
    if (((fStartParams = fUrlData.hash.indexOf("?")) != -1)&&(fStartParams)) {
      var fReturn = atob(fUrlData.hash.substring((fStartParams + 1)));
      try{
        fUrlData.params = JSON.parse(fReturn);
      }
      catch(err){
        console.log('Error', err);
      }
    }
  }

  return fUrlData;
}
var ioServer = function (iConfig) {
  var fConfig = (iConfig) ? iConfig : {};
  //var self = this;
  this.ioloadedallready = false;
  this.receiveHandlers = [];
  this.userslistHandlers = [];
  this.listentogroup = null;
  this.Session = fConfig.Session;
  this.urlServer = (fConfig.Server&&fConfig.Server.URI) ? fConfig.Server.URI : '';
  this.iosocket = io.connect(this.urlServer, {query: 'Client='+btoa(unescape(encodeURIComponent(JSON.stringify(this.Session))))});

  this.iosocket.on('connect', function () {
    if (isEcho()) console.log('You are online');
    Global.Server.Status = true;
    if (Global.Display.Root) {
      Global.Display.Root.fire('cmdEvent', 'connect', 'system');
    }

    this.on('refresh', function (iMessage, iFrom) {
      if (isEcho())  console.log('refresh', iMessage, iFrom);
      if (Global.Display.Current) {
        Global.Display.Current.fire('cmdRefresh', iMessage, iFrom);
      }
    });
    this.on('auth', function (iSession, iSockets) {
      if(isEcho())  console.log('Auth', iSession, iSockets);
      console.log('Echo Auth', iSession, iSockets);
      Global.Session.UserUID = iSession.session.UserUID;
      Global.Session.UserName = iSession.session.UserName;
      Global.Session.UserData = iSession.session.UserData;
      Global.Session.UserEmail = iSession.session.UserEmail;
      Global.Session.UserAdmin = iSession.session.UserAdmin;

      if(iSession.session.UserAuth){
        Global.Session.UserAuth = true;
      }else{
        Global.Session.UserAuth = false;
      }
      Client.setCache('Session', Global.Session);
      for(i=0; i<Global.Display.Frames.length; i++){
        try{
          Global.Display.Frames[i].fire('doEvent', 'Auth', iSession);
        }catch(err){
          console.log(err);
        }
      }
      if(Global.Display.Callbacks&&Global.Display.Callbacks.Events){
        for(var fEvent in Global.Display.Callbacks.Events){
          try{
            Global.Display.Callbacks.Events[fEvent].fire('doEvent', 'Auth', iSession);
          }catch(err){
            console.log(err);
            delete(Global.Display.Callbacks.Events[fEvent]);
          }
        }
      }
    });
    this.on('event', function (iMessage, iData, iFrom, iTo) {
      if(isEcho())  console.log('Event', iMessage, iData, iFrom, iTo);
      switch (iMessage) {
        case "News.New":
        //toastr.info(iData.title);
        break;
      }

      try{
        if (Global.Display.Root)  Global.Display.Root.fire('doEvent', iMessage, iData, iFrom, iTo);
      }catch(err){
        console.log(err);
      }
      try{
        if (Global.Display.Current) Global.Display.Current.fire('doEvent', iMessage, iData, iFrom, iTo);
      }catch(err){
        console.log(err);
      }
      for(i=0; i<Global.Display.Frames.length; i++){
        try{
          Global.Display.Frames[i].fire('doEvent', iMessage, iData, iFrom, iTo);
        }catch(err){
          console.log(err);
        }
      }
      if(Global.Display.Callbacks&&Global.Display.Callbacks.Events){
        for(var fEvent in Global.Display.Callbacks.Events){
          try{
            Global.Display.Callbacks.Events[fEvent].fire('doEvent', iMessage, iData, iFrom, iTo);
          }catch(err){
            console.log(err);
            delete(Global.Display.Callbacks.Events[fEvent]);
          }
        }
      }
    });
    this.on('message', function (message, from) {
      if (isEcho()) console.log('Message', message);
      /*for (var i = 0, len = self.receiveHandlers.length; i < len; i++) {
        var handler = self.receiveHandlers[i];
        setTimeout(handler(message, from), 0);
      } */
    });
    this.on('echo', function (iEcho) {
      if (isEcho()) console.log('Echo', iEcho);
    });
  });
  this.iosocket.on('connect_error', function (iError) {
    console.log('Failed to connect to server');
    Global.Server.Status = false;
    Global.Server.ErrLast = iError;
  });
  this.iosocket.on('disconnect', function () {
    Global.Server.Status = false;
    if (Global.Display.Root) {
      Global.Display.Root.fire('cmdEvent', 'disconnect', 'system');
    }
    console.log('Socket is disconnected.');
  });
}
var SyncNodeSrv = function (componentInstance, iServices, iCallback, iAction, iRow) {
  if(Array.isArray(iServices)){
    for (i=0; i<iServices.length; i++){
      //console.log(iServices[i], iCallback, iAction, iRow);
      //if (isEcho()) console.log('Sync Try:', componentInstance, iServices[i], iCallback, iAction, iRow);
      SyncNode(componentInstance, iServices[i], iCallback, iAction, iRow);
    }
  }else{
    if(typeof(iServices)=="object"){
      //console.log(typeof(iServices) );
      //if (isEcho()) console.log('Sync Try:', componentInstance, iServices, iCallback, iAction, iRow);
      SyncNode(componentInstance, iServices, iCallback, iAction, iRow);
    }
  }
}
var setAction = function(iSelf, iAction, iSrv, iLang, iRow) {
  if(!iSelf||!iAction||!iSrv){
    return false;
  }
  var fAction = (iAction||"");

  switch(fAction){
    case "doAdd":
      iSelf.sync.Sync(iSelf, iSelf.sync.EditRow(iSrv, iSelf.sync.NewRow({ }, iLang)));
    break;
    case "doEdit":
      iSelf.sync.Sync(iSelf, iSelf.sync.EditRow(iSrv, iRow, iSelf));
    break;
    case "doSave":
      iSelf.sync.Sync(iSelf, iSelf.sync.SaveRow(iSrv, iRow, iSelf));
    break;
    case "doDelete":
      iSelf.sync.Sync(iSelf, iSelf.sync.DeleteRow(iSrv, iRow, iSelf));
    break;
    case "doRefresh":
      iSelf.sync.Sync(iSelf, iSelf.sync.Refresh(iSrv, iSelf));
    break;
    case "doAdd":
      var fRow = iSelf.sync.NewRow({ }, iLang)
      iSelf.sync.Sync(iSelf, iSelf.sync.EditRow(iSrv, fRow));
    break;
    case "list":
      iSelf.sync.Sync(iSelf, iSelf.sync.Refresh(iSrv, iSelf));
    break;
    case "detail":
      var fRow = iSelf.sync.View(iSelf, iSrv, iRow);
      iSelf.sync.Sync(iSelf, iSelf.sync.DetailRow(iSrv, fRow));
    break;
  }

  return false;
}
var setSync = function (componentInstance, iServices, iAction, iRow, iCallback) {
  if(Array.isArray(iServices)){
    for (i=0; i<iServices.length; i++){
      if (isEcho(11)) console.log('setSync arr:', componentInstance, iServices[i], iCallback, iAction, iRow);
      SyncNode(componentInstance, iServices[i], iCallback, iAction, iRow);
    }
  }else{
    if(typeof(iServices)=="object"){
      if (isEcho(11)) console.log('setSync obj:', componentInstance, iServices, iCallback, iAction, iRow);
      SyncNode(componentInstance, iServices, iCallback, iAction, iRow);
    }
  }
}
var SyncNode = function (componentInstance, iService, iCallback, iAction, iRow) {
  var fMessage = getClone(iService);

  fMessage.UID = getGuid();
  fMessage.Name = fMessage.Name || "";
  fMessage.Action = fMessage.Action || iAction || "default";
  fMessage.Row = fMessage.Row || iRow || {};
  fMessage.Rows = fMessage.Rows || 'rows';
  fMessage.Behavior = fMessage.Behavior || {};
  fMessage.Session = getSession();

  if((fMessage.ModalRow)&&(fMessage.ModalName)&&(fMessage.Action)&&(fMessage.Row)){
    switch(fMessage.Action.toLowerCase()) {
      case "view":
        window.location.hash = fMessage.Row.guid;
        componentInstance.set(fMessage.DetailRow, fMessage.Row);
        return;
      break;
      case "edit":
        if(fMessage&&fMessage.ChangeUrl&&fMessage.ModalName){
          window.location.hash = fMessage.Row.guid+'/edit';
        }
        componentInstance.set(fMessage.ModalRow, fMessage.Row);
      
        if(fMessage.ModalName){
          $(fMessage.ModalName).modal('show');
        }
        return;
      break;
      case "insert":
      case "update":
      case "save":
        if(componentInstance&&iService&&iService.Behavior&&iService.Behavior.Validate){
          var fLng = componentInstance.get('Lng');
          var fFaildCount = 0;
          var fCheckCount = 0;
          if(fLng&&fLng[iService.Behavior.Validate]){
            for(var fKey in fLng[iService.Behavior.Validate]){
              if((fItem=fLng[iService.Behavior.Validate][fKey])&&(fRegEx=fItem.RegEx)&&(fRowValue=fMessage.Row[fKey])){
                if(isEcho(4)) console.log('Validate', fKey, fItem, fRegEx,fRowValue );
                var patt = new RegExp(fRegEx);
                var res = patt.test(fRowValue);
  
                if (!res){
                  fFaildCount++;
                  fCheckCount++;
                  fLng[iService.Behavior.Validate][fKey].ValidFaild = ((fLng[iService.Behavior.Validate][fKey].ValidFaild)? fLng[iService.Behavior.Validate][fKey].ValidFaild:1)+1;
                }else{
                  if(fLng[iService.Behavior.Validate][fKey].ValidFaild){
                    fCheckCount++;
                    fLng[iService.Behavior.Validate][fKey].ValidFaild = 0;
                  }
                }
              }
            }
          }
          if(fCheckCount){
            componentInstance.set('Lng', fLng);
          }
          if(fFaildCount){
            componentInstance.set('Lng', fLng);
            MessageShow("Proverite podatke", { type:"pop", timeOut: 5000 });
            return false;
          }
        }
      case "delete":
        if(fMessage.Row){
          fMessage.Row.session = {
            SessionUID: Global.Session.SessionUID,
            CookieUID: Global.Session.CookieUID,
            UserUID: Global.Session.UserUID,
            UserName: Global.Session.UserName,
            UserEmail: Global.Session.UserEmail
          }
          fMessage.Row.date = new Date();
        }
      break;
    }
  }

  fCallback = iCallback || SyncResult;
  if((!iCallback)&&(componentInstance)){
    Global.Callbacks[fMessage.UID] = componentInstance;

    if((iService)&&(iService.Behavior)&&(fLoading=iService.Behavior.Loading)){
      for(var fIdx in fLoading){
        componentInstance.set(fLoading[fIdx], true);
      }
    }
  }

  /* if((fMessage.Action == "default")&&(componentInstance)&&(fRows = getCache(iService))){
    if ((iService.Behavior) && (iService.Behavior.singleRow)) {
      var fRow = (fRows[0])? fRows[0] : {};

      componentInstance.set(iService.Rows, fRow);
    } else {
      componentInstance.set(fMessage.Rows, fRows);
    }
  } */

  //if (isEcho()) console.log('Sync Start: '+fMessage.Name, fMessage);
  try{
    $(".dd-fade").fadeOut(200);
    if(!iCallback){
      $('.dd-action').prop('disabled', true);
    }
    ServerIO.iosocket.emit('Sync', fMessage, fCallback);
    if(isEcho()) console.log('Sync start:', fMessage);
    if(fCallback&&iCallback){
      /*setTimeout(function(){
        $('.dd-action').prop('disabled', false);
      }, 800);*/
    }
  }catch(err){
    console.log("Error sync:", err);
  }
}
var SyncResult = function (componentInstance, iService, iData) {
  ///if (isEcho(8)) console.log('Sync End: '+iService.Name, iService, iData);
  if(isEcho()) console.log('Sync result:', iService, iData);
  $('.dd-action').prop('disabled', false);
  if((!componentInstance)&&(Global.Callbacks[iService.UID])){
    componentInstance = Global.Callbacks[iService.UID];
    delete(Global.Callbacks[iService.UID]);
    if((componentInstance)&&(iService)&&(iService.Behavior)&&(fLoading=iService.Behavior.Loading)){
      for(var fIdx in fLoading){
        componentInstance.set(fLoading[fIdx], false);
      }
    }
  } 
  var fData = iData;
  if ((iService.Behavior) && (iService.Behavior.singleRow)) {

    if ((iService.Behavior) && (iService.Behavior.Key)) {
      for (var key in iData.Rows) {
        fData = iData.Rows[key];
        break;
      }
    }else{
      if (0 < iData.Rows.length) {
        fData = iData.Rows[0];
      }
    }
  }

  if(componentInstance&&iService.Action=="detail"){
    if(iService.DetailRow){
      componentInstance.set(iService.DetailRow, fData);
    }
  }else{
    if(componentInstance&&iService.Rows){
      if((iService.Behavior) && (iService.Behavior.KeyName)) {
        if(iService.Behavior.KeyName=="array"){
          fData = getArray(fData);
          if(iService.Behavior.Sort){
            fData = getSortBy(fData, iService.Behavior.Sort, true);
          }
        }else{
          fData = getObject(fData, iService.Behavior.KeyName);
        }
      }

      componentInstance.set(iService.Rows, fData);
    }
  }
  if((iService.Name)&&(iService.Name.indexOf("CacheData")!=-1)){
    setCache(iService.Name, fData);
  }
  $(".dd-fade").fadeIn("slow");
  
  if(iService.Behavior){
    if((iService.Behavior.rootRefresh) && (Global.Display.Root)) {
      Global.Display.Root.fire('doRefresh');
    }
    if((iService.Behavior.frameRefresh) && (Global.Display.Current)) {
      Global.Display.Current.fire('doRefresh');
    }
  } 

  if((iService.ModalRow)&&(iService.ModalName)&&(iService.Action)&&(iService.Action!="detail")&&(iService.Row)&&(iService.Row.guid)){
    //window.location.hash = iService.Row.guid;
    if(iService.DetailRow&&componentInstance.get(iService.DetailRow)&&componentInstance.get(iService.DetailRow).guid){
      componentInstance.set(iService.DetailRow, Client.getClone(iService.Row));
    }
    if(iService.ModalName){
      $(iService.ModalName).modal('hide');
      //window.location.hash = iService.Row.guid;
      //window.history.back();
    }
  }
  if(componentInstance&&iService.FireName){
    componentInstance.fire(iService.FireName, iService, fData);
  }
  if(componentInstance&&iService.Behavior&&iService.Behavior.FireName){
    componentInstance.fire(iService.Behavior.FireName, iService, fData);
  }  
  //if(isEcho()) console.log('Sync result:', iService, fData);
}
var setCache = function (iService, iData) {
  var fCacheData = {};
  var key;

  if (localStorage.getItem('storageU')) {
    try {
      fCacheData = JSON.parse(localStorage.getItem('storageU'));
    } catch (ex) {
      localStorage.clear();
    }
  }

  if (iService.Name) key = iService.Name;
  if (typeof (iService) == "string") key = iService;
  //if ((iService.Behavior) && (iService.Behavior.CacheKey)) key = key + iService.Behavior.CacheKey;

  if ((key) && (key != "") && (iData)) {
    try
    {
      fCacheData[key] = iData;
      localStorage.setItem('storageU', JSON.stringify(fCacheData));  
    }catch(err){

    }
  }
}
var getCache = function (iService, iDefault) {
  var key;
  if (iService.Name) key = iService.Name;
  if (typeof (iService) == "string") key = iService;
  //if ((iService.Config) && (iService.Config.cacheData)) key = key + iService.Behavior.CacheKey;

  if ((key) && (key != "") && (localStorage.getItem('storageU'))) {
    var fCacheData = JSON.parse(localStorage.getItem('storageU'));
    if ((fCacheData) && (fCacheData[key])) {
      return fCacheData[key];
    }
  }

  return iDefault;
}
var getNewRow = function (iDefault, iExtra) {
  var fDefault = (iDefault)? iDefault:{};

  if(!fDefault.guid){
    fDefault.guid = getGuid();
  }
  fDefault.rev = 0;
  fDefault.date = new Date();
  if(Global.Session){
    fDefault.session = Global.Session;
  }

  if(iExtra&&typeof(iExtra)=="object"){
    for(var fKey in iExtra){
      if(typeof(iExtra[fKey].NewValue)!="undefined"){
        fDefault[fKey] = iExtra[fKey].NewValue;
      }
    }

    if(Global.Session.UserEmail&&iExtra['moderators']){
      fDefault['moderators'] = [  ];
      fDefault['moderators'].push(Global.Session.UserEmail);
    }
  }

  return fDefault;
}
var getSaveRow = function (iData, iRow, iSelf, iFireName) {
  var fData = getClone(iData);
  fData.Action = "save";
  var fRow;
  if(iRow){
    fRow = iRow;
  }else{
    if(iData&&iSelf&&iData.ModalRow){
      fRow = iSelf.get(iData.ModalRow);
    }
  }
  fData.Row = getClone(fRow);
  if(iFireName){
    fData.FireName = iFireName;
  }
  return fData;
}
var getDeleteRow = function (iData, iRow, iSelf) {
  var fData = getClone(iData);
  fData.Action = "delete";
  var fRow = {};
  if(iRow){
    fRow = iRow;
  }else{
    if(iData&&iSelf&&iData.ModalRow){
      fRow = iSelf.get(iData.ModalRow);
      fRow.deleted  = true;
      iSelf.set(iData.DetailRow, fRow);
    }
  }
  fData.Row = getClone(fRow);

  return fData;
}
var getEditRow = function (iData, iRow, iSelf) {
  var fData = getClone(iData);
  fData.Action = "edit";
  var fRow;
  if(iRow){
    fRow = iRow;
  }else{
    if(iData&&iSelf&&iData.DetailRow){
      fRow = iSelf.get(iData.DetailRow);
    }
  }

  fData.Row = getClone(fRow);

  return fData;
}
var getDetailRow = function (iData, iRow, iExtra) {
  var fData = getClone(iData);
  fData.Action = "detail";
  fData.Row = iRow;
  fData = getMerge(fData, iExtra);

  return fData;
}
var getViewRow = function (iData, iRow) {
  var fData = getClone(iData);
  fData.Action = "view";
  fData.Row = getClone(iRow);

  return fData;
}
var getViewRefresh = function (iData, iSelf) {
  var fData = (iData)? getClone(iData):{};

  if(fData){
    if(fData.ModalName){
      $(fData.ModalName).modal('hide');
    }
    if(fData.ModalRow&&iSelf){
      iSelf.set(fData.ModalRow, {});
    }
    if(fData.DetailRow&&iSelf){
      iSelf.set(fData.DetailRow, {});
    }
  }

  return fData;
}
var setView = function (iSelf, iSrv, iRow) {
  var fRow = iRow;
  var fRows = Client.getObject(iSelf.get(iSrv.Rows), 'guid');

  if(fRows&&fRows[iRow.guid]){
    fRow = fRows[iRow.guid];
  }

  if(iSelf){
/*     iSelf.set(SrvVote.ModalRow, fRow);
    iSelf.set(SrvVote.DetailRow, {});
    $(SrvVote.ModalName).modal('show'); */
  }

  iSelf.set(iSrv.DetailRow, fRow);

  return fRow;
}
var setNew = function (iSelf, iSrv, iRow) {
  var fRow = iRow;


  if(iSrv&&iSelf){
    if(iSrv.Rows&&iRow&&iRow.guid){
      var fRows = Client.getObject(iSelf.get(iSrv.Rows), 'guid');

      if(fRows&&fRows[iRow.guid]){
        fRow = fRows[iRow.guid];
      }

    }
    if(iSrv.ModalRow){
      iSelf.set(iSrv.ModalRow, fRow);
    }
    if(iSrv.DetailRow){
      iSelf.set(iSrv.DetailRow, {});
    }
    if(iSrv.ModalName){
      $(iSrv.ModalName).modal('show'); 
    }
  }

  return fRow;
}
var setEdit = function (iSelf, iSrv, iRow) {
  var fRow = iRow;


  if(iSrv&&iSelf){
    if(iSrv.Rows&&iRow&&iRow.guid){
      var fRows = Client.getObject(iSelf.get(iSrv.Rows), 'guid');

      if(fRows&&fRows[iRow.guid]){
        fRow = fRows[iRow.guid];
      }
    }
    if(iSrv.ModalRow){
      iSelf.set(iSrv.ModalRow, fRow);
    }
    if(iSrv.ModalName){
      $(iSrv.ModalName).modal('show'); 
    }
  }

  return fRow;
}
var MessageShow= function (iMessage, iBehavior) {
  console.log("MessageShow", iBehavior.type.indexOf("pop"), iMessage, iBehavior);
  var fMessageText = iMessage;
  var fMessageTitle = 'Info';
  var fMessagePicture = '';

  if (iMessage.Text){
    fMessageText = iMessage.Text;
  }

  if(iBehavior&&iBehavior.type&&iBehavior.type.indexOf("pop")!=-1){
    toastr.warning(fMessageText, fMessageTitle, iBehavior);
  }

  if(iBehavior&&iBehavior.type&&iBehavior.type.indexOf("desktop")!=-1){
 
    if (iMessage.Type)
      fMessageTitle = iMessage.Type;
  
    if (!("Notification" in window)) {
      console.log("This browser does not support desktop notification!");
    }
    else if (Notification.permission === "granted") {
      var options = {
        body: fMessageText,
        icon: fMessagePicture
      }
      var notification = new Notification(fMessageTitle, options);
      setTimeout(notification.close.bind(notification), 5000);
    }
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          var notification = new Notification(fMessageText);
        }
      });
    } 

  }
}
var getFileBase64 = function (iFile, iCallback) {
  if(iFile){
    try
    {
      var reader = new FileReader();
      reader.readAsDataURL(iFile);
      reader.onload = function () {
        var fResult = reader.result;
        if(iCallback) iCallback(fResult, undefined);
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
        if(iCallback) iCallback(undefined, error);
      };
    }catch(err){
      console.log('Error: ', err);
      if(iCallback) iCallback(undefined, err);
    }
  }else{
    if(iCallback) iCallback(undefined, undefined);
  }
}
var getFileContent = function (iFile, iCallback) {
  var fReturn = true;
  try
  {
    var reader = new FileReader();
    reader.readAsDataURL(iFile);
    fReturn = true;
    reader.onload = function () {
      var fResult = reader.result;
      fResult = fResult.replace('data:application/octet-stream;base64,', '');
      fResult = atob(fResult);
      if(iCallback) iCallback(fResult, undefined);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
      if(iCallback) iCallback(undefined, error);
    };
  }catch(err){
    console.log('Error: ', err);
    fReturn = false;
    if(iCallback) iCallback(undefined, err);
  }

  return fReturn;
}
module.exports = {
  MessageShow: function (iMessage, iBehavior) {
    console.log("MessageShow", iMessage, iBehavior);
    var fMessageText = iMessage;
    var fMessageTitle = 'Info';
    var fMessageIcon = '';
  
    if (iMessage.Title){
      fMessageTitle = iMessage.Title;
    }
    if (iMessage.Text){
      fMessageText = iMessage.Text;
    }
    if (iMessage.Icon){
      fMessageIcon = iMessage.Icon;
    }
    if(iBehavior&&iBehavior.type&&iBehavior.type.indexOf("pop")!=-1){
      toastr.warning(fMessageText, fMessageTitle, iBehavior);
    }
  
    if(iBehavior&&iBehavior.type&&iBehavior.type.indexOf("desktop")!=-1){
      if (!("Notification" in window)) {
        console.log("This browser does not support desktop notification!");
      }
      else if (Notification.permission === "granted") {
        var options = {
          body: fMessageText,
          icon: fMessageIcon
        }
        var notification = new Notification(fMessageTitle, options);
        setTimeout(notification.close.bind(notification), 5000);
      }
      else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
          if (permission === "granted") {
            var notification = new Notification(fMessageText);
          }
        });
      }
    }
  },
  getPin: function (iLength) {
    if (!iLength) iLength = 4;
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < iLength; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },
  getGuid: function () {
  var d = new Date().getTime();
  if (window.performance && typeof window.performance.now === "function") {
    d += performance.now(); //use high-precision timer if available
  }
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
  },
  getShort: function (iLength) {
    if (!iLength) iLength = 4;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < iLength; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },
  getClone: function (obj) {
    if (obj&&Array.isArray(obj)){
      return obj.slice();
    }
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  },
  getSortBy: function (iData, field, reverse, primer) {

    var fData = [];
    var fKeepProp = false;
    if (Array.isArray(iData)) {
      fData = iData;
    } else {
      for (var key in iData) {
        fData.push(iData[key]);
        fKeepProp = true;
      }
    }

    reverse = !reverse ? 1 : -1;
    var key = primer ?
      function (x) { return primer(x[field]) } :
      function (x) { return x[field] };

    fData.sort(function (a, b) {
      var a = key(a);//.ChangeDate;
      var b = key(b);//.ChangeDate;
      return reverse * ((a > b) - (b > a));
    });

    if (fKeepProp) {
      var fTemp = {};
      for (i = 0; i < fData.length; i++) {
        //fTemp[fData[i].PageUID] = fData[i]; 
      }

      //fData = fTemp;
    }

    return fData;
  },
  syncGeo: function(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(iPosition){
        Global.Session.UserGeo = {
          Position: {
            latitude: iPosition.latitude,
            longitude: iPosition.longitude,
            accuracy: iPosition.accuracy,
            altitude: iPosition.altitude,
            altitudeAccuracy: iPosition.altitudeAccuracy,
            heading: iPosition.heading,
            speed: iPosition.speed
          },
          lat: iPosition.coords.latitude,
          lng: iPosition.coords.longitude
        };
      });
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  },
  getArray: function (iValue) {
    var fResult = [];

    if (Array.isArray(iValue)) {
      return iValue
    }
    if (typeof (iValue) == 'string') {
      return iValue;
    }
    if (typeof (iValue) == 'object') {
      var fResult = [];
      for (var key in iValue) {
        fResult.push(iValue[key]);
      }
      return fResult;
    }

    return [];
  },
  isEcho: function (iLevel) {
    if ((Global) && (Global.Behavior) && (Global.Behavior.echoTest)) {
      if ((Global) && (Global.Behavior) && (Global.Behavior.echoLevel) && (iLevel) && (Global.Behavior.echoLevel <= iLevel)) {
        return false;
      }
      return true;
    }

    return false;
  },
  Busy:  function (iState, iSelf){

    if(typeof(iState)!=="undefined"){
      $('.dd-action').prop('disabled', iState);
      return iState;
    }
  
    return false;
  },
  getNow: function(){
    return new Date();
  },
  setCache: setCache,
  getCache: getCache,
  getMerge: getMerge,
  setDisplay: setDisplay,
  session: getSessionShort,
  Sync: {
    Data: SyncNodeSrv,
    Sync: setSync,
    SyncResult: SyncResult,
    //Session: setSession,
    Url: setUrl,
    UrlChange: setUrlChange,  
    SaveRow: getSaveRow,
    DeleteRow: getDeleteRow,
    NewRow: getNewRow,
    EditRow: getEditRow,
    ViewRow: getViewRow,
    DetailRow: getDetailRow,
    Refresh: getViewRefresh,
    View: setView,
    New: setNew,
    Edit: setEdit,
    Action: setAction
  },
  getUrlData: function (value) {
    var fUrlData = {
      guid: "",
      data: {},
      hash: ((value)?value:window.location.hash),
      cmd: "",
      params: {}
    };
    fUrlData.hash = fUrlData.hash.replace('#', '');
  
    if (fUrlData.hash) {
      var fSpl = fUrlData.hash.split('/');
  
      fUrlData.guid = fSpl[0];
      fUrlData.command = fSpl[1];
      
      if (((fStartParams = fUrlData.hash.indexOf("?")) != -1)&&(fStartParams)) {
        var fReturn = atob(fUrlData.hash.substring((fStartParams + 1)));
        try{
          fUrlData.params = JSON.parse(decodeURIComponent(fReturn));
        }
        catch(err){
          console.log('Error', err);
        }
      }
    }
  
    if(fUrlData.guid){
      if(fUrlData.guid.toLowerCase()=="add"){
        fUrlData.guid = "";
        fUrlData.cmd = "add";
      }else{
        fUrlData.cmd = "detail";
      }
    }else{
      fUrlData.cmd = "list";
    }

    return fUrlData;
  },
  getGlobal: function (iCurrent) {
    if (iCurrent) {
      return Global;
    }

    var isOpera = false;
    var isFirefox = false;
    var isSafari = false;
    var isIE = false;
    var isEdge = false;
    var isChrome = false;
    var isBlink = false;

    try {
      // Opera 8.0+
      isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
      // Firefox 1.0+
      isFirefox = typeof InstallTrigger !== 'undefined';
      // Safari 3.0+ "[object HTMLElementConstructor]" 
      isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
      // Internet Explorer 6-11
      isIE = /*@cc_on!@*/false || !!document.documentMode;
      // Edge 20+
      isEdge = !isIE && !!window.StyleMedia;
      // Chrome 1+
      isChrome = !!window.chrome && !!window.chrome.webstore;
      // Blink engine detection
      isBlink = (isChrome || isOpera) && !!window.CSS;
    } catch (err) {
      console.log('Error detectig brouser:', err);
    }

    var fBrowsers = [];
    if (isOpera) fBrowsers.push("opera");
    if (isFirefox) fBrowsers.push("firefox");
    if (isSafari) fBrowsers.push("safari");
    if (isIE) fBrowsers.push("ie");
    if (isEdge) fBrowsers.push("edge");
    if (isChrome) fBrowsers.push("chrome");
    if (isBlink) fBrowsers.push("blink");

    var fConfig =  {
      Behavior: {
        echoTest: false,
        echoLevel: 0,
        Browser: fBrowsers
      },
      Display: {
        Current: null,
        Frames: [],
        Root: null
      },
      Variables: {
        CacheExpireMSec: 60000,
        PingInterval: 120000,
        CreateDate: (new Date()).toISOString().substring(0, 19).replace('T', ' ')
      },
      Server: {
        Status: false,
        URI: undefined,
        Port: undefined
      },
      Session: {
        SessionUID: undefined,
        CookieUID: undefined,
        UserUID: undefined,
        UserName: "Visitor",
        UserData: {

        }
      },
      CacheData: {

      },
      Callbacks: {

      },
      Config: {
        echoTest: false,
        echoLevel: 0,
        offLine: false,
        myURL: "/"
      }
    };

    if(typeof(ClientConfig)==="object"&&ClientConfig){
      fConfig.Behavior  = getMerge(fConfig.Behavior, ClientConfig.Behavior);
      fConfig.Config  = getMerge(fConfig.Config, ClientConfig.Config);
    }
    fConfig.Session = this.getCache('Session', fConfig.Session);
    fConfig.Session.SessionUID = this.getGuid();
    if(fConfig.Session&&!fConfig.Session.CookieUID){
      fConfig.Session.CookieUID = getGuid();
      fConfig.Session.UserName = getShort(8);
      this.setCache('Session', fConfig.Session);
    }

    if(fConfig.Behavior&&fConfig.Behavior.echoTest){
      console.log("Config:", fConfig);
    }
    console.log("Config:", fConfig);
    ServerIO = new ioServer(fConfig);
    //fConfig.Server.IO  = new ioServer(fConfig);
    
    return fConfig;
  },
  getObject: function (iValue, iKey) {

    if (Array.isArray(iValue)) {
      var fResult = {};

      for (i = 0; i < iValue.length; i++) {
        var fKey = iValue[i][iKey];
        fResult[fKey] = iValue[i];
      }

      return fResult;
    }

    return iValue;
  },
  startsWith: function (iValue, iStartChar) {
    if (typeof (iValue) === "undefined" || !iValue) {
      return false;
    }
    try {
      return iValue.startsWith(iStartChar);
    } catch (err) {
      return (iValue)&&(iValue.indexOf(iStartChar) == 0);
    }
  },
  setFullScreen: function(iElementName){
    var elem = (iElementName)? iElementName:document.body;
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
      elem.msRequestFullscreen();
    }
  },
  setCmd: function (iAction, iData) {
    console.log('setCmd', iAction, iData);
    switch (iAction) {
      case "doEdit":
        if(iData.guid){
          window.location.hash = '#'+iData.guid+"/edit";
        }
        break;
      case "doPrint":
        window.print();
        break;
      case "doBack":
        if (1 < history.length) {
          window.history.back();
        } else {
          window.close();
          window.open('', '_self').close();
        }
        break;
      case "setUrl":
        setUrl(iData);
        break;
      default:
        if ((Global) && (Global.Display)) {
          Global.Display.Root.fire('doCmd', iAction, iData);
        }
        break;
    }
  },
  getFileBase64: getFileBase64,
  getFileContent: getFileContent
}

module.exports.Air = {
  videoInput: undefined,
  videoList: [],
  audioInput: undefined,
  audioList: [],
  Init: false,
  Active: false,
  Media: {
    constraints: {
      audio: true, 
      video: true
    },
    video: undefined,
    localMediaStream: undefined
  },
  hasMedia: function(){
    return !!(navigator.mediaDevices&&navigator.mediaDevices.getUserMedia);
  },
  getDevices: function(){    
    this.audioInput = [];
    this.videoList = [];
    var self = this;
    var deviceInfos = navigator.mediaDevices.enumerateDevices().then(function(devices) {
      for (const deviceInfo of devices) {
        switch(deviceInfo.kind){
          case  'audioinput':
            self.audioList.push(deviceInfo);
          break;
          case  'videoinput':
            self.videoList.push(deviceInfo);
          break;
        }
      }
      self.Init = true;
    })
    .catch(function(err) {
      console.log(err.name + ": " + err.message);
    });

    return deviceInfos;
  },
  handleSuccess: function(stream) {
    try{

    }catch(err){

    }
    try{

    }catch(err){

    }
    this.Media.localMediaStream = stream;
    this.Media.video.srcObject = stream;
  },
  handleError: function(error) {
    console.error('navigator.getUserMedia error: ', error);
  },  
  start: function(){
    var self = this;
    self.Active = true;
    //navigator.mediaDevices.getUserMedia(this.Media.constraints).then(this.handleSuccess).catch(this.handleError);
    navigator.mediaDevices.getUserMedia(this.Media.constraints).then(function(stream) {
      self.Media.localMediaStream = stream;
      self.Media.video.srcObject = stream;
      self.Active = true;
    }).catch(function(error) {
      self.Active = false;
      console.error('navigator.getUserMedia error: ', error);
    });
  },
  stop: function(){
    try{
      this.Media.video.pause();
      this.Active = false;
    }catch(err){

    }
    try{
      this.Media.localMediaStream.stop();
      this.Media.video.stop();
      this.Active = false;
    }catch(err){

    }
  }
}

module.exports.authJMBG = function(iJMBG) {

  let fJMBGData = {
    Status: false,
    Msg: "",
    Id: iJMBG,
    BirthDate: "",
    BirthPlace: "",
    Sex: ""
  };

  let jmbg =iJMBG.split("");
  // console.log(jmbg);
  if (jmbg.length === 13) {
      dan = Number(jmbg[0] + jmbg[1]);
      mesec = Number(jmbg[2] + jmbg[3]);
      godina = Number(jmbg[4] + jmbg[5] + jmbg[6]);
      regija = Number(jmbg[7] + jmbg[8]);
      jedinstveniBroj = Number(jmbg[9] + jmbg[10] + jmbg[11]);
      kontrolna = Number(jmbg[12]);
      ispravnaKontrolna =
          11 -
          (7 * (Number(jmbg[0]) + Number(jmbg[6])) +
            6 * (Number(jmbg[1]) + Number(jmbg[7])) +
            5 * (Number(jmbg[2]) + Number(jmbg[8])) +
            4 * (Number(jmbg[3]) + Number(jmbg[9])) +
            3 * (Number(jmbg[4]) + Number(jmbg[10])) +
            2 * (Number(jmbg[5]) + Number(jmbg[11]))) %
            11;
      if (ispravnaKontrolna > 9) ispravnaKontrolna = 0;
  } else {
      fJMBGData.Msg = "JMBG mora imati 13 cifara!";
      return fJMBGData;
  }

  function checkJMBG(iJMBG) {
    if (dan < 1 || dan > 31) {
        fJMBGData.Msg = "Neispravan dan rodjenja";
    } else if (mesec < 1 || mesec > 12) {
        fJMBGData.Msg = "Neispravan mesec rodjenja";
    } else if (godina < 900 || godina > 2017) {
        fJMBGData.Msg = "Neispravna godina rodjenja";
    } else if ((regija > 50 && regija < 70) || regija > 99) {
        fJMBGData.Msg = "Neispravna regija rodjenja";
    } else if (jedinstveniBroj > 999) {
        fJMBGData.Msg = "Neispravno unet pol!"; //nepotrbno, uvek ce tri cifre biti manje od 1000
    } else if (kontrolna !== ispravnaKontrolna) {
        fJMBGData.Msg = "Neispravan kontrolni broj";
    } else {
        fJMBGData.Msg = "Ispravan maticni broj";
        fJMBGData.Status = true;
        return true;
    }
    return false;
  }

    if(!checkJMBG(iJMBG)){
      return fJMBGData;
    }
    godina = ((godina[0]=='0')? '2':'1')+godina;
    fJMBGData.BirthPlace = regija;
    fJMBGData.BirthDate = godina+'-'+mesec+'-'+dan;
    fJMBGData.Sex = (jedinstveniBroj < 500)? "M":"F";
    var sha512 = require('js-sha512');
    fJMBGData.Id = sha512(iJMBG);

    return fJMBGData;
}

