﻿namespace BarTim
{
    partial class FormFullScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTimePast = new System.Windows.Forms.Label();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTimePast
            // 
            this.lblTimePast.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTimePast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTimePast.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimePast.Location = new System.Drawing.Point(0, 532);
            this.lblTimePast.Name = "lblTimePast";
            this.lblTimePast.Size = new System.Drawing.Size(959, 97);
            this.lblTimePast.TabIndex = 10;
            this.lblTimePast.Text = "00:00";
            this.lblTimePast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTimeLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTimeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 240F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeLeft.Location = new System.Drawing.Point(0, 0);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(959, 629);
            this.lblTimeLeft.TabIndex = 9;
            this.lblTimeLeft.Text = "00:00";
            this.lblTimeLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormFullScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 629);
            this.Controls.Add(this.lblTimePast);
            this.Controls.Add(this.lblTimeLeft);
            this.KeyPreview = true;
            this.Name = "FormFullScreen";
            this.Text = "*";
            this.Load += new System.EventHandler(this.FormFullScreen_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormFullScreen_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTimePast;
        private System.Windows.Forms.Label lblTimeLeft;
    }
}