﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace BarTim
{
    public partial class FormMain : Form
    {
        #region Timer setting

        public enum States { Start, Pause, Stop };
        private class MyTimer
        {
            private int fSecondsTotal;
            private int fSeconds;
            private DateTime fStartTime;
            private DateTime fStart;
            private DateTime fEnd;
            public States State;

            public MyTimer(int iSeconds)
            {
                this.fSecondsTotal = iSeconds;
                this.fSeconds = this.fSecondsTotal;
                this.fStartTime = DateTime.Now;
                this.fStart = DateTime.Now;
                this.fEnd = DateTime.Now;
                this.State = States.Start;
            }

            public DateTime StartAt { get { return this.fStart; } }

            public DateTime EndAt { get { return this.fEnd; } }

            public int TimeAt { get { return this.fSecondsTotal; } }

            public String TimeLeft()
            {
                if (this.State != States.Start)
                {
                    this.fStartTime = DateTime.Now;
                    return Sec2Time(this.fSeconds);
                }

                TimeSpan fTime = DateTime.Now - this.fStartTime;

                int fDifSec = fTime.Seconds;

                if (fDifSec > 0)
                {
                    this.fSeconds = this.fSeconds - fTime.Seconds;
                    this.fStartTime = DateTime.Now;
                }

                return Sec2Time(this.fSeconds);
            }

            public String TimePast()
            {
                if (this.State != States.Start)
                {
                    this.fStartTime = DateTime.Now;
                    return Sec2Time((this.fSecondsTotal - this.fSeconds));
                }

                TimeSpan fTime = DateTime.Now - this.fStartTime;
                int fDifSec = fTime.Seconds;

                if (fDifSec > 0)
                {
                    this.fSeconds = this.fSeconds - fDifSec;
                    this.fStartTime = DateTime.Now;
                }

                return Sec2Time(this.fSecondsTotal - this.fSeconds);
            }

            public String Sec2Time(int iSec)
            {
                bool Negativ = (iSec < 0);

                iSec = Math.Abs(iSec);

                String fMinute = (((iSec / 60) < 10) ? ("0" + (iSec / 60).ToString()) : ((iSec / 60).ToString()));
                String fSecond = (((iSec % 60) < 10) ? ("0" + (iSec % 60).ToString()) : ((iSec % 60).ToString()));

                return ((Negativ) ? String.Format("-{0}:{1}", fMinute, fSecond) : String.Format("{0}:{1}", fMinute, fSecond));
            }

            public void Reset()
            {
                this.fSeconds = this.fSecondsTotal;
                this.fStartTime = DateTime.Now;
                this.State = States.Start;
            }

            public void Continue()
            {
                this.fStartTime = DateTime.Now;
                this.State = States.Start;
            }
        }

        #endregion Timer setting

        private MyTimer myTimer = null;
        private Dictionary<string, int> intVal = new Dictionary<string, int>();
        private Form monitorForm = null;

        public FormMain()
        {
            InitializeComponent();
        }

        private void Action_Click(object sender, EventArgs e)
        {
            try
            {

                if (sender == this.cmdStart3Min)
                {
                    int value = 0;

                    try
                    {
                        value = this.intVal[IntervalSec.Text];
                    }
                    catch
                    {
                        try
                        {
                            int.TryParse(IntervalSec.Text, out value);

                            value = value * 60;
                        }
                        catch
                        {

                        }
                    }

                    this.myTimer = new MyTimer(value);
                    this.timer1.Enabled = true;
                }

                if (sender == this.cmdPause)
                {
                    if (this.myTimer.State != States.Pause)
                        this.myTimer.State = States.Pause;
                    else
                        this.myTimer.Continue();
                }

                if (sender == this.cmdReset)
                {
                    this.myTimer.Reset();
                }

                if (sender == this.cmdStop)
                {
                    this.myTimer.State = States.Stop;

                    ListViewItem fItem = new ListViewItem();
                    fItem.Text = this.lvHistory.Items.Count.ToString();
                    fItem.Tag = myTimer;
                    fItem.SubItems.Add(comboBox1.Text);
                    fItem.SubItems.Add(myTimer.TimePast());
                    fItem.SubItems.Add(String.Format("{0}-{1}", myTimer.StartAt.ToString("H:mm:ss"), myTimer.EndAt.ToString("H:mm:ss")));

                    this.lvHistory.Items.Insert(0, fItem);
                    this.myTimer = null;
                }

                if (sender == this.monitorOff)
                {
                    this.monitorForm.Hide();
                    this.monitorForm.Close();
                }

                if ((sender == this.eksport) && (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK))
                {
                    String fFile = "Rb,Akcija,Trajanje,Vreme";

                    foreach (ListViewItem fItem in this.lvHistory.Items)
                    {
                        fFile += String.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"", fItem.SubItems[0], fItem.SubItems[1], fItem.SubItems[2], fItem.SubItems[3]) + Environment.NewLine;
                    }

                    File.WriteAllText(saveFileDialog1.FileName, fFile, Encoding.UTF8);
                }
            }
            catch
            {

            }

        }

        private void Monitor_Click(object sender, EventArgs e)
        {
            try
            {
                Screen screen = ((sender as ToolStripMenuItem).Tag as Screen);
                this.monitorForm = new FormFullScreen();

                this.monitorForm.Location = screen.WorkingArea.Location;
                this.monitorForm.WindowState = FormWindowState.Maximized;
                this.monitorForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.monitorForm.Show();
            }
            catch
            { }

        }

        #region Events

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblTimePast.Text = this.myTimer.TimePast();
                this.lblTimeLeft.Text = this.myTimer.TimeLeft();

                if (this.monitorForm != null)
                {
                    (this.monitorForm as FormFullScreen).setTime(this.lblTimePast.Text, this.lblTimeLeft.Text);
                }
            }
            catch
            { }

        }

        private void lvHistory_Resize(object sender, EventArgs e)
        {
            try
            {
                this.lvHistory.Columns[1].Width = (this.lvHistory.Width - (this.lvHistory.Columns[0].Width + this.lvHistory.Columns[2].Width + this.lvHistory.Columns[3].Width + 15));
            }
            catch
            { }
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            this.lvHistory_Resize(sender, e);

            foreach (Screen screen in Screen.AllScreens)
            {
                ToolStripItem fItem = new ToolStripMenuItem();
                fItem.Text = screen.DeviceName;
                fItem.Tag = screen;
                fItem.Click += Monitor_Click;

                this.monitorFullScreen.Items.Add(fItem);
            }

            try
            {
                this.IntervalSec.Items.Clear();
                this.intVal.Clear();

                foreach (String s in File.ReadAllText("interval.txt", Encoding.UTF8).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    string fText = s.Split(',')[0];
                    int fInt = Convert.ToInt32(s.Split(',')[1]);

                    this.IntervalSec.Items.Add(fText);
                    this.intVal.Add(fText, fInt);
                }

                this.comboBox1.Items.Clear();
                foreach (String s in File.ReadAllText("list.txt", Encoding.UTF8).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                    this.comboBox1.Items.Add(s);
            }
            catch
            { }
        }

        #endregion Events
    }
}
