﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BarTim
{
    public partial class FormFullScreen : Form
    {
        public FormFullScreen()
        {
            InitializeComponent();
        }

        private void FormFullScreen_Load(object sender, EventArgs e)
        {
            this.lblTimeLeft.Font = new Font(this.lblTimeLeft.Font.OriginalFontName, Properties.Settings.Default.FontSize);
        }

        public void setTime(string p, string l)
        {
            this.lblTimePast.Text = p;
            this.lblTimeLeft.Text = l;
        }

        private void FormFullScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
