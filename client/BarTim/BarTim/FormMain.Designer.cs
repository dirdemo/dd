﻿namespace BarTim
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.IntervalSec = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblTimePast = new System.Windows.Forms.Label();
            this.cmdStop = new System.Windows.Forms.Button();
            this.cmdReset = new System.Windows.Forms.Button();
            this.cmdPause = new System.Windows.Forms.Button();
            this.cmdStart3Min = new System.Windows.Forms.Button();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.monitorFullScreen = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.monitorOff = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lvHistory = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eksport = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.monitorFullScreen.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.IntervalSec);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.lblTimePast);
            this.panel1.Controls.Add(this.cmdStop);
            this.panel1.Controls.Add(this.cmdReset);
            this.panel1.Controls.Add(this.cmdPause);
            this.panel1.Controls.Add(this.cmdStart3Min);
            this.panel1.Controls.Add(this.lblTimeLeft);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(852, 116);
            this.panel1.TabIndex = 0;
            // 
            // IntervalSec
            // 
            this.IntervalSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IntervalSec.FormattingEnabled = true;
            this.IntervalSec.Items.AddRange(new object[] {
            "-ZA EVROPSKI BAR - MILO ÐUKANOVIC",
            "",
            "1. BRANKA NIKEZIC",
            "2. SLOBO PAJOVIC",
            "3. NASER KRAJA",
            "4. BRANISLAV BRANKOVIC",
            "5. MIRA PRENTOVIC",
            "6. mr ZARIJA FRANOVIC",
            "7. ALEKSANDAR MARKOVIC",
            "8. dr VESNA ÐOKVUCIC",
            "9. MUHAREM MURATOVIC",
            "10. ÐURO PEJOVIC",
            "11. PEŠO GLAVANOVIC",
            "12. SONJA STANIŠLJEVIC",
            "13. MARKO BRNJADA",
            "",
            "-SOCIJALDEMOKRATSKA PARTIJA BAR-CIST IZBOR",
            "14. MICO ORLANDIC",
            "15. OSMAN SUBAŠIC",
            "16. DAMIR KALAMPEROVIC",
            "17. DENISA HALIMIC",
            "18. BECIR BERJAŠEVIC",
            "19. NEDELJKO-LALE COVIC",
            "20. SAVO PAVLOVIC",
            "21. mr NADA MARSTIJEPOVIC",
            "",
            "-DEMOKRATSKI FRONT - MIODRAG LEKIC",
            "22. dr SRÐAN KALINIC",
            "23. ALEKSANDAR RAKOCEVIC",
            "24. ZORAN TAJIC",
            "25. RANKA ŠLJIVANCANIN",
            "26. BOSILJKA RAŽNATOVIC",
            "27. mr DEJAN ÐUROVIC",
            "28. ALEKSANDRA VUJICIC",
            "29. GORAN-GULA PEJOVIC",
            "30. SAVO VUJOŠEVIC",
            "",
            "-SNP-MLADOST, MUDROST, HRABROST - SNEŽANA JONICA",
            "31. SNEŽANA JONICA",
            "32. mr MLADEN ZAGARCANIN",
            "33. MILENKO CEBALOVIC",
            "",
            "-BOŠNJACI, MUSLIMANI I ALBANCI - BAR U KOJI VJERUJEMO",
            "34. MUNIB LICINA",
            "35. dr ISMAIL DODA",
            "",
            "-POZITIVNA CRNA GORA-DOMACINSKI ZA BAR - RADOMIR NOVAKOVIC - CAKAN",
            "36. RADOMIR NOVAKOVIC-CAKAN",
            "37. EMIN DURAKOVIC"});
            this.IntervalSec.Location = new System.Drawing.Point(14, 12);
            this.IntervalSec.Name = "IntervalSec";
            this.IntervalSec.Size = new System.Drawing.Size(239, 32);
            this.IntervalSec.TabIndex = 10;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "-ZA EVROPSKI BAR - MILO ÐUKANOVIC",
            "",
            "1. BRANKA NIKEZIC",
            "2. SLOBO PAJOVIC",
            "3. NASER KRAJA",
            "4. BRANISLAV BRANKOVIC",
            "5. MIRA PRENTOVIC",
            "6. mr ZARIJA FRANOVIC",
            "7. ALEKSANDAR MARKOVIC",
            "8. dr VESNA ÐOKVUCIC",
            "9. MUHAREM MURATOVIC",
            "10. ÐURO PEJOVIC",
            "11. PEŠO GLAVANOVIC",
            "12. SONJA STANIŠLJEVIC",
            "13. MARKO BRNJADA",
            "",
            "-SOCIJALDEMOKRATSKA PARTIJA BAR-CIST IZBOR",
            "14. MICO ORLANDIC",
            "15. OSMAN SUBAŠIC",
            "16. DAMIR KALAMPEROVIC",
            "17. DENISA HALIMIC",
            "18. BECIR BERJAŠEVIC",
            "19. NEDELJKO-LALE COVIC",
            "20. SAVO PAVLOVIC",
            "21. mr NADA MARSTIJEPOVIC",
            "",
            "-DEMOKRATSKI FRONT - MIODRAG LEKIC",
            "22. dr SRÐAN KALINIC",
            "23. ALEKSANDAR RAKOCEVIC",
            "24. ZORAN TAJIC",
            "25. RANKA ŠLJIVANCANIN",
            "26. BOSILJKA RAŽNATOVIC",
            "27. mr DEJAN ÐUROVIC",
            "28. ALEKSANDRA VUJICIC",
            "29. GORAN-GULA PEJOVIC",
            "30. SAVO VUJOŠEVIC",
            "",
            "-SNP-MLADOST, MUDROST, HRABROST - SNEŽANA JONICA",
            "31. SNEŽANA JONICA",
            "32. mr MLADEN ZAGARCANIN",
            "33. MILENKO CEBALOVIC",
            "",
            "-BOŠNJACI, MUSLIMANI I ALBANCI - BAR U KOJI VJERUJEMO",
            "34. MUNIB LICINA",
            "35. dr ISMAIL DODA",
            "",
            "-POZITIVNA CRNA GORA-DOMACINSKI ZA BAR - RADOMIR NOVAKOVIC - CAKAN",
            "36. RADOMIR NOVAKOVIC-CAKAN",
            "37. EMIN DURAKOVIC"});
            this.comboBox1.Location = new System.Drawing.Point(259, 58);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(279, 32);
            this.comboBox1.TabIndex = 9;
            // 
            // lblTimePast
            // 
            this.lblTimePast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimePast.Location = new System.Drawing.Point(636, 90);
            this.lblTimePast.Name = "lblTimePast";
            this.lblTimePast.Size = new System.Drawing.Size(204, 23);
            this.lblTimePast.TabIndex = 8;
            this.lblTimePast.Text = "00:00";
            this.lblTimePast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdStop
            // 
            this.cmdStop.Image = global::BarTim.Properties.Resources.media_stop_red;
            this.cmdStop.Location = new System.Drawing.Point(96, 58);
            this.cmdStop.Name = "cmdStop";
            this.cmdStop.Size = new System.Drawing.Size(75, 31);
            this.cmdStop.TabIndex = 7;
            this.cmdStop.Text = "Stop";
            this.cmdStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdStop.UseVisualStyleBackColor = true;
            this.cmdStop.Click += new System.EventHandler(this.Action_Click);
            // 
            // cmdReset
            // 
            this.cmdReset.Location = new System.Drawing.Point(178, 58);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(75, 31);
            this.cmdReset.TabIndex = 3;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            this.cmdReset.Click += new System.EventHandler(this.Action_Click);
            // 
            // cmdPause
            // 
            this.cmdPause.Image = global::BarTim.Properties.Resources.media_pause;
            this.cmdPause.Location = new System.Drawing.Point(14, 58);
            this.cmdPause.Name = "cmdPause";
            this.cmdPause.Size = new System.Drawing.Size(75, 31);
            this.cmdPause.TabIndex = 2;
            this.cmdPause.Text = "Pauza";
            this.cmdPause.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdPause.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdPause.UseVisualStyleBackColor = true;
            this.cmdPause.Click += new System.EventHandler(this.Action_Click);
            // 
            // cmdStart3Min
            // 
            this.cmdStart3Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdStart3Min.Image = global::BarTim.Properties.Resources.media_play;
            this.cmdStart3Min.Location = new System.Drawing.Point(259, 13);
            this.cmdStart3Min.Name = "cmdStart3Min";
            this.cmdStart3Min.Size = new System.Drawing.Size(167, 31);
            this.cmdStart3Min.TabIndex = 0;
            this.cmdStart3Min.Text = "Započni";
            this.cmdStart3Min.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdStart3Min.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdStart3Min.UseVisualStyleBackColor = true;
            this.cmdStart3Min.Click += new System.EventHandler(this.Action_Click);
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimeLeft.ContextMenuStrip = this.monitorFullScreen;
            this.lblTimeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeLeft.Location = new System.Drawing.Point(626, 1);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(225, 89);
            this.lblTimeLeft.TabIndex = 5;
            this.lblTimeLeft.Text = "00:00";
            this.lblTimeLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // monitorFullScreen
            // 
            this.monitorFullScreen.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monitorOff,
            this.toolStripSeparator1});
            this.monitorFullScreen.Name = "monitorFullScreen";
            this.monitorFullScreen.Size = new System.Drawing.Size(104, 32);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(100, 6);
            // 
            // monitorOff
            // 
            this.monitorOff.Name = "monitorOff";
            this.monitorOff.Size = new System.Drawing.Size(103, 22);
            this.monitorOff.Text = "Ugasi";
            this.monitorOff.Click += new System.EventHandler(this.Action_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lvHistory);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 116);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(852, 467);
            this.panel2.TabIndex = 1;
            // 
            // lvHistory
            // 
            this.lvHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader5});
            this.lvHistory.ContextMenuStrip = this.contextMenuStrip1;
            this.lvHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvHistory.Location = new System.Drawing.Point(0, 0);
            this.lvHistory.Name = "lvHistory";
            this.lvHistory.Size = new System.Drawing.Size(852, 467);
            this.lvHistory.TabIndex = 0;
            this.lvHistory.UseCompatibleStateImageBehavior = false;
            this.lvHistory.View = System.Windows.Forms.View.Details;
            this.lvHistory.Resize += new System.EventHandler(this.lvHistory_Resize);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Akcija";
            this.columnHeader2.Width = 383;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Trajanje";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 80;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Vreme";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 120;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eksport});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(114, 26);
            // 
            // eksport
            // 
            this.eksport.Name = "eksport";
            this.eksport.Size = new System.Drawing.Size(113, 22);
            this.eksport.Text = "Eksport";
            this.eksport.Click += new System.EventHandler(this.Action_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "csv";
            this.saveFileDialog1.Title = "Snimi za Excel";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 583);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "TiM";
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.panel1.ResumeLayout(false);
            this.monitorFullScreen.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTimeLeft;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.Button cmdPause;
        private System.Windows.Forms.Button cmdStart3Min;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lvHistory;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader5;        
        private System.Windows.Forms.Button cmdStop;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTimePast;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ContextMenuStrip monitorFullScreen;
        private System.Windows.Forms.ComboBox IntervalSec;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem monitorOff;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem eksport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

